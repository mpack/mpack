#include "LiteXipBuilderUnt.h"
#include "ui_LiteXipBuilderFrm.h"
#include "XipStore.h"
#include "ROMUtils.h"
#include "DbCentre.h"
#include <LibMUIMaker.h>
#include <vector>
#include <set>
#include <QtCore/QFile>
#include <QtGUI/QMessageBox>
#include <QtGUI/QErrorMessage>

using std::vector;
using std::set;

TLiteXipBuilderFrm::TLiteXipBuilderFrm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TLiteXipBuilderFrm)
{
    ui->setupUi(this);
    QPalette palette;
    palette.setColor(QPalette::Base, palette.color(QPalette::Window));
    ui->inputFnameEdt->setPalette(palette);
    ui->overlayFnameEdt->setPalette(palette);
    ui->stockXipFnameEdt->setPalette(palette);
    ui->romSizeEdt->setPalette(palette);
    ui->loadAddressEdt->setPalette(palette);

    ui->label_7->setVisible(false);
    ui->overlayButton->setVisible(false);
    ui->overlayFnameEdt->setVisible(false);

    ui->label_8->setVisible(false);
    ui->stockXipButton->setVisible(false);
    ui->stockXipFnameEdt->setVisible(false);

    ui->label_6->setVisible(false);
    ui->stackSizeEdt->setVisible(false);

    fTargetBaseAddress = fOriginalBaseAddress = 0;
    fTargetOEP = fOriginalOEP = 0;
}

TLiteXipBuilderFrm::~TLiteXipBuilderFrm()
{
    delete ui;
}

void TLiteXipBuilderFrm::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void TLiteXipBuilderFrm::onBrowseClicked()
{
    QString exefilename = OpenSaveExeDialog.getOpenFileName(this, tr("Open Executable File"), PathSetting.LastSourceFolder, "Executable Files (*.exe);;All Files (*.*)", 0, 0);
    if (exefilename.isEmpty()) {
        return;
    }
    PathSetting.LastSourceFolder = QFileInfo(exefilename).path();

    QFile inputfile(exefilename);
    inputfile.open(QFile::ReadOnly);
    QByteArray filecontent = inputfile.readAll();
    inputfile.close();
    TPEDirector director(filecontent.data(), filecontent.size());
    if (director.IsNull()) {
        QMessageBox msgBox;
        msgBox.setText(tr("Invalidate Input."));
        msgBox.setInformativeText(tr("This file is not a executable file."));
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setDefaultButton(QMessageBox::Ok);
        msgBox.exec();

        return;
    }
    IMAGE_NT_HEADERS ntheaders = director.GetNTHeaders();
    BOOL Dropable;
    int relocindex = director.GetPureRelocSectionIndex(Dropable);
    if (relocindex == -1) {
        // no relocable
        if (ntheaders.OptionalHeader.ImageBase < 0x80000000) {
            QMessageBox msgBox;
            msgBox.setText(tr("Invalidate Input."));
            msgBox.setInformativeText(tr("This file have ImageBase lower than 0x80000000 and can't be rebase. \nIgnore this may get your xip failed to start."));
            msgBox.setStandardButtons(QMessageBox::Abort | QMessageBox::Ignore);
            msgBox.setDefaultButton(QMessageBox::Abort);
            int ret = msgBox.exec();
            if (ret == QMessageBox::Abort) {
                return;
            }
        }
    }
    fOriginalBaseAddress = ntheaders.OptionalHeader.ImageBase;
    fOriginalOEP = ntheaders.OptionalHeader.AddressOfEntryPoint;
    if (fTargetBaseAddress == 0) {
        fTargetBaseAddress = fOriginalBaseAddress;
    }
    if (ui->stoneChk->isChecked() == false) {
        fTargetBaseAddress = 0x80100000UL;
    }
    fTargetOEP = fOriginalOEP; // must change
    RefreshEditor();
    DWORD SizeOfImage = 0;
    IMAGE_SECTION_HEADER* secheader = director.GetSectionHeaders();
    for (int i = 0; i < ntheaders.FileHeader.NumberOfSections; i++) {
        if (secheader[i].VirtualAddress + secheader[i].Misc.VirtualSize > SizeOfImage) {
            SizeOfImage = AlignByBlocksize(secheader[i].VirtualAddress + secheader[i].Misc.VirtualSize, 0x1000);
        }
    }
    ui->romSizeEdt->setText(QString("%1").arg(quint32(AlignByBlocksize(SizeOfImage, 0x1000)), 8, 16, QChar('0')).toUpper());
    ui->inputFnameEdt->setText(exefilename);
}

void TLiteXipBuilderFrm::onOverlayClicked()
{
    QString kernelfilename = OpenSaveXipDialog.getOpenFileName(this, tr("Open Overlay File"), PathSetting.LastCustomXipFolder, "Kernel Images (zImage;xipImage;Image);;All Files (*.*)", 0, 0);
    if (kernelfilename.isEmpty()) {
        return;
    }
    PathSetting.LastCustomXipFolder = QFileInfo(kernelfilename).path();
    ui->overlayFnameEdt->setText(kernelfilename);
}

void TLiteXipBuilderFrm::onStockXipClicked()
{
    QString stockxipfilename = OpenSaveXipDialog.getOpenFileName(this, tr("Select Stock Xip File"), PathSetting.LastStockXipFolder + "/xip.bin", "Xip Files (*.bin);;All Files (*.*)", 0, 0);
    if (stockxipfilename.isEmpty()) {
        return;
    }
    PathSetting.LastStockXipFolder = QFileInfo(stockxipfilename).path();
    ui->stockXipFnameEdt->setText(stockxipfilename);
}

void TLiteXipBuilderFrm::onBuildClicked()
{
    QString exefilename = ui->inputFnameEdt->text();
    if (exefilename.isEmpty()) {
        onBrowseClicked();
        exefilename = ui->inputFnameEdt->text();
    }
    QFile inputfile(exefilename);
    inputfile.open(QFile::ReadOnly);
    QByteArray filecontent = inputfile.readAll();
    inputfile.close();
    TPEDirector director(filecontent.data(), filecontent.size());
    if (director.IsNull()) {
        return;
    }
    IMAGE_NT_HEADERS ntheaders = director.GetNTHeaders();
    if (fOriginalOEP == fTargetOEP) {
        // prevent user bypass load n update exe
        fTargetOEP = fOriginalOEP = ntheaders.OptionalHeader.AddressOfEntryPoint;
        RefreshEditor();
    }
    bool injectormode = ui->injectorChk->isChecked();
    bool fillarmzmagic = injectormode || ui->armZMagicChk->isChecked();
    bool directed = false;
    // TODO: Drop Exception Section
    if (ntheaders.OptionalHeader.NumberOfRvaAndSizes > IMAGE_DIRECTORY_ENTRY_IMPORT &&
        (ntheaders.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].Size > 0 || ntheaders.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress > 0)
    ) {
        // Warning!
        QErrorMessage *errorMessageDialog = new QErrorMessage(this);
        errorMessageDialog->showMessage(
            tr("Your input exe have import tables. "
            "That may kill your xip.bin on startup, "
            "Check manual to prevent this."));
    }
    if (ntheaders.OptionalHeader.NumberOfRvaAndSizes > IMAGE_DIRECTORY_ENTRY_EXCEPTION &&
        (ntheaders.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXCEPTION].Size > 0 || ntheaders.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXCEPTION].VirtualAddress > 0)
    ) {
        // Fill will 00 instead drop
        // ... later
    }
    // TODO: Calc fTargetBaseAddress
    QByteArray StockXipContent;
    DWORD StockROMBaseAddress, StockROMSize, StockROMLoadAddress; // balabala
    unsigned MainStageFreeSpace = 0;
    if (injectormode) {
        QString stockxipfilename = ui->stockXipFnameEdt->text();
        if (stockxipfilename.isEmpty()) {
            onStockXipClicked();
            stockxipfilename = ui->stockXipFnameEdt->text();
            if (stockxipfilename.isEmpty()) {
                return;//?
            }
        }
        QFile stockxipfile(stockxipfilename);
        stockxipfile.open(QFile::ReadOnly);
        LoadFlatBufferFromXip(&stockxipfile, StockXipContent, StockROMBaseAddress, StockROMSize, StockROMLoadAddress);
        stockxipfile.close();
        // Analyst NK area to find enough space place loader + kernel + etc
        //MainStageFreeSpace = 0;
        int storagetail = GetMainAreaStorageTail(StockXipContent, StockROMBaseAddress, StockROMSize, MainStageFreeSpace);
        if (storagetail != -1) {
            fTargetBaseAddress = AlignByBlocksize(storagetail, 0x10); // TODO: more safe
            MainStageFreeSpace -= (fTargetBaseAddress - storagetail);
            RefreshEditor();
        }
    }
    if (ntheaders.OptionalHeader.ImageBase != fTargetBaseAddress) {
        BOOL Dropable;
        int relocindex = director.GetPureRelocSectionIndex(Dropable);
        if (relocindex != -1) {
            director.HardRebaseModule(fTargetBaseAddress >= 0x80000000UL ? fTargetBaseAddress - 0x30000000 : fTargetBaseAddress);
            if (Dropable && relocindex == ntheaders.FileHeader.NumberOfSections - 1) {
                director.ForceRemoveSection(relocindex);
            }
            directed = true;
        }
    }
    if (directed) {
        TByteArray newcontent = director.GetJackedBuf();
        filecontent.resize(newcontent.GetSize());
        memmove_s(filecontent.data(), filecontent.size(), newcontent[0], newcontent.GetSize());
        ntheaders = director.GetNTHeaders();
    }
    QByteArray FlatBuffer;
    DWORD SizeOfImage = 0;
    DWORD PEHeaderTail = UINT_MAX;
    IMAGE_SECTION_HEADER* secheader = director.GetSectionHeaders();
    for (int i = 0; i < ntheaders.FileHeader.NumberOfSections; i++) {
        if (secheader[i].VirtualAddress + secheader[i].Misc.VirtualSize > SizeOfImage) {
            SizeOfImage = AlignByBlocksize(secheader[i].VirtualAddress + secheader[i].Misc.VirtualSize, 0x1000);
        }
        if (PEHeaderTail > secheader[i].VirtualAddress) {
            PEHeaderTail = secheader[i].VirtualAddress;
        }
    }
    DWORD MagicCost = 0;
    if (fTargetOEP != PEHeaderTail) {
        MagicCost += 4;
        if (fTargetOEP - PEHeaderTail >= 0x2000000) {
            MagicCost += 8;
        }
    }
    // Injector mode do not need magic jump ourself
    // TODO: HardRebase Tail Space?
    if (PEHeaderTail < MagicCost && injectormode == false) {
        QMessageBox msgBox;
        msgBox.setText(tr("Workaround warning"));
        msgBox.setInformativeText(tr("your executable file have non stand PE header.\nResult file may failed to boot."));
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setDefaultButton(QMessageBox::Ok);
        msgBox.exec();

        return;
    }
    ui->romSizeEdt->setText(QString("%1").arg(quint32(AlignByBlocksize(SizeOfImage, 0x1000)), 8, 16, QChar('0')).toUpper());
    FlatBuffer.resize(AlignByBlocksize(SizeOfImage, 0x1000));
    FlatBuffer.fill(0);
    for (int i = 0; i < ntheaders.FileHeader.NumberOfSections; i++) {
        int sourceoffset = secheader[i].PointerToRawData;//director.RVA2Offset(secheader[i].Misc.)
        int destoffset = secheader[i].VirtualAddress;// - fTargetBaseAddress;
        memmove_s(FlatBuffer.data() + destoffset, FlatBuffer.size() - destoffset, filecontent.data() + sourceoffset, secheader[i].SizeOfRawData);
    }
    if (ntheaders.OptionalHeader.NumberOfRvaAndSizes > IMAGE_DIRECTORY_ENTRY_EXCEPTION &&
        (ntheaders.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXCEPTION].Size > 0 || ntheaders.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXCEPTION].VirtualAddress > 0)
    ) {
        // Fill will 00 instead drop
        int destoffset = ntheaders.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXCEPTION].VirtualAddress;
        memset(FlatBuffer.data() + destoffset, 0, ntheaders.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXCEPTION].Size);
    }

    DWORD LoadAddress = fTargetBaseAddress + fTargetOEP;
    DWORD BaseAddress = fTargetBaseAddress;
    // ImageStart = BaseAddress
    // LaunchAddr = LoadAddress
    DWORD LoaderSize = FlatBuffer.size();
    if (ui->overlayChk->isChecked()) {
        QString kernelfilename = ui->overlayFnameEdt->text();
        if (kernelfilename.isEmpty()) {
            onOverlayClicked();
            kernelfilename = ui->overlayFnameEdt->text();
        }

        QFile kernelfile(kernelfilename);
        kernelfile.open(QFile::ReadOnly);
        QByteArray filecontent = kernelfile.readAll();
        kernelfile.close();
        FlatBuffer.append(filecontent); // do not padding by blocksize
    }
    if (fillarmzmagic) {
        // TODO: direct patch into xip.bin
        DWORD ArmZMagic = 0x016F2818;
        int zMagicOffset = FlatBuffer.indexOf(QByteArray((char*)&ArmZMagic, sizeof(ArmZMagic)));
        if (zMagicOffset != -1) {
            // replace with end of FlatBuffer
            // please shrink FlatBuffer now for more effective inject
            ArmZMagic = ntheaders.OptionalHeader.ImageBase; //fTargetBaseAddress >= 0x80000000UL ? fTargetBaseAddress - 0x30000000 : fTargetBaseAddress;
            ArmZMagic += LoaderSize;
            memmove_s(FlatBuffer.data() + zMagicOffset, FlatBuffer.size() - zMagicOffset, &ArmZMagic, 4);
        }
    }
    if (injectormode) {
        if (FlatBuffer.size() > MainStageFreeSpace) {
            // Warning!
            QErrorMessage *errorMessageDialog = new QErrorMessage(this);
            errorMessageDialog->showMessage(
                tr("Your inject content exceeded max space of Main Area. "
                "That may overwrite into next area, "
                "Check manual to prevent this."));
            return;
        }
        int jWongMagic = FlatBuffer.indexOf("J.WONG"); // ASCII
        if (jWongMagic != -1) {
            // TODO: work with second xip input box
            quint32 magicjump[1];
            // ARM, endian same as x86 in this time
            magicjump[0] = ((StockROMLoadAddress - 0x30000000 - (ntheaders.OptionalHeader.ImageBase + jWongMagic)) / 4 - 2) & 0xFFFFFF | 0xEA000000UL;
            memmove_s(FlatBuffer.data() + jWongMagic, FlatBuffer.size() - jWongMagic, &magicjump[0], 4); // J.WO
            memmove_s(&magicjump[0], 4, StockXipContent.data(), 4);
            jWongMagic += 4;
            memmove_s(FlatBuffer.data() + jWongMagic, FlatBuffer.size() - jWongMagic, &magicjump[0], 4); // NG00
        }
        int injectoffset = ntheaders.OptionalHeader.ImageBase - (StockROMBaseAddress - 0x30000000);
        if (injectoffset >= 4) {
            // magic jump to our OEP
            quint32 magicjump[1];
            // ARM, endian same as x86 in this time
            magicjump[0] = (((LoadAddress>=0x80000000?LoadAddress:LoadAddress-0x30000000) - (StockROMBaseAddress - 0x30000000)) / 4 - 2) & 0xFFFFFF | 0xEA000000UL;
            memmove_s(StockXipContent.data(), StockXipContent.size(), &magicjump[0], 4);
        }
        memmove_s(StockXipContent.data() + injectoffset, StockXipContent.size() - injectoffset, FlatBuffer.data(), FlatBuffer.size()); // inject done?
        FlatBuffer = StockXipContent; // Cuptools!
    } else {
        if (PEHeaderTail >= MagicCost && fTargetOEP != PEHeaderTail) {
            if (fTargetOEP - PEHeaderTail >= 0x2000000) {
                // jump
                quint32 magicjump[3];
                // ARM, endian same as x86 in this time
                magicjump[0] = quint32(0xE59F0000UL); // LDR R0, [+8]
                magicjump[1] = quint32(0xE590F000UL); // LDR PC, [R0]
                magicjump[2] = quint32(LoadAddress);  // [+8] = LoadAddress
                //FlatBuffer.replace(PEHeaderTail - 0xC, 0xC, magicjump);
                memmove_s(FlatBuffer.data() + PEHeaderTail - 0xC, FlatBuffer.size() - PEHeaderTail + 0xC, &magicjump[0], 0xC);
            } else {
                quint32 magicjump[1];
                // ARM, endian same as x86 in this time
                magicjump[0] = ((fTargetOEP - (PEHeaderTail - 4)) / 4 - 2) & 0xFFFFFF | 0xEA000000UL;
                memmove_s(FlatBuffer.data() + PEHeaderTail - 4, FlatBuffer.size() - PEHeaderTail + 4, &magicjump[0], 4);
            }
        }
    }

    ui->buildButton->setVisible(false);
    QString xipfilename = OpenSaveXipDialog.getSaveFileName(this, tr("Create Custom Xip File"), PathSetting.LastCustomXipFolder + "/xip.bin", "Xip Files (*.bin);;All Files (*.*)", 0, 0);
    if (xipfilename.isEmpty()) {
        ui->buildButton->setVisible(true);
        return;
    }

    if (ui->keepNB0Chk->isChecked()) {
        // TODO: work with xip input
        QFile nb0file(xipfilename + ".nb0");
        nb0file.open(QIODevice::WriteOnly);
        nb0file.write(FlatBuffer);
        nb0file.close();
    }

    if (injectormode == false) {
        GenerateXipFromFlatBuffer(FlatBuffer.data(), FlatBuffer.size(), BaseAddress, LoadAddress, xipfilename, ui->compressionSlider->value(), ui->keepBeginChk->isChecked());
    } else {
        // may be stock new mix? only to verify
        GenerateXipFromFlatBuffer(FlatBuffer.data(), FlatBuffer.size(), StockROMBaseAddress, StockROMLoadAddress, xipfilename, ui->compressionSlider->value(), ui->keepBeginChk->isChecked());
    }
    PathSetting.LastCustomXipFolder = QFileInfo(xipfilename).path();
    ui->buildButton->setVisible(true);
}

void TLiteXipBuilderFrm::onBaseAddressEdited(QString NewValue)
{
    bool ok;
    uint newint = NewValue.toUInt(&ok, 16);
    if (ok) {
        fTargetBaseAddress = newint;
    }
    RefreshEditor();
}

void TLiteXipBuilderFrm::onLoadAddressEdited(QString NewValue)
{
    bool ok;
    uint newint = NewValue.toUInt(&ok, 16);
    if (ok) {
        fTargetOEP = newint - fTargetBaseAddress;
    }
    RefreshEditor();
}

void TLiteXipBuilderFrm::RefreshEditor()
{
    QString newbasestr = QString("%1").arg(quint32(fTargetBaseAddress), 8, 16, QChar('0')).toUpper();
    QString newloadstr = QString("%1").arg(quint32(fTargetBaseAddress + fTargetOEP), 8, 16, QChar('0')).toUpper();
    if (ui->baseAddressEdt->text() != newbasestr) {
        ui->baseAddressEdt->setText(newbasestr);
    }
    if (ui->loadAddressEdt->text() != newloadstr){
        ui->loadAddressEdt->setText(newloadstr);
    }
}

void TLiteXipBuilderFrm::onUserdefineChanged( int state )
{
    if (state == Qt::Checked) {
        ui->loadAddressEdt->setReadOnly(false);
        QPalette palette;
        palette.setColor(QPalette::Base, palette.color(QPalette::Base));
        ui->loadAddressEdt->setPalette(palette);
    } else if (state == Qt::Unchecked) {
        ui->loadAddressEdt->setReadOnly(true);
        QPalette palette;
        palette.setColor(QPalette::Base, palette.color(QPalette::Window));
        ui->loadAddressEdt->setPalette(palette);
    }
}

void TLiteXipBuilderFrm::onInjectorChanged( int state )
{
    if (state == Qt::Checked) {
        ui->baseAddressLabel->setText(tr("Inject Address:"));
    } else if (state == Qt::Unchecked) {
        ui->baseAddressLabel->setText(tr("Base Address:"));
        if (fTargetBaseAddress != 0) {
            if (ui->stoneChk->isChecked() == false) {
                fTargetBaseAddress = 0x80100000UL;
            }
            fTargetOEP = fOriginalOEP; // must change
        }
    }
}

bool GenerateXipFromFlatBuffer( void * Data, unsigned Size, unsigned BaseAddress, unsigned LoadAddress, QString XipFilename, int Compression, bool KeepBeginEmpty ) {
    bool Result = false;

    // this function translate from pascal version released by kingvera
    vector <TChunkHeader> Chunks;
    LPSTR BuffStart, BuffCurr, BuffPrev, BuffEnd;
    tagAddressBundle Address;
    int Index;
    quint32 SourcePos;
    TChunkHeader Tail;
    TRLEXIPMBR MBR;
    int GapGates[5];
    int GapFilter;

    BuffStart = LPSTR(Data);
    BuffEnd   = LPSTR(DWORD(BuffStart) + Size);
    BuffCurr  = BuffStart;
    BuffPrev  = BuffStart;
    GapGates[0] = 0x40;
    GapGates[1] = 0x36;
    GapGates[2] = 0x30;
    GapGates[3] = 0x18;
    GapGates[4] = 0xC;
    if (Compression > 4) Compression = 4;
    if (Compression < 0) Compression = 0;
    GapFilter = GapGates[Compression];
    int runpercent = 0;
    while (true) {
        // TODO: check 128K/4K Chunk
        Address.Union = SearchGap(BuffCurr, (DWORD(BuffEnd) - DWORD(BuffCurr)) / 4);
        if (Address.Stop < DWORD(BuffEnd)) {
            // Check valuable
            if (Address.Stop - Address.Start > GapFilter ) { // $36
                TChunkHeader item;
                item.TargetAddress = DWORD(BuffPrev) - DWORD(BuffStart);
                item.SourceSize = Address.Start - DWORD(BuffPrev) - 4;
                item.Checksum32 = CalcChecksum32(&BuffStart[item.TargetAddress], item.SourceSize);
                if (item.SourceSize > 0 || BuffCurr != BuffStart) {
                    Chunks.push_back(item); // Expand
                } else if (KeepBeginEmpty) {
                    // ==0 n ==start
                    Chunks.push_back(item);
                }
                BuffCurr = LPSTR(Address.Stop - 4);
                BuffPrev = BuffCurr;
            } else {
                BuffCurr = LPSTR(Address.Stop - 4);
                // Keep BuffPrev
            }
            int curprecent = (BuffCurr - BuffStart) * 20 / (BuffEnd - BuffStart);
            if (curprecent > runpercent) {
                runpercent = curprecent;
                //emit xipProgressChanged(runpercent);
            }
        }  else {
            // End Reached
            TChunkHeader item;
            item.TargetAddress = DWORD(BuffPrev) - DWORD(BuffStart);
            item.SourceSize = Address.Start - DWORD(BuffPrev) - 4;
            item.Checksum32 = CalcChecksum32(&BuffStart[item.TargetAddress], item.SourceSize);
            Chunks.push_back(item);
            break;
        }
    }
    runpercent = 20;
    //emit xipProgressChanged(runpercent);

    QFile xipfile(XipFilename);
    xipfile.open(QIODevice::WriteOnly);

    strcpy_s(&MBR.Magic[0], sizeof(MBR.Magic), "B000FF");
    MBR.Magic[6] = 0x0A;
    MBR.RomAddress = BaseAddress;
    MBR.RomSize = Size;
    xipfile.write((char*)&MBR, sizeof(TRLEXIPMBR));
    xipfile.seek(0x0F);
    for (Index = 0; Index < Chunks.size(); Index++) {
        SourcePos = Chunks[Index].TargetAddress;
        Chunks[Index].TargetAddress += MBR.RomAddress;
        xipfile.write((char*)&Chunks[Index], sizeof(TChunkHeader));
        xipfile.write((char*)&BuffStart[SourcePos], Chunks[Index].SourceSize);
        int curprecent = (SourcePos + Chunks[Index].SourceSize - DWORD(BuffStart)) * 80 / (BuffEnd - BuffStart) + 20;
        if (curprecent > runpercent) {
            runpercent = curprecent;
            //emit xipProgressChanged(runpercent);
        }
    }
    Tail.TargetAddress = 0;
    Tail.SourceSize = LoadAddress;
    Tail.Checksum32 = 0;
    xipfile.write((char*)&Tail, sizeof(TChunkHeader));
    runpercent = 100;
    //emit xipProgressChanged(runpercent);

    //Free_And_Nil(OutputFStream);
    xipfile.close();
    //SetLength(WorkingBuf, 0);
    Chunks.clear();

    Result = true;

    return Result;
}

// TODO: move to .h
#define IMGOFSINCREMENT 0x1000


unsigned GetMainAreaStorageTail( QByteArray& FlatBuffer, unsigned ROMBaseAddress, unsigned ROMSize, unsigned& FreeSpace )
{
    int Result = -1;

    DWORD ROMXipChainAddress;
    QVector < TAreaRec > ROMHeaders;
    QVector < XIPCHAIN_ENTRY > XipChainRecs; // count must equal to headers. position may reversed

    set < DWORD > headerentrys;
    set < DWORD > knownpid;
    // future: fix iterating over memblocks, now it does not handle 'holes' in the memory range very well.
    int areaindex = 0;
    for (DWORD romofs = (ROMBaseAddress + IMGOFSINCREMENT - 1) & ~(IMGOFSINCREMENT - 1); romofs < ROMBaseAddress + ROMSize; romofs += IMGOFSINCREMENT) {
        DWORD *rom = (DWORD*)(FlatBuffer.data() + romofs - ROMBaseAddress); // we do not need work as dumprom because M8 have flat layout
        if (rom == NULL)
            continue;
        if (rom[ROM_SIGNATURE_OFFSET/sizeof(DWORD)] == ROM_SIGNATURE) {
            if (rom[0] == 0xea0003fe) {
                //g_regions.MarkRegion(romofs, 4, "JUMP to kernel start");
            }
            //g_regions.MarkRegion(g_mem.GetOfs(&rom[16]), 12, "'ECEC' -> %08lx %08lx", rom[17], rom[18]);

            if (headerentrys.find(rom[17]) == headerentrys.end()) {
                // We should have 2 headers for M8
                //DumpRomHdr(areaindex++, rom[17]);
                PROMHDR header = PROMHDR(FlatBuffer.data() + rom[17] - ROMBaseAddress);

                TAreaRec rec;
                memcpy_s(&rec.AreaMark, sizeof(rec.AreaMark), &rom[0], sizeof(rec.AreaMark));
                int paddingsum = 0;
                for (int i = 0; i < _countof(rec.AreaMark.Padding); i++) {
                    paddingsum += rec.AreaMark.Padding[i];
                }
                rec.PureSignature = paddingsum == 0;
                paddingsum = 0;
                for (int i = 0x13; i < 0x3FF; i++) {
                    paddingsum += rom[i];
                }
                rec.PureECEC = paddingsum == 0;
                Q_ASSERT(rec.PureECEC && (rec.PureSignature || areaindex > 0));
                rec.Header = *header; // @CopyRecord
                rec.MarkAddress = romofs; //DataToVA(&rom[0]);
                ROMHeaders.push_back(rec);

                headerentrys.insert(rom[17]);    // keep track of multiple pointers to same header.

                areaindex++;
            }
        }
    }
    //Result = headerentrys.size() > 0;

    // XipEntry?
    //fROMXipChainAddress = fROMBaseAddress + 0x07F00000;
    // we consider unaligned bytes inside rom maybe xipchain
    if (ROMSize % 0x1000 != 0) {
        ROMXipChainAddress = ROMBaseAddress + ROMSize - (ROMSize % 0x1000);
    } else {
        if (ROMSize == 0x7F00528UL) {
            ROMXipChainAddress = ROMBaseAddress + 0x07F00000;
        } else if (ROMSize = 0x3F00264UL) {
            ROMXipChainAddress = ROMBaseAddress + 0x03F00000;
        }
    }
    XIPCHAIN_INFO *xipchain = (XIPCHAIN_INFO *)(FlatBuffer.data() + ROMXipChainAddress - ROMBaseAddress);
    Q_ASSERT(xipchain != NULL);

    if (xipchain->cXIPs > MAX_ROM) {
        //logprintf("ERROR - invalid xipchain\n");
        return Result;
    }

    XIPCHAIN_ENTRY *xip = &xipchain->xipEntryStart;

    for (DWORD i = 0 ; i < xipchain->cXIPs ; ++i) {
        //DumpXIPChainEntry(i, &xip[i]);
        XipChainRecs.push_back(xip[i]);
    }
    if (XipChainRecs.size() <= 0) {

    }

    Q_ASSERT(XipChainRecs.size() == ROMHeaders.size());

    foreach(TAreaRec arearec, ROMHeaders) {
        if (Result == -1 || Result > arearec.Header.physlast) {
            Result = arearec.Header.physlast;
        } else {
            // Result assigned.
            if (arearec.Header.physfirst > Result) {
                if (arearec.Header.physfirst - Result > FreeSpace) {
                    FreeSpace = arearec.Header.physfirst - Result;
                }
            }
        }
    }

    return unsigned(Result);
}
