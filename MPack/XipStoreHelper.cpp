#include "XipStore.h"


QDataStream & operator<<( QDataStream &out, const DWORD &w )
{
    out << quint32(w);
    return out;
}

QDataStream & operator>>( QDataStream &out, DWORD &w )
{
    out >> reinterpret_cast<quint32&>(w);
    return out;
}



QDataStream & operator<<( QDataStream &out, const TAreaRec &area )
{
    out.writeRawData((char*)&area, sizeof(area));
    return out;
}

QDataStream & operator<<( QDataStream &out, const TROMExtRecItem &ext )
{
    out << ext.EntryAddress;
    out << ext.HeaderIndex;
    out << ext.ExtEntry;
    out << ext.ExtData;
    return out;
}
QDataStream & operator<<( QDataStream &out, const ROMPID &ext )
{
    out.writeRawData((char*)&ext, sizeof(ext));
    return out;
}

QDataStream & operator<<( QDataStream &out, const XIPCHAIN_ENTRY &xip )
{
    out.writeRawData((char*)&xip, sizeof(xip));
    return out;
}

QDataStream & operator<<( QDataStream &out, const TBundleRecItem &item )
{
    out << quint32(item.ItemType[0]) << quint32(item.ItemType[1]) << quint32(item.ItemType[2]);
    out << item.Stage[0] << item.Stage[1] << item.Stage[2];
    out << item.EntryAddress[0] << item.EntryAddress[1] << item.EntryAddress[2];
    out << item.TocInfo[0] << item.TocInfo[1] << item.TocInfo[2]; // Module
    out << item.FileInfo[0] << item.FileInfo[1] << item.FileInfo[2]; // File filename missed
    out << item.CopyInfo[0] << item.CopyInfo[1] << item.CopyInfo[2]; // Copy
    out << item.RawBuffer[0] << item.RawBuffer[1] << item.RawBuffer[2];
    out << item.Filename[0] << item.Filename[1] << item.Filename[2];
    out << item.Desription[0] << item.Desription[1] << item.Desription[2];
    out << item.E32Info[0] << item.E32Info[1] << item.E32Info[2];
    out << item.O32Info[0] << item.O32Info[1] << item.O32Info[2];
    out << item.PureResource[0] << item.PureResource[1] << item.PureResource[2];
    out << item.StockPresent << item.PreviousPresent << item.IncomingPresent;
    out << item.ItemNuked[0] << item.ItemNuked[1] << item.ItemNuked[2];
    return out;
}

QDataStream & operator<<( QDataStream &out, const e32_rom &e32 )
{
    out.writeRawData((char*)&e32, sizeof(e32));
    return out;
}

QDataStream & operator<<( QDataStream &out, const o32_rom &o32 )
{
    out.writeRawData((char*)&o32, sizeof(o32));
    return out;
}

QDataStream & operator<<( QDataStream &out, const TOCentry &toc )
{
    out.writeRawData((char*)&toc, sizeof(toc));
    return out;
}

QDataStream & operator<<( QDataStream &out, const COPYentry &copy )
{
    out.writeRawData((char*)&copy, sizeof(copy));
    return out;
}

QDataStream & operator<<( QDataStream &out, const FILESentry &files )
{
    out.writeRawData((char*)&files, sizeof(files));
    return out;
}

QDataStream & operator>>( QDataStream &in, TAreaRec &area )
{
    in.readRawData((char*)&area, sizeof(area));
    return in;
}

QDataStream & operator>>( QDataStream &in, TROMExtRecItem &ext )
{
    in >> ext.EntryAddress;
    in >> ext.HeaderIndex;
    in >> ext.ExtEntry;
    in >> ext.ExtData;
    return in;
}

QDataStream & operator>>( QDataStream &in, ROMPID &ext )
{
    in.readRawData((char*)&ext, sizeof(ext));
    return in;
}

QDataStream & operator>>( QDataStream &in, XIPCHAIN_ENTRY &xip )
{
    in.readRawData((char*)&xip, sizeof(xip));
    return in;
}

QDataStream & operator>>( QDataStream &in, TBundleRecItem &item )
{
    quint32 ItemType[3];
    in >> ItemType[0] >> ItemType[1] >> ItemType[2];
    item.ItemType[0] = TBundleType(ItemType[0]);
    item.ItemType[1] = TBundleType(ItemType[1]);
    item.ItemType[2] = TBundleType(ItemType[2]);
    in >> item.Stage[0] >> item.Stage[1] >> item.Stage[2];
    in >> item.EntryAddress[0] >> item.EntryAddress[1] >> item.EntryAddress[2];
    in >> item.TocInfo[0] >> item.TocInfo[1] >> item.TocInfo[2]; // Module
    in >> item.FileInfo[0] >> item.FileInfo[1] >> item.FileInfo[2]; // File filename missed
    in >> item.CopyInfo[0] >> item.CopyInfo[1] >> item.CopyInfo[2]; // Copy
    in >> item.RawBuffer[0] >> item.RawBuffer[1] >> item.RawBuffer[2];
    in >> item.Filename[0] >> item.Filename[1] >> item.Filename[2];
    in >> item.Desription[0] >> item.Desription[1] >> item.Desription[2];
    in >> item.E32Info[0] >> item.E32Info[1] >> item.E32Info[2];
    in >> item.O32Info[0] >> item.O32Info[1] >> item.O32Info[2];
    in >> item.PureResource[0] >> item.PureResource[1] >> item.PureResource[2];
    in >> item.StockPresent >> item.PreviousPresent >> item.IncomingPresent;
    in >> item.ItemNuked[0] >> item.ItemNuked[1] >> item.ItemNuked[2];
    return in;
}

QDataStream & operator>>( QDataStream &in, e32_rom &e32 )
{
    in.readRawData((char*)&e32, sizeof(e32));
    return in;
}

QDataStream & operator>>( QDataStream &in, o32_rom &o32 )
{
    in.readRawData((char*)&o32, sizeof(o32));
    return in;
}

QDataStream & operator>>( QDataStream &in, TOCentry &toc )
{
    in.readRawData((char*)&toc, sizeof(toc));
    return in;
}

QDataStream & operator>>( QDataStream &in, COPYentry &copy )
{
    in.readRawData((char*)&copy, sizeof(copy));
    return in;
}

QDataStream & operator>>( QDataStream &in, FILESentry &files )
{
    in.readRawData((char*)&files, sizeof(files));
    return in;
}
