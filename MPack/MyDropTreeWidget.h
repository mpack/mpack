#ifndef _MY_DROP_TREE_WIDGET_H
#define _MY_DROP_TREE_WIDGET_H

#include <QObject>
#include <QTreeWidget>

class MyDropTreeWidget : public QTreeWidget
{
    Q_OBJECT

public:
    MyDropTreeWidget(QWidget* parent = 0);
private:
    bool ContentSortingEnabled;
    Qt::SortOrder Order;
protected:
    void dragEnterEvent(QDragEnterEvent *event);
    void dragMoveEvent(QDragMoveEvent *event);
    void dragLeaveEvent(QDragLeaveEvent *event);
    void dropEvent(QDropEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
public slots:
    void clearDrop();
    void headerClicked(int);
signals:
    void dropChanged(const QMimeData *mimeData = 0);
    void sortDisabled(int);
    void deleteRequest(bool);
};



#endif
