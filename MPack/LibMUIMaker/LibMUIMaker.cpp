


////////////////////////////////////////////////////////////////////////////////////////////////////
// file: LibMUIMaker.cpp
// date: February 09 2009
// font: consolas,9pt
// 
// Interface for MUI Maker
////////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////////
#include "LibMUIMaker.h"

using namespace MakerBinaryInterface;
using namespace KOLBinaryInterface;


bool TMUIMaker::InitString( WCHAR* content )
{
	return MakerDoInitString(fMaker, content) == TRUE;
}

bool TMUIMaker::SaveToFile( WCHAR* filename )
{
	return MakerDoSaveToFile(fMaker, filename) == TRUE;
}

bool TMUIMaker::RegisterString( INT32 stringid, WCHAR* content )
{
    return MakerDoRegisterString(fMaker, stringid, content) == TRUE;
}

bool TMUIMaker::IsNull()
{
    return fMaker == NULL;
}

TMUIMaker::TMUIMaker()
{
	fMaker = NewMUIMaker();
	//if (fMaker == NULL) {
	//	delete this;
	//}
}

TMUIMaker::TMUIMaker( void * buffer, int size )
{
	fMaker = NewMUIMakerEx(buffer, size);
	//if (fMaker == NULL) {
	//	delete this;
	//}
}

TMUIMaker::~TMUIMaker()
{
    FreeMUIMaker(fMaker);
}

bool TPEDirector::ResizeSection( INT32 index, INT32 newrawsize, DWORD newvirtualsize, BOOL autofix )
{
    return PEDirectorDoResizeSection(fPEDirector, index, newrawsize, newvirtualsize, autofix) == TRUE;
}

int TPEDirector::GetLiteralSection()
{
    return PEDirectorDoGetLiteralSection(fPEDirector);
}

bool TPEDirector::MakeupLiteralSection( CHAR *name )
{
    return PEDirectorDoMakeupLiteralSection(fPEDirector, name) == TRUE;
}

int TPEDirector::GetPureRelocSectionIndex( BOOL &dropable )
{
    return PEDirectorDoGetPureRelocSectionIndex(fPEDirector, dropable);
}

int TPEDirector::GetPureResourceSectionIndex()
{
    return PEDirectorDoGetPureResourceSectionIndex(fPEDirector);
}

bool TPEDirector::HardRebaseModule( DWORD newbase )
{
    return PEDirectorDoHardRebaseModule(fPEDirector, newbase) == TRUE;
}

bool TPEDirector::ForceRemoveSection( DWORD Index )
{
    return PEDirectorDoForceRemoveSection(fPEDirector, Index) == TRUE;
}

bool TPEDirector::FlushSectionToJackedBuf()
{
    return PEDirectorDoFlushSectionToJackedBuf(fPEDirector) == TRUE;
}

bool TPEDirector::FlushNTHeaderToJackedBuf()
{
    return PEDirectorDoFlushNTHeaderToJackedBuf(fPEDirector) == TRUE;
}

TByteArray TPEDirector::GetJackedBuf()
{
    return TByteArray(PEDirectorDoGetJackedBuf(fPEDirector));
}

DWORD TPEDirector::RVA2Offset( DWORD address )
{
    return PEDirectorDoRVA2Offset(fPEDirector, address);
}

DWORD TPEDirector::VA2Offset( DWORD address )
{
    return PEDirectorDoVA2Offset(fPEDirector, address);
}

DWORD TPEDirector::Offset2RVA( DWORD offset )
{
    return PEDirectorDoOffset2RVA(fPEDirector, offset);
}

DWORD TPEDirector::Offset2VA( DWORD offset )
{
    return PEDirectorDoOffset2VA(fPEDirector, offset);
}

IMAGE_NT_HEADERS TPEDirector::GetNTHeaders()
{
    //return PEDirectorDoGetNTHeaders(fPEDirector);
    IMAGE_NT_HEADERS headers;
    GetNTHeadersEx(headers);
    return headers;
}

bool TPEDirector::GetNTHeadersEx( IMAGE_NT_HEADERS &ntheaders )
{
    return PEDirectorDoGetNTHeadersEx(fPEDirector, ntheaders) == TRUE;
}

IMAGE_SECTION_HEADER* TPEDirector::GetSectionHeaders()
{
    return (IMAGE_SECTION_HEADER*)PEDirectorDoGetSectionHeaders(fPEDirector);
}


bool TPEDirector::IsNull()
{
    return fPEDirector == NULL;
}

/*TPEDirector::TPEDirector()
{
    fPEDirector = NewPEDirector();
}*/

TPEDirector::TPEDirector( void * buffer, int size )
{
    fPEDirector = NewPEDirectorEx(buffer, size);
}

TPEDirector::~TPEDirector()
{
    FreePEDirector(fPEDirector);
}

void TByteArray::Resize( int newsize )
{
    ByteArrayDoResize(fByteArray, newsize);
}

int TByteArray::GetSize()
{
    return ByteArrayDoGetSize(fByteArray);
}

char* TByteArray::operator[]( int pos )
{
    char* Result = (char*)ByteArrayDoGetData(fByteArray);
    if (pos >= ByteArrayDoGetSize(fByteArray)) {
        Result = NULL;
    } else if (Result != 0) {
        Result += pos;
    }
    return Result;
}

TByteArray::operator LPINT() const
{
    return fByteArray;
}

bool TByteArray::IsNull()
{
    return fByteArray == NULL;
}

TByteArray::TByteArray(): fNoStorage(false)
{
    fByteArray = NewByteArray();
}

TByteArray::TByteArray( LPINT ByteArray ): fNoStorage(true), fByteArray(ByteArray)
{

}

TByteArray::~TByteArray()
{
    if (fNoStorage == false) {
        FreeByteArray(fByteArray);
    }
}

void SetLength( TByteArray& bytearray, int size )
{
    bytearray.Resize(size);
}

int Length( TByteArray& bytearray )
{
    return bytearray.GetSize();
}

int TStream::GetSize()
{
    return MemoryStreamDoGetSize(fStream);
}

unsigned TStream::Read( void *buffer, unsigned size )
{
    return MemoryStreamDoRead(fStream, buffer, size);
}

unsigned TStream::Write( void *buffer, unsigned size )
{
    return MemoryStreamDoWrite(fStream, buffer, size);
}

unsigned TStream::GetPosition()
{
    return MemoryStreamDoGetPosition(fStream);
}

unsigned TStream::SetPosition( unsigned newpos )
{
    return MemoryStreamDoSetPosition(fStream, newpos);
}

TStream::operator LPINT() const
{
    return fStream;
}

bool TStream::IsNull()
{
    return fStream == NULL;
}

TStream::TStream()
{
    fStream = NewMemoryStream();
}

TStream::~TStream()
{
    FreeMemoryStream(fStream);
}
