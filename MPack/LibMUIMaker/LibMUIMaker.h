


////////////////////////////////////////////////////////////////////////////////////////////////////
// file: LibMUIMaker.cpp
// date: February 09 2009
// font: consolas,9pt
// 
// Interface defination for MUI Maker
////////////////////////////////////////////////////////////////////////////////////////////////////


#ifndef _LIB_MUIMAKER
#define _LIB_MUIMAKER


////////////////////////////////////////////////////////////////////////////////////////////////////
#include <Windows.h>




namespace MakerBinaryInterface {
// Lets makeup self
extern "C" LPINT  WINAPI NewMUIMaker( void );

// Build Maker from buffer
extern "C" LPINT  WINAPI NewMUIMakerEx(LPVOID buffer, INT32 size);

// Free the blue skin
extern "C" BOOL   WINAPI FreeMUIMaker(LPINT maker);

// insert 
extern "C" BOOL   WINAPI MakerDoRegisterString(LPINT maker, INT32 stringid, WCHAR *content);

// init
extern "C" BOOL   WINAPI MakerDoInitString(LPINT maker, WCHAR *section);

// save
extern "C" BOOL   WINAPI MakerDoSaveToFile(LPINT maker, WCHAR *filename);

// stream
extern "C" BOOL   WINAPI MakerDoSaveToStream(LPINT maker, LPINT *stream);


extern "C" LPINT    WINAPI NewPEDirectorEx(LPVOID Buffer, INT32 Size);
extern "C" BOOL     WINAPI FreePEDirector(LPINT PEDirector);
extern "C" BOOL     WINAPI PEDirectorDoResizeSection(LPINT PEDirector, INT32 Index, INT32 newRawSize, DWORD newVirtualSize, BOOL AutoFix = FALSE);
extern "C" INT32    WINAPI PEDirectorDoGetLiteralSection(LPINT PEDirector);
extern "C" BOOL     WINAPI PEDirectorDoMakeupLiteralSection(LPINT PEDirector, CHAR *Name);
extern "C" INT32    WINAPI PEDirectorDoGetPureRelocSectionIndex(LPINT PEDirector, BOOL &Dropable);
extern "C" INT32    WINAPI PEDirectorDoGetPureResourceSectionIndex(LPINT PEDirector);
extern "C" BOOL     WINAPI PEDirectorDoHardRebaseModule(LPINT PEDirector, DWORD Newbase);
extern "C" BOOL     WINAPI PEDirectorDoForceRemoveSection(LPINT PEDirector, DWORD Index);
extern "C" BOOL     WINAPI PEDirectorDoFlushSectionToJackedBuf(LPINT PEDirector);
extern "C" BOOL     WINAPI PEDirectorDoFlushNTHeaderToJackedBuf(LPINT PEDirector);
extern "C" LPINT    WINAPI PEDirectorDoGetJackedBuf(LPINT PEDirector);
extern "C" DWORD    WINAPI PEDirectorDoRVA2Offset(LPINT PEDirector, DWORD Address);
extern "C" DWORD    WINAPI PEDirectorDoVA2Offset(LPINT PEDirector, DWORD Address);
extern "C" DWORD    WINAPI PEDirectorDoOffset2RVA(LPINT PEDirector, DWORD Offset);
extern "C" DWORD    WINAPI PEDirectorDoOffset2VA(LPINT PEDirector, DWORD Offset);
//extern "C" IMAGE_NT_HEADERS   WINAPI PEDirectorDoGetNTHeaders(LPINT PEDirector);
extern "C" BOOL     WINAPI PEDirectorDoGetNTHeadersEx(LPINT PEDirector, IMAGE_NT_HEADERS &NTHeaders);
extern "C" LPINT    WINAPI PEDirectorDoGetSectionHeaders(LPINT PEDirector);

}

namespace KOLBinaryInterface {
extern "C"  LPINT   WINAPI NewByteArray();
extern "C"  BOOL    WINAPI FreeByteArray(LPINT ByteArray);
extern "C"  VOID    WINAPI ByteArrayDoResize(LPINT ByteArray, INT32 NewSize);
extern "C"  INT32   WINAPI ByteArrayDoGetSize(LPINT ByteArray);
extern "C"  LPVOID  WINAPI ByteArrayDoGetData(LPINT ByteArray);

extern "C"  LPINT   WINAPI NewMemoryStream();
extern "C"  BOOL    WINAPI FreeMemoryStream(LPINT Stream);
extern "C"  INT64   WINAPI MemoryStreamDoGetSize(LPINT Streamm);
extern "C"  DWORD   WINAPI MemoryStreamDoRead(LPINT Stream, LPVOID Buffer, DWORD Size);
extern "C"  DWORD   WINAPI MemoryStreamDoWrite(LPINT Stream, LPVOID Buffer, DWORD Size);
extern "C"  DWORD   WINAPI MemoryStreamDoGetPosition(LPINT Stream);
extern "C"  DWORD   WINAPI MemoryStreamDoSetPosition(LPINT Stream, DWORD NewPos);
}

// omake
extern "C" BOOL   WINAPI UtilNukeFile(WCHAR *filename, BOOL *mayuseconsole);

class TMUIMaker
{
public:
	TMUIMaker();
	explicit TMUIMaker(void * buffer, int size);
	~TMUIMaker();
public:
    bool IsNull();
	bool InitString(WCHAR* content);
    bool RegisterString(INT32 stringid, WCHAR* content);
	bool SaveToFile(WCHAR* filename);
private:
	LPINT fMaker;
};


class TByteArray;
class TPEDirector
{
public:
    //TPEDirector();
    explicit TPEDirector(void * buffer, int size);
    ~TPEDirector();
public:
    bool    IsNull();
    bool    ResizeSection(INT32 index, INT32 newrawsize, DWORD newvirtualsize, BOOL autofix = FALSE);
    int     GetLiteralSection();
    bool    MakeupLiteralSection(CHAR *name);
    int     GetPureRelocSectionIndex(BOOL &dropable);
    int     GetPureResourceSectionIndex();
    bool    HardRebaseModule(DWORD newbase);
    bool    ForceRemoveSection(DWORD Index);
    bool    FlushSectionToJackedBuf();
    bool    FlushNTHeaderToJackedBuf();
    TByteArray  GetJackedBuf();
    DWORD   RVA2Offset(DWORD address);
    DWORD   VA2Offset(DWORD address);
    DWORD   Offset2RVA(DWORD offset);
    DWORD   Offset2VA(DWORD offset);
    IMAGE_NT_HEADERS    GetNTHeaders();
    bool    GetNTHeadersEx(IMAGE_NT_HEADERS &ntheaders);
    IMAGE_SECTION_HEADER*   GetSectionHeaders();
private:
    LPINT fPEDirector;
};


class TByteArray
{
public:
    TByteArray();
    explicit TByteArray(LPINT ByteArray);
    ~TByteArray();
public:
    bool IsNull();
    void Resize(int newsize);
    int GetSize();
    operator LPINT() const;
    char* operator [](int pos);
private:
    bool fNoStorage;
    LPINT fByteArray;
};

class TStream
{
public:
    TStream();
    ~TStream();
public:
    bool IsNull();
    int GetSize();
    unsigned Read(void *buffer, unsigned size);
    unsigned Write(void *buffer, unsigned size);
    unsigned GetPosition();
    unsigned SetPosition(unsigned newpos);
    operator LPINT() const;
private:
    LPINT fStream;
};

void SetLength(TByteArray& bytearray, int size);
int Length(TByteArray& bytearray);

#endif // _LIB_LANCZOSDRIVER