#ifndef _ROM_UTILS_H
#define _ROM_UTILS_H

#include "OAKRemix.h"
#include <QIODevice>
#include <QByteArray>

#ifndef __GNUC__
#define IMAGE_SCN_CNT_CODE                   0x00000020  // Section contains code.
#define IMAGE_SCN_CNT_INITIALIZED_DATA       0x00000040  // Section contains initialized data.
#define IMAGE_SCN_CNT_UNINITIALIZED_DATA     0x00000080  // Section contains uninitialized data.
#endif

#define ST_TEXT  0
#define ST_DATA  1
#define ST_PDATA 2
#define ST_RSRC  3
#define ST_OTHER 4
extern DWORD g_segmentNameUsage[5];
extern char *g_segmentNames[5];

#ifdef USENKCOMP

typedef DWORD (*CEDECOMPRESSFN)(LPBYTE BufIn, DWORD InSize, LPBYTE BufOut, DWORD OutSize, DWORD skip, DWORD n, DWORD blocksize);
extern "C" DWORD CEDecompressROM(LPBYTE BufIn, DWORD InSize, LPBYTE BufOut, DWORD OutSize, DWORD skip, DWORD n, DWORD blocksize);
extern "C" DWORD CEDecompress(LPBYTE BufIn, DWORD InSize, LPBYTE BufOut, DWORD OutSize, DWORD skip, DWORD n, DWORD blocksize);
extern CEDECOMPRESSFN cedecompress;

typedef DWORD (*CECOMPRESSFN)(LPBYTE BufIn, DWORD InSize, LPBYTE BufOut, DWORD OutSize, DWORD skip, DWORD n, DWORD blocksize);
extern "C" DWORD CECompress(LPBYTE BufIn, DWORD InSize, LPBYTE BufOut, DWORD OutSize, DWORD skip, DWORD n, DWORD blocksize);
extern CECOMPRESSFN cecompress;

#else

typedef DWORD (*CECOMPRESS)(LPBYTE  lpbSrc, DWORD cbSrc, LPBYTE lpbDest, DWORD cbDest, WORD wStep, DWORD dwPagesize);
typedef DWORD (*CEDECOMPRESS)(LPBYTE  lpbSrc, DWORD cbSrc, LPBYTE  lpbDest, DWORD cbDest, DWORD dwSkip, WORD wStep, DWORD dwPagesize);

extern "C" DWORD CECompress(LPBYTE lpbSrc, DWORD cbSrc, LPBYTE lpbDest, DWORD cbDest, WORD wStep, DWORD dwPagesize);
extern "C" DWORD CEDecompress(LPBYTE lpbSrc, DWORD cbSrc, LPBYTE lpbDest, DWORD cbDest, DWORD dwSkip, WORD wStep, DWORD dwPagesize);

extern CECOMPRESS cecompress;
extern CEDECOMPRESS cedecompress;

#endif

//extern bool g_iswince3rom;

#define CECOMPRESS_ALLZEROS 0
#define CECOMPRESS_FAILED   0xffffffffUL
#define CEDECOMPRESS_FAILED 0xffffffffUL

void WriteDummyMZHeader(QIODevice *qfile);
DWORD FindFirstSegment(const o32_rom *o32, DWORD objcnt, int segtypeflag);
DWORD CalcSegmentSizeSum(const o32_rom *o32, DWORD objcnt, int segtypeflag);
DWORD FiletimeToTime_t(const FILETIME *pft);
void WriteE32Header(QIODevice *f, const ROMHDR *rom, const e32_rom *e32, const TOCentry *t, const o32_rom *o32);
DWORD WriteO32Header(QIODevice *f, const e32_rom *e32, const o32_rom *o32);
void WriteAlignment(QIODevice *f, DWORD pagesize);
bool isObjectContainsSection(const o32_rom& o32, const info& inf);
QByteArray qDECompressMSLZX(const uchar* data, int nbytes, int outputbytes);
QByteArray qCompressMSLZX(const uchar* data, int nbytes);
#ifdef __GNUC__
extern "C" quint64 __fastcall SearchGap(LPVOID Buffer, quint32 Size);
extern "C" quint32 __fastcall CalcChecksum32(LPVOID Buffer, quint32 Size);
#else
quint64 __fastcall SearchGap(LPVOID Buffer, quint32 Size);
quint32 __fastcall CalcChecksum32(LPVOID Buffer, quint32 Size);
#endif
int AlignByBlocksize(int basesize, int blocksize);
int AlignAddressByBlocksizeCrazy(DWORD baseaddress, int basesize, int newsize, int blocksize);
bool LoadFlatBufferFromXip( QIODevice *qfile, QByteArray& FlatBuffer, DWORD& ROMBaseAddress, DWORD& ROMSize, DWORD& ROMLoadAddress );

#ifdef __GNUC__

// simple memcpy_s macro
#define memcpy_s(a1, s1, a2, s2) memcpy(a1, a2, s1 < s2? s1 : s2)

#define memmove_s(a1, s1, a2, s2) memmove(a1, a2, s1 < s2? s1 : s2)

/* _countof helper */
#if !defined(_countof)
#if !defined(__cplusplus)
#define _countof(_Array) (sizeof(_Array) / sizeof(_Array[0]))
#else
extern "C++"
{
template <typename _CountofType, size_t _SizeOfArray>
char (*__countof_helper(UNALIGNED _CountofType (&_Array)[_SizeOfArray]))[_SizeOfArray];
#define _countof(_Array) sizeof(*__countof_helper(_Array))
}
#endif
#endif

// _snprintf_s
#define _snprintf_s(a1, s1, s2, args...) _snprintf(a1, s1 < s2? s1 : s2, args)

// strcpy_s
#define strcpy_s(a1, s1, a2) strncpy(a1, a2, s1)

#endif // GNUC


#endif
