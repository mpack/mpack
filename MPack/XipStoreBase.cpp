#include "XipStore.h"
#include <cstdio>
using namespace std;


tagBundleRecItem::tagBundleRecItem()
{
    StockPresent = PreviousPresent = IncomingPresent = false;
    ItemNuked[0] = ItemNuked[1] = ItemNuked[2] = false;
}

void tagBundleRecItem::CopyMetaData( TSourceSlot src, TSourceSlot dest )
{
    if (src == dest) {
        return;
    }
    ItemType[dest] = ItemType[src];
    Stage[dest] = Stage[src];
    EntryAddress[dest] = EntryAddress[src];
    TocInfo[dest] = TocInfo[src];
    FileInfo[dest] = FileInfo[src];
    CopyInfo[dest] = CopyInfo[src]; 
    // RawBuffer discard
    Filename[dest] = Filename[src];
    Desription[dest] = Filename[src];
    E32Info[dest] = E32Info[src];
    O32Info[dest] = O32Info[src];
    // PureResource discard
    // Preset discard
    ItemNuked[dest] = ItemNuked[src];
}

void tagBundleRecItem::CopyBuffer( TSourceSlot src, TSourceSlot dest )
{
    RawBuffer[dest] = RawBuffer[src];
    PureResource[dest] = PureResource[src]; // related to raw
}

TSourceSlot GetActiveSlotForUser( TBundleRecItem& item )
{
    TSourceSlot Result = stStock;
    if (item.PreviousPresent) {
        Result = stPrevious; // Previous higher than stock
    }
    if (item.IncomingPresent) {
        Result = stIncoming; // Incoming more important than stock
    }
    return Result;
}

char* ROMHelper::GetVAData( DWORD VirtualAddress )
{
    if (VirtualAddress == 0) {
        return NULL; // semi dumprom
    }
    Q_ASSERT(VirtualAddress >= fROMBaseAddress && VirtualAddress < fROMBaseAddress + fROMSize);
    // dangerous using in FlatBuffer extending
    return fFlatBuffer.data() - fROMBaseAddress + VirtualAddress;// + ROM_SIGNATURE_OFFSET;
}

char* ROMHelper::GetOffestData( DWORD FileOffset )
{
    return fFlatBuffer.data() + FileOffset;
}

DWORD ROMHelper::VA2Offset( DWORD VirtualAddress )
{
    if (VirtualAddress == 0) {
        return 0;
    }
    return VirtualAddress - fROMBaseAddress;
}

DWORD ROMHelper::DataToVA( LPVOID Data )
{
    DWORD Result = -1;
    if (Data >= fFlatBuffer.data() && Data <= fFlatBuffer.data() + fROMSize) {
        Result = DWORD(Data) - DWORD(fFlatBuffer.data()) + fROMBaseAddress;
    }
    return Result;
}

TBundleRec& ROMHelper::GetSortedBuffer()
{
    return fSortedBuffer;
}

int ROMHelper::QueryStepMoveDelta( DWORD VirtualAddress ) {
    // Address = 18,15
    // 10, 15, 20, 25
    int Result = 0;
    if (fStepMoveRecs.empty()) {
        return Result;
    }
    /*QMap < int, TBundleMoveRecItem >::iterator it = fSortMoveRecs.lowerBound(VirtualAddress); // >= Input
    if (it == fSortMoveRecs.end()) {
    return Result;
    }
    if (it.key() == VirtualAddress) {
    //Result = it->Delta;
    // ok, Address = 15
    } else if (it != fSortMoveRecs.begin()) {
    it--;
    //Result = it->Delta;
    // ok, Address = 15, it = 20 -> 15
    } else {
    // != Address, == begin , only 1, >= Address
    // failed, Address = 8
    return Result;
    }

    do {
    Result += it->Delta;
    if (it != fSortMoveRecs.begin()){
    it--;
    }
    } while (it != fSortMoveRecs.begin());*/

    TMoveRec::iterator it;
    for (it = fStepMoveRecs.begin(); it != fStepMoveRecs.end(); it++) {
        if (it.key() > VirtualAddress) {
            // 20, 25
            break;
        } else {
            // 10, 15
            Result += it->Delta;
        }
    }
    return Result;
}

int ROMHelper::ApplyStepMoveDelta( DWORD& VirtualAddress )
{
    if (VirtualAddress == 0) {
        return 0;
    }
    int Result = QueryStepMoveDelta(VirtualAddress);
    if (Result != 0) {
        VirtualAddress += Result;
    }
    return Result;
}

int ROMHelper::QueryAreaMoveDelta( DWORD VirtualAddress ) {
    int Result = 0;
    if (fAreaMoveRecs.empty()) {
        return Result;
    }
    /*QMap < int, TBundleMoveRecItem >::iterator it = fAreaMoveRecs.lowerBound(VirtualAddress); // find a key >= Input
    if (it == fAreaMoveRecs.end()) {
        // input gather than last element
        it--; // switch to last (maybe first)
    }
    // emit an else case. lowerBound can't pass 2 gap
    if (it.key() > VirtualAddress) {
        it--;
    }
    Q_ASSERT(it.key() <= VirtualAddress);
    if (it.key() == VirtualAddress) {
        Result = it->Delta;
    } else if (it.key() < VirtualAddress) {
        Result = it->Delta;
    } else {
        // our code running in an virtual machine?
    }*/
    // TODO: Area
    for (TMoveRec::iterator it = fAreaMoveRecs.begin(); it != fAreaMoveRecs.end(); it++) {
        TMoveRec::iterator nextit = it + 1;
        if (it.key() == VirtualAddress) {
            Result = (*it).Delta;
            return Result;
        }
        if (nextit == fAreaMoveRecs.end()) {
            if (it.key() <= VirtualAddress) {
                // 15, 18, EOF
                Result = (*it).Delta;
            }
            return Result;
        }
        // nextit usable
        if (it.key() < VirtualAddress && nextit.key() > VirtualAddress) {
            Result = (*it).Delta;
            return Result;
        }
    }

    return Result;
}

int ROMHelper::ApplyAreaMoveDelta( DWORD& VirtualAddress )
{
    if (VirtualAddress == 0) {
        return 0;
    }
    int Result = QueryAreaMoveDelta(VirtualAddress);
    if (Result != 0) {
        VirtualAddress += Result;
    }
    return Result;
}

bool ROMHelper::RegisterMoveDelta( DWORD VirtualAddress, DWORD Size, int Delta, bool Brick )
{
    // TODO: brick accept
    if (VirtualAddress == 0 || (Delta == 0 && Brick == false)) {
        return false;
    }
    TMoveRec::iterator it = fStepMoveRecs.find(VirtualAddress);
    if (it == fStepMoveRecs.end()) {
        TBundleMoveRecItem item;
        item.Delta = Delta;
        item.Size = Size;
        fStepMoveRecs.insert(VirtualAddress, item);
    } else {
        (*it).Delta += Delta;
        (*it).Size = qMax((*it).Size, Size);
    }
    return true;
}

bool ROMHelper::GetProjectEmpty()
{
    return fSortedBuffer.isEmpty();
}

bool ROMHelper::GetProjectModified()
{
    return fProjectModified;
}

bool ROMHelper::UpdateProjectModifiedStatus( bool modified )
{
    fProjectModified = modified;
    return true;
}

int __cdecl ROMHelper::logprintf(const char * _Format, ... )
{
    va_list ext;

    va_start( ext, _Format );
    //vprintf(_Format, marker);
#ifdef __GNUC__
    int len = vsnprintf(NULL, 0, _Format, ext);
    // FIXME: differnt than a _vsnprintf one
#else
    int len = _vscprintf(_Format, ext);
#endif
    QByteArray buf;
    buf.resize(len + 1);
    buf.fill(0);
#ifdef __GNUC__
    len = vsnprintf(buf.data(), len, _Format, ext);
#else
    len = _vsnprintf_s(buf.data(), buf.size(), len, _Format, ext);
#endif
    va_end(ext);

    AddLog(buf, ltMessage);

    return len;
}

void ROMHelper::AddLog( QString content, TLogType logtype )
{
    emit logStored(content, logtype);
}


bool ROMHelper::MarkRegion( DWORD VirtualAddress, DWORD Size, const char * _Format, ... )
{
    va_list ext;

    va_start( ext, _Format );
    //vprintf(_Format, marker);
#ifdef __GNUC__
    int len = vsnprintf(NULL, 0, _Format, ext);
    // FIXME: differnt than a _vsnprintf one
#else
    int len = _vscprintf(_Format, ext);
#endif
    QByteArray buf;
    buf.resize(len + 1);
    buf.fill(0);
#ifdef __GNUC__
    len = vsnprintf(buf.data(), len, _Format, ext);
#else
    len = _vsnprintf_s(buf.data(), buf.size(), len, _Format, ext);
#endif
    va_end(ext);

    TRegionRecItem item;
    item.Size = Size;
    item.Description = buf;

    fRegionRecs.insert(VirtualAddress, item);

    return true;
}

TEntryType BundleType2EntryType( TBundleType bundletype )
{
    TEntryType Result = etUnkown;
    switch (bundletype) {
        case btPEFile:
        case btRawFile:
            Result = etFile;
            break;
        case btPEModule:
            Result = etModule;
            break;
        case btRawCode:
            Result = etCopy;
            break;
        case btRawGap:
            break;
    }
    return Result;
}
