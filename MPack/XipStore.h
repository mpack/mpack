#ifndef _XIP_STORE_H
#define _XIP_STORE_H

#include <cstdio>
#include <windows.h>
#include "OAKRemix.h"
#include "Common.h"
#include <QFile>
#include <QIODevice>
#include <QByteArray>
#include <QVector>
#include <QMap>
#include <QDataStream>

#pragma pack(push, 1)
typedef union tagAddressBundle {
    struct {
        DWORD Start;
        DWORD Stop;
    };
    ULONGLONG Union;
} TAddressBundle;

typedef struct tagChunkHeader {
    DWORD TargetAddress;
    DWORD SourceSize;
    DWORD Checksum32;
} TChunkHeader, *PChunkHeader;

typedef struct tagRLEXIPMBR {
    char Magic[7];
    DWORD RomAddress;
    DWORD RomSize;
} TRLEXIPMBR, *PRLEXIPMBR;

typedef struct tagAreaMark {
    DWORD Signature;
    char Padding[0x3C];
    DWORD ECEC;
    DWORD HeaderAddress;
    DWORD HeaderOffset;
} TAreaMark, *PAreaMark;

typedef struct tagAreaRec {
    DWORD MarkAddress; // 8010,0000
    TAreaMark AreaMark; // +44, +48
    bool PureSignature; // 4 ~ 40- NULL
    bool PureECEC; // 4C ~ 1000- NULL
    ROMHDR Header; // HeaderAddress stored in AreaMark
} TAreaRec, *PAreaRec;

typedef struct tagFileMatrixRec {
    DWORD ModuleLoadStart;
    DWORD ModuleLoadEnd;
    DWORD FileStoreStart;
    DWORD FileStoreEnd;
} TFileMatrixRec, *PFileMatrixRec;

typedef struct tagMeizuOEMTag {
    DWORD MajorTag; // 0.
    DWORD MinorTag; // 9.
    DWORD WeekTag;  // 3.
    DWORD PubTag;   // 7.
} TMeizuOEMTag, *PMeizuOEMTag;

#pragma pack(pop)

enum TBundleType {
    btPEFile, btRawFile, btPEModule, btRawCode, btRawGap
};

enum TEntryType {
    etUnkown = -1, etModule, etFile, etCopy
};

enum TSourceSlot {
    stStock, stPrevious, stIncoming, stWork, stCount
};

enum TROMInfoIndex {
    riiBassAddress, riiSize, riiLoadAddress, riiLoadAddressOverided, riiXipChainAddress,
};

// Project Management
typedef struct tagBundleRecItem {
    TBundleType ItemType[stCount];
    int Stage[stCount];
    DWORD EntryAddress[stCount];
    TOCentry TocInfo[stCount]; // Module, header
    FILESentry FileInfo[stCount]; // filename missed
    COPYentry CopyInfo[stCount]; // RawCode
    QByteArray RawBuffer[stCount];
    QByteArray RawHash[stCount]; // opt. not store in project
    QByteArray Filename[stCount]; // for FilesEntry, TOCEntry
    QString Desription[stCount];
    e32_rom E32Info[stCount]; // module, content
    QVector < o32_rom > O32Info[stCount]; // module, content
    bool PureResource[stCount];
    bool ItemNuked[stCount];
    bool StockPresent, PreviousPresent, IncomingPresent;
    tagBundleRecItem();
    void CopyMetaData(TSourceSlot src, TSourceSlot dest);
    void CopyBuffer(TSourceSlot src, TSourceSlot dest);
} TBundleRecItem, *PBundleRecItem;

typedef QVector < TBundleRecItem > TBundleRec;

typedef struct tagROMExtRecItem {
    DWORD EntryAddress;
    int HeaderIndex;
    ROMPID ExtEntry;
    QByteArray ExtData;
} TROMExtRecItem, *PRomExtRecItem;

typedef struct tagBundleMoveRecItem {
    DWORD Size;
    DWORD Gap;
    int Delta;
} TBundleMoveRecItem, *PBundleMoveRecItem;

typedef QMap < DWORD, TBundleMoveRecItem > TMoveRec;

typedef struct tagRegionRecItem {
    DWORD Size;
    QString Description;
} TRegionRecItem;

class ROMHelper : public QObject {
    Q_OBJECT

public:
    ROMHelper();
protected:
    QByteArray fFlatBuffer; // used for speedup parse Xip inputs
    TBundleRec fSortedBuffer;
    TMoveRec fStepMoveRecs;
    TMoveRec fAreaMoveRecs;
    QMap < DWORD, TRegionRecItem > fRegionRecs; // Hint only?
private:
    DWORD fROMBaseAddress, fROMSize, fROMLoadAddress, fROMLoadAddressOverided;
    DWORD fROMXipChainAddress;
    QVector < TAreaRec > fROMHeaders;
    QVector < TROMExtRecItem > fExtRecs; // may share with two headers, or only used in Main Stage
    QVector < XIPCHAIN_ENTRY > fXipChainRecs; // count must equal to headers. position may reversed
    bool fReadOnlyMode, fNoChainMode; // no chain is not equal single chain
protected:
    bool fProjectModified;
private:
    char* GetVAData(DWORD VirtualAddress);
    char* GetOffestData(DWORD FileOffset);
    DWORD VA2Offset(DWORD VirtualAddress);
    DWORD DataToVA(LPVOID Data);
    int __cdecl logprintf(const char * _Format, ...);
    void AddLog(QString content, TLogType logtype = ltMessage);
    bool MarkRegion(DWORD VirtualAddress, DWORD Size, const char * _Format, ...);
    bool RegisterMoveDelta(DWORD VirtualAddress, DWORD Size, int Delta, bool brick = false);
    int QueryStepMoveDelta(DWORD VirtualAddress);
    int ApplyStepMoveDelta(DWORD& VirtualAddress);
    int QueryAreaMoveDelta(DWORD VirtualAddress);
    int ApplyAreaMoveDelta(DWORD& VirtualAddress);
    void CreateOriginalFile(QIODevice* f, QString filename, const ROMHDR *rom, const TOCentry *t, const e32_rom *e32, const o32_rom *o32);
    DWORD WriteUncompressedData(QIODevice *f, DWORD dataptr, DWORD datasize, BOOL bCompressed, DWORD maxUncompressedSize);
signals:
    void logStored(QString content, TLogType logtype);
    void xipProgressChanged(int percent);
    void prepareBufferProgressChanged(int percent);
    void prepareBufferStageChanged(QString stage);
public:
    bool LoadFlatBufferFromXip(QString xipfilename);
    bool ParseFlatBuffer(bool update = false);
    bool SaveParsedProjectToFile(QString mpkfilename, bool compress = true);
    bool LoadProjectFromFile(QString mpkfilename);
    bool PrepareFlatBufferFromProjectGroovy();
    bool PrepareFlatBufferFromProjectSemiStock();
    bool GenerateXipFromFlatBuffer(QString xipfilename, int compression);
    bool GetProjectEmpty();
    bool GetProjectModified();
    bool UpdateProjectModifiedStatus(bool modified);
    TBundleRec& GetSortedBuffer();
    DWORD ReadROMInfo(TROMInfoIndex index);
    int QueryNKIndex();
    int SearchOEMTagInNK(QByteArray& data);
    TMeizuOEMTag ReadOEMTag();
    bool WriteOEMTag(const TMeizuOEMTag newtag);
};

TEntryType BundleType2EntryType(TBundleType bundletype);
TSourceSlot GetActiveSlotForUser(TBundleRecItem& item);

// Instead of inheritance
QDataStream &operator<<(QDataStream &out, const DWORD &w);
QDataStream &operator<<(QDataStream &out, const TAreaRec &area);
QDataStream &operator<<(QDataStream &out, const TROMExtRecItem &ext);
QDataStream &operator<<(QDataStream &out, const ROMPID &ext);
QDataStream &operator<<(QDataStream &out, const XIPCHAIN_ENTRY &xip);
QDataStream &operator<<(QDataStream &out, const TBundleRecItem &item);
QDataStream &operator<<(QDataStream &out, const e32_rom &e32);
QDataStream &operator<<(QDataStream &out, const o32_rom &o32);
QDataStream &operator<<(QDataStream &out, const TOCentry &toc);
QDataStream &operator<<(QDataStream &out, const COPYentry &copy);
QDataStream &operator<<(QDataStream &out, const FILESentry &files);

QDataStream &operator>>(QDataStream &in, DWORD &w);
QDataStream &operator>>(QDataStream &in, TAreaRec &area);
QDataStream &operator>>(QDataStream &in, TROMExtRecItem &ext);
QDataStream &operator>>(QDataStream &in, ROMPID &ext);
QDataStream &operator>>(QDataStream &in, XIPCHAIN_ENTRY &xip);
QDataStream &operator>>(QDataStream &in, TBundleRecItem &item);
QDataStream &operator>>(QDataStream &in, e32_rom &e32);
QDataStream &operator>>(QDataStream &in, o32_rom &o32);
QDataStream &operator>>(QDataStream &in, TOCentry &toc);
QDataStream &operator>>(QDataStream &in, COPYentry &copy);
QDataStream &operator>>(QDataStream &in, FILESentry &files);


#endif