#ifndef MAINFRM_H
#define MAINFRM_H

#include <QtGui/QMainWindow>
#include "ui_MainFrm.h"
#include <QtGUI/QFileDialog>
#include <QtCore/QPoint>
#include <QtGUI/QCloseEvent>
#include <QtGUI/QProgressBar>
#include "Common.h"

class CERegistryKey;

class TMainFrm : public QMainWindow
{
    Q_OBJECT

public:
    TMainFrm(QWidget *parent = 0, Qt::WFlags flags = 0);
    ~TMainFrm();
private:
    Ui::mainFrm ui;
    QFileDialog OpenSaveFileDialog, OpenSaveXipDialog, OpenSaveMpkDialog;
    QString fLastSaveMPackFilename;
    QString fMeizuOEMTagExpr;
    QMap < QString, int > fEditingHives;
    QTranslator* fTranslator;
    QString fLastLangCode;
protected:
    void closeEvent(QCloseEvent *event);
private:
    void TryAcceptSource( QFileInfo &info, QSet< int > &processedindex, int maxdepth, int &bypassoucnt, int &replacedcount, int &acceptcount );
    void TestDumpKey( CERegistryKey &topkey );
private slots:
    void onFileExitClicked();
    void onFileNewClicked();
    void onFileOpenClicked();
    void onFileCloseClicked();
    void onFileSaveClicked();
    void onFileSaveAsClicked();
    void onProjectExtractSingleClicked();
    void onProjectDumpAllClicked();
    void onProjectBuildOutputXipClicked();
    void onProjectReplaceClicked();
    void onProjectDeleteClicked();
    void onProjectRestoreClicked();
    void onProjectRollbackClicked();
    void onProjectAddClicked();
    void onProjectRenameClicked();
    void onProjectViewRightclick(const QPoint& point);
    void onProjectViewItemDoubleClicked(QTreeWidgetItem*,int);
    void onRegeditorHiveModified(QString filename, bool modified, bool nukefile );
    void onProjectDropfiles(const QMimeData* data = 0);
    void onProjectExtraInfoClicked();
    void onExtraInfoAccepted();
    void onExtraInfoRejected();
    void onMeizuTagModified(QString);
    void onProjectFindPatternChanged();
    void onTranslateFilterChanged();
    void onProjectSortDisabled(int);
    void onLanguageEnglishClicked();
    void onLanguageChineseClicked();
    void onLanguageDutchClicked();
    void onHelpAboutClicked();
    void onHelpContentsClicked();
    void onToolsLiteXipBuilderClicked();
    void onToolsLiteFactroyClicked();
public slots:
    void writeLog(QString content, TLogType logtype = ltMessage);
public:
    void StoreTranslator(QTranslator* translator);
};

extern TMainFrm* mainfrm;

QString LogTypeToString(TLogType logtype);
QString localLanguage();

#endif // MAINFRM_H
