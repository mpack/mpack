#ifndef _PLATFORMBUILDER_IMAGEUTILS_H
#define _PLATFORMBUILDER_IMAGEUTILS_H

#include <QString>
#include <QByteArray>
#include <QStringList>
#include <basetyps.h>
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

// we use VOL_CREATE_NEW instead CREATE_NEW to prevent define conflict
enum VolumeFileFlag {
    VOL_CREATE_NEW = 1,
    VOL_CREATE_ALWAYS = 2,
    VOL_OPEN_EXISTING = 3,
    VOL_OPEN_ALWAYS = 4,
    VOL_TRUNCATE_EXISTING = 5,
    VOL_OPEN_FOR_LOADER = 6,
    VOL_HIVE_FLAG_DISABLE_FLUSH = 65536,
};

enum VolumeOpenFlag : unsigned short {
    FSVOL_DEFAULT_FLAGS = 0,
    FSVOL_TYPE_FILE = 1,
    FSVOL_TYPE_DATABASE = 2,
    FSVOL_TYPE_REGISTRY = 4,
    FSVOL_TYPE_VIRTUAL = 8,
    FSVOL_TYPE_SINGLEINST = 16,
    FSVOL_TYPE_READONLY = 32,
    FSVOL_TYPE_RAMMAP = 64,
    FSVOL_TYPE_NOPREFILL = 128,
    FSVOL_TYPE_NOCOMPACTOR = 256,
};

// we use CE_REG_NONE to keep out WinNT.h
enum RegistryValueDataType
{
    CE_REG_NONE = 0,
    CE_REG_SZ = 1,
    CE_REG_EXPAND_SZ = 2,
    CE_REG_BINARY = 3,
    CE_REG_DWORD = 4,
    CE_REG_LINK = 6,
    CE_REG_MULTI_SZ = 7,
    CE_REG_MUI_SZ = 21,
};

QString ValueType2Str(RegistryValueDataType type);

struct Fshasht {
    quint32 Hashdata1;
    quint32 Hashdata2;
    quint32 Hashdata3;
    quint32 Hashdata4;
};

// C# style
/*
extern int CEREG_RegCloseKey(UIntPtr key);
extern int CEREG_RegEnumKeyEx(UIntPtr key, uint index, StringBuilder name, ref uint nameSize, IntPtr reserved, IntPtr className, IntPtr classNameSize, IntPtr lastWriteTime);
extern int CEREG_RegEnumValue(UIntPtr key, uint index, StringBuilder valueName, ref uint valueNameSize, ref uint reserved, ref RegistryValueDataType type, byte[] data, ref uint dataSize);
extern uint CEREG_RegMountHive(ref Guid pguid, string vol, UIntPtr hiveRAMInfo, CERegistry.VolumeFileFlag fileFlags, CERegistry.VolumeOpenFlag volFlags, ref Fshasht romSignature, uint initialSize);
extern int CEREG_RegOpenKeyEx(UIntPtr key, string subKey, uint options, CERegistry.REGSAM samDesired, ref UIntPtr keyResult);
extern int CEREG_RegQueryInfoKey(UIntPtr key, IntPtr className, IntPtr classSize, IntPtr reserved, out uint subKeys, out uint maxSubKeyLen, out uint maxClassLen, out uint values, out uint maxValueNameLen, out uint maxValueLen, out uint securityDescriptor, out FILETIME lastWriteTime);
extern int CEREG_RegQueryValueEx(UIntPtr key, string valueName, ref uint reserved, ref RegistryValueDataType type, byte[] data, ref uint dataSize);
extern int CEREG_RegSetHKEYActiveHive(UIntPtr key, uint hiveidROM, uint hiveidRAM);
extern int CEREG_RegUnmountHive(uint hive);
*/

// M$ mixed style. out use ref, NULL use ptr
extern "C" quint32  CEREG_RegMountHive(GUID& pguid, const ushort* vol, quint32* hiveRAMInfo, VolumeFileFlag fileFlags, VolumeOpenFlag volFlags, Fshasht& romSignature, uint initialSize);
extern "C" qint32   CEREG_RegUnmountHive(uint hive);
//CEREG_RegCompareHive
//CEREG_RegTrimHive
extern "C" qint32   CEREG_RegOpenKeyEx(HKEY key, const ushort* subKey, uint options, REGSAM samDesired, HKEY& keyResult);
extern "C" qint32   CEREG_RegCloseKey(HKEY key);
extern "C" qint32   CEREG_RegCreateKeyEx(HKEY key, const ushort* subKey, uint reserved, const ushort* className, uint options, REGSAM samDesired, LPSECURITY_ATTRIBUTES securityAttributes, HKEY& keyResult, uint& disposition);
extern "C" qint32   CEREG_RegDeleteKey(HKEY key, const ushort* subKey);
extern "C" qint32   CEREG_RegFlushKey(HKEY key);
extern "C" qint32   CEREG_RegQueryInfoKey(HKEY key, int* className, int* classSize, int* reserved, uint* subKeys, uint* maxSubKeyLen, uint* maxClassLen, uint* values, uint* maxValueNameLen, uint* maxValueLen, uint* securityDescriptor, FILETIME* lastWriteTime);
extern "C" qint32   CEREG_RegEnumKeyEx(HKEY key, uint index, const ushort* name, quint32& nameSize, int* reserved, const ushort* className, int* classNameSize, int* lastWriteTime);
extern "C" qint32   CEREG_RegEnumValue(HKEY key, uint index, const ushort* valueName, quint32& valueNameSize, quint32& reserved, RegistryValueDataType* type, uchar* data, quint32& dataSize);
extern "C" qint32   CEREG_RegQueryValueEx(HKEY key, const ushort* valueName, quint32* reserved, RegistryValueDataType* type, uchar* data, quint32* dataSize);
extern "C" qint32   CEREG_RegSetValueEx(HKEY hKey, const ushort* ValueName, uint reserved, RegistryValueDataType type, const uchar* data, uint dataSize);
extern "C" qint32   CEREG_RegDeleteValue(HKEY key, const ushort* ValueName);
extern "C" qint32   CEREG_RegSetHKEYActiveHive(HKEY key, uint hiveidROM, uint hiveidRAM);
//CEREG_RegSaveDeltaRGUFile
extern "C" void     CEREG_Initialize();

class CERegistryKey;
class CERegistryValue;

class CERegistryHive {
private:
    QString fFilename;
    uint fHiveID;
    GUID fGuid;
public:
    explicit CERegistryHive(uint hiveID, GUID guid, QString filename);
public:
    static CERegistryHive OpenHive(QString fileName, bool writeable);
    CERegistryKey OpenRootKey(CERegistryKey rootKey);
    void Close();
public:
    uint HiveID();
};

class CERegistryKey {
private:
    bool fDisposed;
    HKEY fKey;
    QString fSubName;
    QString fParentPath;
protected:
    int fRef;
public:
    explicit CERegistryKey(HKEY hkey, QString name, QString parentname);
    CERegistryKey();
    ~CERegistryKey();
    //CERegistryKey &operator=(const CERegistryKey &other);
public:
    void Close();
    int GetSubKeyCount();
    QStringList GetSubKeyNames();
    int GetValueCount();
    CERegistryValue GetValue(QString valueName);
    QList<CERegistryValue> GetValues();
    CERegistryKey OpenSubKey(QString subkeyName);
public:
    // More than ImageUtils
    bool SetValue(CERegistryValue& newValue);
    bool DeleteValue(QString valueName);
    CERegistryKey CreateKey(QString subkeyName);
    bool DeleteKey(QString subkeyName);
public:
    HKEY Handle();
    QString Name();
    QString SubName();
};

class CERegistryValue {
private:
    QByteArray fData;
    QString fName;
    RegistryValueDataType fType;
public:
    explicit CERegistryValue(QString name, RegistryValueDataType type, QByteArray data);
public:
    QString Name();
    QString Brief();
    RegistryValueDataType Type();
    QByteArray& Data(); // Modify qua
};

QString Data2Brief(QByteArray& data, RegistryValueDataType type);

class CERegistry {
private:
    static CERegistryKey fClassesRoot;
    static CERegistryKey fCurrentUser;
    static CERegistryKey fLocalMachine;
    static CERegistryKey fUsers;
public:
    CERegistry();
public:
    static CERegistryKey ClassesRoot();
    static CERegistryKey CurrentUser();
    static CERegistryKey LocalMachine();
    static CERegistryKey Users();
public:
    static void SetRootKeyActiveHive(CERegistryKey& key, CERegistryHive& hive); // prevent copy
};

#endif
