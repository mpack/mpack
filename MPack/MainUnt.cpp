#include "MainUnt.h"
#include "NekoDriverUnt.h"
#include "ui_AboutFrm.h"
#include "ui_ROMInfoFrm.h"
#include "RegEdtUnt.h"
#include "LiteXipBuilderUnt.h"
#include "DbCentre.h"
#include <FvQKOL.h>
#include "PlatformBuilderImageUtils.h"
#include <QtCore/QTranslator>
#include <QtCore/QFileInfo>
#include <QtCore/QDatetime>
#include <QtGUI/QMessageBox>
#include <QtGUI/QInputDialog>
#include <QtGUI/QCleanLooksStyle>
#include <QtGUI/QSizePolicy>
#include <QtCore/QDir>
#include <QtCore/QProcess>
#include <QtGUI/QDesktopServices>


TMainFrm* mainfrm = 0;

TMainFrm::TMainFrm(QWidget *parent, Qt::WFlags flags)
    : QMainWindow(parent, flags)
{
    ui.setupUi(this);

    LoadAppSettings();
    // Additionally Initialize
    if (StateSetting.WindowMaxium) {
        setWindowState(Qt::WindowMaximized);
    }
    if (StateSetting.MainFrmState.isEmpty() == false) {
        restoreState(StateSetting.MainFrmState);
    }
    if (StateSetting.ProjectLayoutState.isEmpty() == false) {
        ui.splitterforproject->restoreState(StateSetting.ProjectLayoutState);
    }
    if (StateSetting.MessageLayoutState.isEmpty() == false) {
        ui.splitterforoutput->restoreState(StateSetting.MessageLayoutState);
    }
    ui.logView->horizontalHeader()->resizeSection(0, 60);
    ui.logView->horizontalHeader()->resizeSection(1, 80);
    //ui.logView->horizontalHeader()->setStretchLastSection(true);

    // komaho do koneko
    // TODO: move initialize to main
    nekodriver = new TNekoDriver(this);

    // QtDesigner const bug
    //QObject::connect(ui.projectView, SIGNAL(dropChanged(const QMimeData*)),
    //    this, SLOT(onProjectDropfiles(const QMimeData*)));
    QApplication::setApplicationName(tr("MPack"));
    //setWindowTitle("");
}

TMainFrm::~TMainFrm()
{
    StateSetting.WindowMaxium = windowState() == Qt::WindowMaximized;
    StateSetting.MainFrmState = saveState();
    StateSetting.ProjectLayoutState = ui.splitterforproject->saveState();
    StateSetting.MessageLayoutState = ui.splitterforoutput->saveState();
    SaveAppSettings();

    delete nekodriver;
}

void TMainFrm::closeEvent( QCloseEvent *event )
{
    // check save
    if (nekodriver->IsProjectEmpty() == false) {
        if (nekodriver->IsProjectModified()) {
            QMessageBox msgBox;
            msgBox.setText(tr("The project has been modified."));
            msgBox.setInformativeText(tr("Do you want to save your changes?"));
            msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
            msgBox.setDefaultButton(QMessageBox::Save);
            int ret = msgBox.exec();
            if (ret == QMessageBox::Save) {
                /*if (onFileSaveClicked()) {
                    event->accept();
                } else {
                    event->ignore();
                }*/
                onFileSaveClicked(); // TODO: bool
                event->accept();
            } else if (ret == QMessageBox::Discard) {
                event->accept();
            } else if (ret == QMessageBox::Cancel) {
                event->ignore();
            }
        } else {
            event->accept();
        }
    } else {
        event->accept();
    }
}

void TMainFrm::onFileExitClicked()
{
    this->close();
}

void TMainFrm::onFileOpenClicked()
{
    QString mpackfilename = OpenSaveMpkDialog.getOpenFileName(this, tr("Open MPack Project File"), PathSetting.LastProjectFolder, "MPack Files (*.mpack);;All Files (*.*)", 0, 0);
    if (mpackfilename.isEmpty()) {
        return;
    }
    if (nekodriver->LoadProjectFromFile(mpackfilename) == false) {
        return;
    }
    fLastSaveMPackFilename = mpackfilename;
    // Project Build Success
    nekodriver->KeepProjectView(ui.projectView);
    PathSetting.LastProjectFolder = QFileInfo(mpackfilename).path();
    //setWindowFilePath(QFileInfo(mpackfilename).fileName());
    setWindowTitle(QApplication::applicationName() + " - " + QFileInfo(mpackfilename).fileName() + "[*]");
    setWindowModified(false);
}

void TMainFrm::onFileCloseClicked()
{

}

void TMainFrm::onFileSaveClicked()
{
    if (fLastSaveMPackFilename.isEmpty() == false) {
        nekodriver->SaveProjectToFile(fLastSaveMPackFilename);
        setWindowModified(false);
    } else {
        // ask one
        onFileSaveAsClicked();
    }
}

void TMainFrm::onFileSaveAsClicked()
{
    QString mpackfilename = OpenSaveMpkDialog.getSaveFileName(this, tr("Save MPack Project File as"), PathSetting.LastProjectFolder, "MPack Files (*.mpack);;All Files (*.*)", 0, 0);
    if (mpackfilename.isEmpty()) {
        return;
    }
    nekodriver->SaveProjectToFile(mpackfilename);
    setWindowModified(false);
    fLastSaveMPackFilename = mpackfilename;
    PathSetting.LastProjectFolder = QFileInfo(mpackfilename).path();
    //setWindowFilePath(QFileInfo(mpackfilename).fileName());
    setWindowTitle(QApplication::applicationName() + " - " + QFileInfo(mpackfilename).fileName() + "[*]");
}

void TMainFrm::onFileNewClicked()
{
    // Build Project From Stock Xip
    QString xipfilename = OpenSaveXipDialog.getOpenFileName(this, tr("Select Stock Xip File"), PathSetting.LastStockXipFolder + "/xip.bin", "Xip Files (*.bin);;All Files (*.*)", 0, 0);
    if (xipfilename.isEmpty()) {
        return;
    }
    if (nekodriver->BuildProjectFromXip(xipfilename) == false) {
        return;
    }
    // Project Build Success
    nekodriver->KeepProjectView(ui.projectView);
    PathSetting.LastStockXipFolder = QFileInfo(xipfilename).path();
    fLastSaveMPackFilename.clear();
    //setWindowFilePath(tr("UntitledMod.mpack"));
    setWindowTitle(QApplication::applicationName() + " - UntitledMod.mpack[*]");
    setWindowModified(true);
}

void TMainFrm::onProjectExtractSingleClicked()
{
    // retrieve selected files in projectView
    QList < QTreeWidgetItem* > selecteditems = ui.projectView->selectedItems();
    if (selecteditems.isEmpty()) {
        return;
    }
    foreach(QTreeWidgetItem* currentitem, selecteditems) {
        int selectbundleindex = currentitem->data(0, Qt::UserRole).toInt();
        TBundleRecItem* item = nekodriver->GetBundleItemInfo(selectbundleindex);
        if (item == NULL) {
            continue;
        }
        TSourceSlot sourceslot = GetActiveSlotForUser(*item);
        QString refname = item->Filename[sourceslot];
        QString extlist = "All Files (*.*)"; // TODO: refactory
        if (refname.contains('.')) {
            QString ext = refname.right(refname.length() - refname.lastIndexOf('.') - 1);
            QString niceext = ext.left(1).toUpper() + ext.right(ext.length() - 1).toLower();
            extlist.prepend(niceext + " Files (*." + ext + ");;");
        }
        QString singlefilename = OpenSaveXipDialog.getSaveFileName(this, tr("Extract Single File"), PathSetting.LastDumpFolder + "/" + refname, extlist, 0, 0);
        if (singlefilename.isEmpty()) {
            break;
        } else {
            PathSetting.LastDumpFolder = QFileInfo(singlefilename).path();
            // TODO: save local varfilename
            // TODO: extract into same folder
            // TODO: replace promot
        }
        QFile file(singlefilename);
        file.open(QFile::ReadWrite);
        nekodriver->DumpRefBinaray(selectbundleindex, &file, false);
        file.close();
    }
}

void TMainFrm::onProjectDumpAllClicked()
{
    if (nekodriver->IsProjectEmpty()) {
        return;
    }
    QString outputpath = OpenSaveXipDialog.getExistingDirectory(this, tr("Select output Directory"), PathSetting.LastDumpFolder, QFileDialog::ShowDirsOnly);
    if (outputpath.isEmpty()) {
        return;
    }
    ui.actionDump->setEnabled(false);
    QLabel action(tr("Dumping Stock ROM..."));
    action.setMinimumSize(120, 14);
    action.setMaximumSize(200, 18);
    action.setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
    QProgressBar progress;
    progress.setMinimumSize(250, 14);
    progress.setMaximumSize(400, 18);
    progress.setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);
    progress.setStyle(new QCleanlooksStyle());
    ui.statusBar->addWidget(&action);
    ui.statusBar->addWidget(&progress);
    nekodriver->DumpProjectContent(outputpath, &action, &progress);
    ui.statusBar->removeWidget(&action);
    ui.statusBar->removeWidget(&progress);
    PathSetting.LastDumpFolder = outputpath;
    ui.actionDump->setEnabled(true);
}

void TMainFrm::onProjectBuildOutputXipClicked()
{
    if (nekodriver->IsProjectEmpty()) {
        return;
    }
    // Build Project From Stock Xip
    QString xipfilename = OpenSaveXipDialog.getSaveFileName(this, tr("Create Custom Xip File"), PathSetting.LastCustomXipFolder + "/xip.bin", "Xip Files (*.bin);;All Files (*.*)", 0, 0);
    if (xipfilename.isEmpty()) {
        return;
    }
    ui.actionBuildsketch->setVisible(false);
    QLabel action(tr("Prepare Buffer..."));
    action.setMinimumSize(120, 14);
    action.setMaximumSize(200, 18);
    action.setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
    QProgressBar progress;
    progress.setMinimumSize(250, 14);
    progress.setMaximumSize(400, 18);
    progress.setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);
    progress.setStyle(new QCleanlooksStyle());
    ui.statusBar->addWidget(&action);
    ui.statusBar->addWidget(&progress);
    if (nekodriver->PrepareBufferStock(&action, &progress) == false) {
        ui.statusBar->removeWidget(&action);
        ui.statusBar->removeWidget(&progress);
        return;
    }
    action.setText(tr("Compressing..."));
    bool xipbuilded = nekodriver->BuildXipFromBuffer(xipfilename, 2, &progress);
    ui.statusBar->removeWidget(&action);
    ui.statusBar->removeWidget(&progress);
    if (xipbuilded == false){
        return;
    }

    PathSetting.LastCustomXipFolder = QFileInfo(xipfilename).path();
    ui.actionBuildsketch->setVisible(true);
}

void TMainFrm::onProjectReplaceClicked()
{
    // retrieve selected files in projectView
    QList < QTreeWidgetItem* > selecteditems = ui.projectView->selectedItems();
    if (selecteditems.isEmpty()) {
        return;
    }
    bool changed = false;
    foreach(QTreeWidgetItem* currentitem, selecteditems) {
        int selectbundleindex = currentitem->data(0, Qt::UserRole).toInt();
        TBundleRecItem* item = nekodriver->GetBundleItemInfo(selectbundleindex);
        if (item == NULL) {
            continue;
        }
        TSourceSlot sourceslot = GetActiveSlotForUser(*item);
        QString refname = item->Filename[sourceslot];
        QString extlist = "All Files (*.*)";
        if (refname.contains('.')) {
            QString ext = refname.right(refname.length() - refname.lastIndexOf('.') - 1);
            QString niceext = ext.left(1).toUpper() + ext.right(ext.length() - 1).toLower();
            extlist.prepend(niceext + " Files (*." + ext + ");;");
        }
        QString sourcefilename = OpenSaveFileDialog.getOpenFileName(this, tr("Select File to Replace Select item"), PathSetting.LastSourceFolder + "/" + refname, extlist, 0, 0);
        if (sourcefilename.isEmpty()) {
            break;
        }
        QFile file(sourcefilename);
        file.open(QFile::ReadOnly);
        QByteArray filecontent = file.readAll();
        file.close();
        PathSetting.LastSourceFolder = QFileInfo(sourcefilename).path();
        if (nekodriver->ReplaceBundleRecContent(selectbundleindex, filecontent)) {
            changed = true;
        }
    }

    if (changed) {
        nekodriver->KeepProjectView(ui.projectView);
        setWindowModified(true);
    }
}

void TMainFrm::onProjectDeleteClicked()
{
    // retrieve selected files in projectView
    QList < QTreeWidgetItem* > selecteditems = ui.projectView->selectedItems();
    if (selecteditems.isEmpty()) {
        return;
    }
    bool changed = false;
    foreach(QTreeWidgetItem* currentitem, selecteditems) {
        int selectbundleindex = currentitem->data(0, Qt::UserRole).toInt();
        TBundleRecItem* item = nekodriver->GetBundleItemInfo(selectbundleindex);
        if (item == NULL) {
            continue;
        }
        TSourceSlot sourceslot = GetActiveSlotForUser(*item);
        if (item->ItemNuked[sourceslot]) {
            continue;
        } else {
            if (nekodriver->MarkBundleRecAsDeleted(selectbundleindex)) {
                changed = true; // don't use || in one line
            }
        }
    }

    if (changed) {
        nekodriver->KeepProjectView(ui.projectView);
        setWindowModified(true);
    }
}

void TMainFrm::onProjectRestoreClicked()
{
    // retrieve selected files in projectView
    // TODO: refactory check n foreach codes
    QList < QTreeWidgetItem* > selecteditems = ui.projectView->selectedItems();
    if (selecteditems.isEmpty()) {
        return;
    }
    bool changed = false;
    foreach(QTreeWidgetItem* currentitem, selecteditems) {
        int selectbundleindex = currentitem->data(0, Qt::UserRole).toInt();
        TBundleRecItem* item = nekodriver->GetBundleItemInfo(selectbundleindex);
        if (item == NULL) {
            continue;
        }
        TSourceSlot sourceslot = GetActiveSlotForUser(*item);
        if (item->ItemNuked[sourceslot]) {
            if (nekodriver->ClearBundleRecDeletedMark(selectbundleindex)) {
                changed = true; // don't use || in one line
            }
        } else {
            continue;
        }
    }
    if (changed) {
        nekodriver->KeepProjectView(ui.projectView);
        setWindowModified(true);
    }
}

void TMainFrm::onProjectRollbackClicked()
{
    QList < QTreeWidgetItem* > selecteditems = ui.projectView->selectedItems();
    if (selecteditems.isEmpty()) {
        return;
    }
    bool changed = false;
    foreach(QTreeWidgetItem* currentitem, selecteditems) {
        int selectbundleindex = currentitem->data(0, Qt::UserRole).toInt();
        TBundleRecItem* item = nekodriver->GetBundleItemInfo(selectbundleindex);
        if (item == NULL) {
            continue;
        }
        TSourceSlot sourceslot = GetActiveSlotForUser(*item);
        if (sourceslot != stStock && item->StockPresent) {
            if (nekodriver->RollbackToStockSlot(selectbundleindex)) {
                changed = true; // don't use || in one line
            }
        } else {
            // TODO: rollback user added item
            if (nekodriver->RemoveUserFile(selectbundleindex)) {
                changed = true;
            }
            continue;
        }
    }
    if (changed) {
        nekodriver->KeepProjectView(ui.projectView);
        setWindowModified(true);
    }
}

void TMainFrm::onProjectAddClicked()
{
    bool changed = false;
    QString sourcefilename = OpenSaveFileDialog.getOpenFileName(this, tr("Select File to Add into ROM"), PathSetting.LastStockXipFolder, "All Files (*.*)", 0, 0);
    if (sourcefilename.isEmpty()) {
        return;
    }
    QFileInfo info(sourcefilename);
    int bundleindex = nekodriver->GetBundleIndexByName(info.fileName());
    QFile file(sourcefilename);
    file.open(QFile::ReadOnly);
    QByteArray filecontent = file.readAll();
    file.close();
    if (bundleindex != -1) {
        // exists file
        // TODO: Confirm replace
        TBundleRecItem* item = nekodriver->GetBundleItemInfo(bundleindex);
        if (item == NULL) {
            return;
        }
        QMessageBox msgBox;
        msgBox.setText(tr("Replace Confirm"));
        msgBox.setInformativeText(tr("Do you want to replace exist item?\nIgnore this message will make older one inaccessable in M8."));
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Ignore);
        msgBox.setDefaultButton(QMessageBox::Yes);
        int ret = msgBox.exec();
        if (ret == QMessageBox::Yes) {
            if (nekodriver->ReplaceBundleRecContent(bundleindex, filecontent)) {
                changed = true;
            }
        } else if (ret == QMessageBox::Ignore) {
            // Add anyway?
            bundleindex = -1;
        }
    }
    if (bundleindex == -1) {
        QStringList items;
        items << tr("Main") << tr("Addon");

        bool ok;
        QString item = QInputDialog::getItem(this, tr("Select Target Area"),
            tr("Area:"), items, 0, false, &ok);
        if (ok == false || item.isEmpty()) {
            return;
        }
        int AreaIndex = 0;
        if (item == "Main") {
            AreaIndex = 0;
        } else if (item == "Addon") {
            AreaIndex = 1;
        }

        FILETIME modifytime;
        // Read or convert from QDatetime
#ifdef _LINUX
        *((quint64 *)(&modifytime)) = qint64(info.lastModified().toTime_t()) * 10000000LL + 0x019DB1DED53E8000LL;
#else
        HANDLE dummyHandle = CreateFileW(
            (WCHAR*)sourcefilename.utf16(), FILE_READ_ATTRIBUTES, FILE_SHARE_READ, NULL, OPEN_EXISTING,
            FILE_ATTRIBUTE_NORMAL, 0
        );

        GetFileTime(dummyHandle, NULL, NULL, &modifytime);
        CloseHandle(dummyHandle);
#endif
        if (nekodriver->AddUserFileToProject(AreaIndex, info.fileName().toAscii(), filecontent, modifytime)) {
            changed = true;
        }
    }

    if (changed) {
        nekodriver->KeepProjectView(ui.projectView);
        setWindowModified(true);
    }

}

void TMainFrm::onProjectRenameClicked()
{
    // retrieve selected files in projectView
    QList < QTreeWidgetItem* > selecteditems = ui.projectView->selectedItems();
    if (selecteditems.isEmpty()) {
        return;
    }
    bool changed = false;
    foreach(QTreeWidgetItem* currentitem, selecteditems) {
        int selectbundleindex = currentitem->data(0, Qt::UserRole).toInt();
        TBundleRecItem* item = nekodriver->GetBundleItemInfo(selectbundleindex);
        if (item == NULL) {
            continue;
        }
        TSourceSlot sourceslot = GetActiveSlotForUser(*item);
        QString refname = item->Filename[sourceslot];
        bool ok;
        QString newname = QInputDialog::getText(this, tr("Rename") + " " + refname,
            tr("new filename:"), QLineEdit::Normal,
            refname, &ok);
        if (newname.isEmpty() || ok == false) {
            break;
        }
        if (nekodriver->RenameBundleRec(selectbundleindex, newname.toAscii())) {
            changed = true;
        }
    }

    if (changed) {
        nekodriver->KeepProjectView(ui.projectView);
        setWindowModified(true);
    }
}

void TMainFrm::onProjectViewItemDoubleClicked( QTreeWidgetItem* currentitem, int )
{
    // Properties
    int selectbundleindex = currentitem->data(0, Qt::UserRole).toInt();
    TBundleRecItem* item = nekodriver->GetBundleItemInfo(selectbundleindex);
    if (item == NULL) {
        return;
    }
    TSourceSlot sourceslot = GetActiveSlotForUser(*item);
    QString refname = item->Filename[sourceslot];
    if (refname.endsWith(".hv", Qt::CaseInsensitive)) {
        // RegEditor
        // we only open max 3 window
        if (fEditingHives.values().contains(selectbundleindex) == false) {
            QString tempfilename = QDir::tempPath() + QString("/MPack_%1.hv").arg(random(0, 65535));
            QFile qfile(tempfilename);
            qfile.open(QFile::ReadWrite);
            nekodriver->DumpRefBinaray(selectbundleindex, &qfile, false);
            qfile.close();

            TRegEdtFrm *regedtfrm = new TRegEdtFrm; // Top one?
            regedtfrm->ParseHiveFromFile(tempfilename);
            regedtfrm->setWindowTitle(regedtfrm->windowTitle() + " - " + refname + "[*]");
            fEditingHives.insert(tempfilename, selectbundleindex);
            QObject::connect(regedtfrm, SIGNAL(hiveUpdated(QString, bool, bool)),
                             this, SLOT(onRegeditorHiveModified(QString, bool, bool)));
            regedtfrm->show();
        }
    }
}

void TMainFrm::onRegeditorHiveModified( QString filename, bool modified, bool nukefile )
{
    if (fEditingHives.contains(filename)) {
        int hivindex = fEditingHives[filename];
        if (modified) {
            QFile qfile(filename);
            qfile.open(QFile::ReadOnly);
            QByteArray newhivecontent = qfile.readAll();
            qfile.close();
            if (nukefile) {
                qfile.remove();
            }
            if (nekodriver->ReplaceBundleRecContent(hivindex, newhivecontent)) {
                nekodriver->KeepProjectView(ui.projectView);
                setWindowModified(true);
            }
        }
        if (nukefile) {
            fEditingHives.remove(filename);
            if (!modified) {
                QFile::remove(filename);
            }
        }
    }
}

void TMainFrm::TestDumpKey( CERegistryKey &topkey )
{
    // parent first
    writeLog(QString("[%1]").arg(topkey.Name()), ltDebug);
    foreach (CERegistryValue value, topkey.GetValues() ) {
        writeLog(QString("%1=%2").arg(value.Name()).arg(value.Brief()), ltHint);
    }
    // sub second
    uint count = topkey.GetSubKeyCount();
    if (count > 0) {
        QStringList subkeynames = topkey.GetSubKeyNames();
        foreach ( QString subkeyname, subkeynames ) {
            //writeLog(subkeyname);
            CERegistryKey subkey = topkey.OpenSubKey(subkeyname);
            TestDumpKey(subkey);
        }
    }
}


void TMainFrm::onProjectViewRightclick( const QPoint& point )
{
    // retrieve selected files in projectView
    QList < QTreeWidgetItem* > selecteditems = ui.projectView->selectedItems();
    if (selecteditems.isEmpty()) {
        return;
    }
    bool deletedpresent = false, originalpresent = false;
    bool nonstock = false;
    foreach(QTreeWidgetItem* currentitem, selecteditems) {
        int selectbundleindex = currentitem->data(0, Qt::UserRole).toInt();
        TBundleRecItem* item = nekodriver->GetBundleItemInfo(selectbundleindex);
        if (item == NULL) {
            continue;
        }
        TSourceSlot sourceslot = GetActiveSlotForUser(*item);
        if (item->ItemNuked[sourceslot]) {
            deletedpresent = true;
        } else {
            originalpresent = true;
        }
        if (sourceslot != stStock) {
            nonstock = true;
        }
    }
    QMenu* menu = new QMenu;
    if (selecteditems.size() == 1) {
        menu->addAction(ui.actionProperties);
    }
    menu->addAction(ui.actionExtractSingle);
    menu->addAction(ui.actionAdd);
    menu->addAction(ui.actionReplace);
    menu->addAction(ui.actionRename);
    if (deletedpresent) {
        menu->addAction(ui.actionRestore);
    }
    if (originalpresent) {
        menu->addAction(ui.actionDelete);
    }
    if (nonstock) {
        menu->addAction(ui.actionRollback);
    }
    QPoint globalPos = ui.projectView->mapToGlobal(point);
    menu->exec(globalPos);
}


void TMainFrm::TryAcceptSource( QFileInfo &info, QSet< int > &processedindex, int maxdepth, int &bypassoucnt, int &replacedcount, int &acceptcount )
{
    maxdepth--;
    if ((info.exists() == false)) {
        return;
    }
    if (info.isDir()) {
        // TODO: read files from it
        QDir dir(info.filePath());
        dir.setFilter(QDir::Files | QDir::Dirs | QDir::NoSymLinks | QDir::NoDotAndDotDot);
        dir.setSorting(QDir::DirsLast);

        QFileInfoList list = dir.entryInfoList();
        foreach(QFileInfo subinfo, list) {
            if (maxdepth > 0) {
                TryAcceptSource(subinfo, processedindex, maxdepth, bypassoucnt, replacedcount, acceptcount);
            }
        }
    }
    if (info.isFile()) {
        int bundleindex = nekodriver->GetBundleIndexByName(info.fileName());
        if (bundleindex == -1 || processedindex.contains(bundleindex)) {
            bypassoucnt++;
            return;
        }
        TBundleRecItem* item = nekodriver->GetBundleItemInfo(bundleindex);
        if (item == NULL) {
            return;
        }
        QFile file(info.filePath());
        file.open(QFile::ReadOnly);
        QByteArray filecontent = file.readAll();
        file.close();
        replacedcount++;
        if (nekodriver->ReplaceBundleRecContent(bundleindex, filecontent)) {
            processedindex.insert(bundleindex);
            acceptcount++;
        }
    }
}


void TMainFrm::onProjectDropfiles( const QMimeData* data )
{
    if (data == NULL || nekodriver->IsProjectEmpty()) {
        return;
    }
    int replacedcount = 0, acceptcount = 0, bypassoucnt = 1;
    int costtime = -1;
    // try replace dir / files
    if (data->hasUrls()) {
        if (data->urls().isEmpty() == false) {
            // find match
            QTime starttime = QTime::currentTime();
            QSet < int > processedindex;
            foreach (QUrl url, data->urls()) {
                //int bundleindex = nekodriver->GetBundleIndexByName(url.path())
                QString path = url.toLocalFile();
                QFileInfo info(path);
                TryAcceptSource(info, processedindex, 8, bypassoucnt, replacedcount, acceptcount);
            }
            costtime = starttime.msecsTo(QTime::currentTime());
        }
    }
    writeLog(QString(tr("%1 files matched item name. %2 replaced.")).arg(replacedcount).arg(acceptcount));
    if (costtime != -1) {
        writeLog(QString(tr("we take %1 ms for dropped files.")).arg(costtime), ltHint);
    }
    if (acceptcount > 0) {
        nekodriver->KeepProjectView(ui.projectView);
        setWindowModified(true);
    }
}

void TMainFrm::onProjectExtraInfoClicked()
{
    // ROM Extra Info
    QDialog *dialog = new QDialog;
    Ui::ROMInfoDialog infofrm;
    infofrm.setupUi(dialog);
    infofrm.romBaseAddressEdt->setText(QString("%1").arg(ULONG(nekodriver->GetROMInfo(riiBassAddress)), 8, 16, QChar('0')).toUpper());
    infofrm.romSizeEdt->setText(QString("%1").arg(ULONG(nekodriver->GetROMInfo(riiSize)), 8, 16, QChar('0')).toUpper());
    infofrm.romLoadAddressEdt->setText(QString("%1").arg(ULONG(nekodriver->GetROMInfo(riiLoadAddressOverided)), 8, 16, QChar('0')).toUpper());
    infofrm.romXipChainAddressEdt->setText(QString("%1").arg(ULONG(nekodriver->GetROMInfo(riiXipChainAddress)), 8, 16, QChar('0')).toUpper());

    TMeizuOEMTag tag = nekodriver->GetMeizuTag();
    infofrm.romMeizuTagEdt->setText(QString("%1.%2.%3.%4"/*.%5.%6"*/).arg(tag.MajorTag).arg(tag.MinorTag).arg(tag.WeekTag).arg(tag.PubTag)/*.arg(tag.BuildTag).arg(tag.RevTag)*/);

    QObject::connect(infofrm.romMeizuTagEdt, SIGNAL(textChanged(QString)),
                     this, SLOT(onMeizuTagModified(QString)));

    QObject::connect(dialog, SIGNAL(accepted()),
        this, SLOT(onExtraInfoAccepted()));
    QObject::connect(dialog, SIGNAL(rejected()),
        this, SLOT(onExtraInfoRejected()));


    dialog->show();
    // pushbutton clicked connected to widget close in designer
}


void TMainFrm::onMeizuTagModified( QString text )
{
    // try update
    fMeizuOEMTagExpr = text;
}

void TMainFrm::onExtraInfoAccepted()
{
    if (fMeizuOEMTagExpr.isEmpty() == false) {
        // Parse TagExpr
        // TODO: split?
        QStringList newtaglist = fMeizuOEMTagExpr.split('.', QString::SkipEmptyParts, Qt::CaseSensitive);
        if (newtaglist.size() * sizeof (DWORD) == sizeof(TMeizuOEMTag)) {
            TMeizuOEMTag newtag;
            newtag.MajorTag = newtaglist.at(0).toUInt();
            newtag.MinorTag = newtaglist.at(1).toUInt();
            newtag.WeekTag  = newtaglist.at(2).toUInt();
            newtag.PubTag   = newtaglist.at(3).toUInt();
            //newtag.BuildTag = newtaglist.at(4).toUInt();
            //newtag.RevTag   = newtaglist.at(5).toUInt();
#ifndef _DEBUG
            if (newtag.MajorTag == 1 && newtag.MinorTag == 0 && newtag.WeekTag == 0 && newtag.PubTag == 0) {
                newtag.MajorTag = 0;
            }
#endif
            if (nekodriver->SetMeizuTag(newtag)) {
                nekodriver->KeepProjectView(ui.projectView); // nk.exe mark
                setWindowModified(true);
            }
        }
    }
}

void TMainFrm::onExtraInfoRejected()
{
    fMeizuOEMTagExpr.clear();
}

void TMainFrm::onHelpAboutClicked()
{
    QWidget *widget = new QWidget;
    Ui::AboutFrmClass aboutfrm;
    aboutfrm.setupUi(widget);
    QPalette palette;
    palette.setColor(QPalette::Base, palette.color(QPalette::Window));
    aboutfrm.textBrowser->setPalette(palette);
    widget->show();
    // pushbutton clicked connected to widget close in designer
}

void TMainFrm::onProjectFindPatternChanged()
{
    QString newfilter = ui.projectFindEdt->text();
    if (nekodriver->ApplyProjectViewNameFilter(newfilter)) {
        nekodriver->KeepProjectView(ui.projectView);
    }
}

void TMainFrm::onTranslateFilterChanged()
{
    bool changed = false;
    changed = changed || nekodriver->ApplyTranslateViewFilter(ftOriginal, ui.originalChk->isChecked());
    changed = changed || nekodriver->ApplyTranslateViewFilter(ftModified, ui.changedChk->isChecked());
    changed = changed || nekodriver->ApplyTranslateViewFilter(ftSession, ui.sessionChk->isChecked());
    changed = changed || nekodriver->ApplyTranslateViewFilter(ftImage, ui.imageChk->isChecked());
    changed = changed || nekodriver->ApplyTranslateViewFilter(ftText, ui.textChk->isChecked());
    if (changed) {
        nekodriver->KeepProjectView(ui.projectView);
    }
}

void TMainFrm::onProjectSortDisabled( int )
{
    nekodriver->KeepProjectView(ui.projectView);
}

void TMainFrm::writeLog( QString content, TLogType logtype /*= ltMessage*/ )
{
    if (content.isEmpty()) {
        content = "NULL";
    }
    QStringList list;
    list = content.split("\n");
    for (QStringList::iterator it = list.begin(); it != list.end(); it++) {
        if ((*it).isEmpty() && it + 1 == list.end()) {
            break;
        }
        int prevrowcount = ui.logView->rowCount();
        ui.logView->setRowCount(prevrowcount + 1);
        QTableWidgetItem *item;
        item = new QTableWidgetItem(QDateTime::currentDateTime().toString("hh:mm:ss"));
        ui.logView->setItem(prevrowcount, 0, item);
        item = new QTableWidgetItem(LogTypeToString(logtype));
        ui.logView->setItem(prevrowcount, 1, item);
        item = new QTableWidgetItem(*it);
        ui.logView->setItem(prevrowcount, 2, item);

        ui.logView->scrollToItem(item);
    }
    if (ui.logView->rowCount() < 80) {
        //ui.logView->resizeRowsToContents();
        //ui.logView->resizeColumnsToContents();
    }
}

void TMainFrm::onLanguageEnglishClicked()
{
    QApplication::removeTranslator(fTranslator);
    QFont f = QApplication::font();
    if (f.pointSize() == 9) {
        f.setPointSize(8);
        QApplication::setFont(f);
    }
    ui.retranslateUi(this);
    fLastLangCode = "enu";
}

void TMainFrm::onLanguageChineseClicked()
{
    QApplication::removeTranslator(fTranslator);
    fTranslator->load(QString("MPack_chs"), QApplication::applicationDirPath() + "/Language");
    QApplication::installTranslator(fTranslator);
    QFont f = QApplication::font();
    if (f.pointSize() == 8) {
        f.setPointSize(9); // TODO: Simsun 9 in Tahoma 8 fix
        QApplication::setFont(f);
    }
    ui.retranslateUi(this);
    fLastLangCode = "chs";
}

void TMainFrm::onLanguageDutchClicked()
{
    QApplication::removeTranslator(fTranslator);
    fTranslator->load(QString("MPack_dut"), QApplication::applicationDirPath() + "/Language");
    QApplication::installTranslator(fTranslator);
    QFont f = QApplication::font();
    if (f.pointSize() == 9) {
        f.setPointSize(8);
        QApplication::setFont(f);
    }
    ui.retranslateUi(this);
    fLastLangCode = "dut";
}

void TMainFrm::StoreTranslator( QTranslator* translator )
{
    fTranslator = translator;
}

void TMainFrm::onHelpContentsClicked()
{
    QString smartchmname;
    if (fLastLangCode.isEmpty()) {
        smartchmname = QApplication::applicationDirPath() + "/MPack_" + localLanguage() + ".chm";
    } else {
        smartchmname = QApplication::applicationDirPath() + "/MPack_" + fLastLangCode + ".chm";
    }
    if (QFileInfo(smartchmname).exists() == false) {
        smartchmname = QApplication::applicationDirPath() + "/MPack_enu.chm";
    }
    QDesktopServices::openUrl(smartchmname);
}

void TMainFrm::onToolsLiteXipBuilderClicked()
{
    TLiteXipBuilderFrm *builderfrm = new TLiteXipBuilderFrm(); // Top one?
    builderfrm->show();
}

void TMainFrm::onToolsLiteFactroyClicked()
{

}

QString LogTypeToString( TLogType logtype )
{
    switch (logtype)
    {
    case ltHint:
        return "Hint";
        break;
    case ltDebug:
        return "Debug";
        break;
    case ltMessage:
        return "Message";
        break;
    case ltError:
        return "Error";
        break;
    }
    return "Unkown";
}

QString localLanguage() {
    QLocale locale = QLocale::system();

    // This switch will translate to the correct locale
    switch (locale.language()) {
            case QLocale::German:
                return "deu";
            case QLocale::French:
                return "fra";
            case QLocale::Chinese:
                return "chs";
            case QLocale::HongKong:
            case QLocale::Macau:
            case QLocale::Taiwan:
                return "cht";
            case QLocale::Spanish:
                return "spa";
            case QLocale::Japanese:
                return "ja";
            case QLocale::Korean:
                return "kor";
            case QLocale::Belgium:
            case QLocale::Netherlands:
            case QLocale::NetherlandsAntilles:
                return "dut";
            default:
                return "enu";
    }
}