#include "PlatformBuilderImageUtils.h"
#include <QFileInfo>

CERegistryHive CERegistryHive::OpenHive( QString fileName, bool writeable )
{
    // we do not use pool like Microsoft.PlatformBuilder.ImgExpCommon.dll
    // write rulz take from regcomp.exe
    GUID empty = {0, };
    Fshasht romSignature = {0, };

    QFileInfo info(fileName);
    if (info.isRelative()) {
        fileName = info.absoluteFilePath();
    }
    fileName = fileName.replace('/', '\\');

    VolumeFileFlag fileFlags = writeable ? VOL_OPEN_ALWAYS : VOL_OPEN_EXISTING; 
    VolumeOpenFlag volFlags = writeable ? FSVOL_DEFAULT_FLAGS : FSVOL_TYPE_READONLY;
    uint initialSize = writeable ? 0xC000 : 0;
    uint hiveID = CEREG_RegMountHive(empty, fileName.utf16(), NULL, fileFlags, volFlags, romSignature, initialSize);
    if (hiveID == 0) {
        //int error = Marshal.GetLastWin32Error();
        //string message = string.Format(CultureInfo.CurrentCulture, Strings.FailedOpenHiveFormat, new object[] { fileName });
        //throw new Win32Exception(error, message);
    }
    return CERegistryHive(hiveID, empty, fileName);
}

CERegistryHive::CERegistryHive( uint hiveID, GUID guid, QString filename ) : fHiveID(hiveID), fGuid(guid), fFilename(filename)
{

}

uint CERegistryHive::HiveID()
{
    return fHiveID;
}

CERegistryKey CERegistryHive::OpenRootKey( CERegistryKey rootKey )
{
    if (((rootKey.Handle() != CERegistry::ClassesRoot().Handle()) && (rootKey.Handle() != CERegistry::LocalMachine().Handle())) && ((rootKey.Handle() != CERegistry::Users().Handle()) && (rootKey.Handle() != CERegistry::CurrentUser().Handle()))) {
        //throw new ArgumentException(Strings.MustBeRootKey, "rootKey");
    }
    CERegistry::SetRootKeyActiveHive(rootKey, *this);
    return rootKey;
}

void CERegistryHive::Close()
{
    CEREG_RegUnmountHive(fHiveID);
}

int CERegistryKey::GetSubKeyCount()
{
    uint subKeys = 0;
    uint maxSubKeyLen = 0;
    FILETIME lastWriteTime;
    CEREG_RegQueryInfoKey(fKey, NULL, NULL, NULL, &subKeys, &maxSubKeyLen, &maxSubKeyLen, &maxSubKeyLen, &maxSubKeyLen, &maxSubKeyLen, &maxSubKeyLen, &lastWriteTime);
    return int(subKeys);
}

int CERegistryKey::GetValueCount()
{
    uint values = 0;
    uint subKeys = 0;
    FILETIME lastWriteTime;
    CEREG_RegQueryInfoKey(fKey, NULL, NULL, NULL, &subKeys, &subKeys, &subKeys, &values, &subKeys, &subKeys, &subKeys, &lastWriteTime);
    return int(values);
}

HKEY CERegistryKey::Handle()
{
    return fKey;
}

CERegistryValue CERegistryKey::GetValue( QString valueName )
{
    RegistryValueDataType type = CE_REG_NONE;
    QByteArray data(4096, 0);
    uint reserved = 0;
    uint length = data.size();
    if (CEREG_RegQueryValueEx(fKey, valueName.utf16(), &reserved, &type, (uchar*)data.data(), &length) != 0)
    {
        return CERegistryValue(valueName, type, QByteArray());
    }
    data.resize(length);
    return CERegistryValue(valueName, type, data);
}

QStringList CERegistryKey::GetSubKeyNames()
{
    uint subKeys = 0;
    uint maxSubKeyLen = 0;
    uint maxClassLen = 0;
    FILETIME lastWriteTime;
    CEREG_RegQueryInfoKey(fKey, NULL, NULL, NULL, &subKeys, &maxSubKeyLen, &maxClassLen, &maxClassLen, &maxClassLen, &maxClassLen, &maxClassLen, &lastWriteTime);
    QStringList strArray;// = QStringList(subKeys);
    QString name((int) maxSubKeyLen, 0);
    for (uint i = 0; i < subKeys; i++)
    {
        uint nameSize = (uint) (name.size() + 1);
        name.fill(0);
        CEREG_RegEnumKeyEx(fKey, i, name.utf16(), nameSize, NULL, NULL, NULL, NULL);
        strArray.push_back(name.left(nameSize));
    }
    return strArray;
}

QList<CERegistryValue> CERegistryKey::GetValues()
{
    uint values = 0;
    uint maxValueNameLen = 0;
    uint maxValueLen = 0;
    uint subKeys = 0;
    FILETIME lastWriteTime;
    CEREG_RegQueryInfoKey(fKey, NULL, NULL, NULL, &subKeys, &subKeys, &subKeys, &values, &maxValueNameLen, &maxValueLen, &subKeys, &lastWriteTime);
    QByteArray data(maxValueLen, 0);
    QList<CERegistryValue> valueArray;// = new CERegistryValue[values];
    QString valueName ((int) maxValueNameLen, 0);
    for (uint i = 0; i < values; i++)
    {
        uint valueNameSize = maxValueNameLen + 1;
        uint length = (uint) data.size();
        RegistryValueDataType type = CE_REG_NONE;
        valueName.fill(0);
        CEREG_RegEnumValue(fKey, i, valueName.utf16(), valueNameSize, subKeys, &type, (uchar*)data.data(), length);
        valueArray.push_back(CERegistryValue(valueName.left(valueNameSize), type, data.left(length)));
    }
    return valueArray;

}

CERegistryKey CERegistryKey::OpenSubKey( QString subkeyName )
{
    if (subkeyName.isEmpty())
    {
        return *this;
    }
    HKEY zero = NULL;
    if (CEREG_RegOpenKeyEx(fKey, subkeyName.utf16(), 0, 0, zero) != ERROR_SUCCESS)
    {
        return *this;
    }
    // TODO: fParentKey n SelfKeyname
    return CERegistryKey(zero, subkeyName, this->Name());
}

QString CERegistryKey::Name()
{
    if (fParentPath.isEmpty()) {
        return fSubName;
    }
    return fParentPath + "\\" + fSubName;
}

QString CERegistryKey::SubName()
{
    return fSubName;
}

void CERegistryKey::Close()
{
    if (fDisposed) {
        return;
    }
    CEREG_RegCloseKey(fKey);
    fDisposed = true;
}

bool CERegistryKey::SetValue( CERegistryValue& newValue )
{
    bool Result = false;
    if (newValue.Name().isEmpty()) {
        return Result;
    }
    if (CEREG_RegSetValueEx(fKey, newValue.Name().utf16(), 0, newValue.Type(), (uchar*)newValue.Data().data(), newValue.Data().size()) == ERROR_SUCCESS) {
        Result = true;
    }
    return Result;
}

bool CERegistryKey::DeleteValue( QString valueName )
{
    bool Result = false;
    if (valueName.isEmpty()) {
        return Result;
    }
    if (CEREG_RegDeleteValue(fKey, valueName.utf16()) == ERROR_SUCCESS) {
        Result = true;
    }
    return Result;
}

CERegistryKey::CERegistryKey( HKEY hkey, QString name, QString parentname ): fKey(hkey), fSubName(name), fParentPath(parentname), fDisposed(false), fRef(0)
{
    if (fKey == 0) {
        fDisposed = true;
    }
}

CERegistryKey::CERegistryKey(): fKey(NULL), fSubName(""), fParentPath(""), fDisposed(true), fRef(0)
{

}

CERegistryKey::~CERegistryKey()
{
    //Close();
}

CERegistryKey CERegistryKey::CreateKey( QString subkeyName )
{
    CERegistryKey Result;
    HKEY zero = NULL;
    uint dispostion;
    if (CEREG_RegCreateKeyEx(fKey, subkeyName.utf16(), 0, NULL, 0, 0, 0, zero, dispostion) == ERROR_SUCCESS) {
        Result = CERegistryKey(zero, subkeyName, this->Name());
    }
    return Result;
}

bool CERegistryKey::DeleteKey( QString subkeyName )
{
    bool Result = false;
    if (subkeyName.isEmpty()) {
        return Result;
    }
    Result = (CEREG_RegDeleteKey(fKey, subkeyName.utf16()) == ERROR_SUCCESS);
    return Result;
}
/*
CERegistryKey & CERegistryKey::operator=( const CERegistryKey &other )
{
    fRef++;
    other.fDisposed = false;
    other.fKey = fKey;
    other.fName = fName;
    other.fRef = 0;
}
*/

CERegistryValue::CERegistryValue( QString name, RegistryValueDataType type, QByteArray data ): fName(name), fType(type), fData(data)
{

}

QString CERegistryValue::Name()
{
    return fName;
}

QString CERegistryValue::Brief()
{
    QString Result = Data2Brief(fData, fType); // Debugger friendly
    return Result;
}

RegistryValueDataType CERegistryValue::Type()
{
    return fType;
}

QByteArray& CERegistryValue::Data()
{
    return fData;
}

// Dangerous!
CERegistryKey CERegistry::fClassesRoot    = CERegistryKey((HKEY)0x80000000UL, "HKEY_CLASSES_ROOT", "");
CERegistryKey CERegistry::fCurrentUser    = CERegistryKey((HKEY)0x80000001UL, "HKEY_CURRENT_USER", "");
CERegistryKey CERegistry::fLocalMachine   = CERegistryKey((HKEY)0x80000002UL, "HKEY_LOCAL_MACHINE", "");
CERegistryKey CERegistry::fUsers          = CERegistryKey((HKEY)0x80000003UL, "HKEY_USERS", "");

CERegistry::CERegistry()
{
}

CERegistryKey CERegistry::ClassesRoot()
{
    return fClassesRoot;
}

CERegistryKey CERegistry::CurrentUser()
{
    return fCurrentUser;
}

CERegistryKey CERegistry::LocalMachine()
{
    return fLocalMachine;
}

CERegistryKey CERegistry::Users()
{
    return fUsers;
}

void CERegistry::SetRootKeyActiveHive( CERegistryKey& key, CERegistryHive& hive )
{
    //Q_ASSERT(key)
    //Q_ASSERT(hive)
    CEREG_RegSetHKEYActiveHive(key.Handle(), hive.HiveID(), hive.HiveID());
}
QString ValueType2Str( RegistryValueDataType type ) {
    QString Result = "REG_ERROR";
    switch (type) {
        case CE_REG_SZ:
            Result = "REG_SZ";
            break;
        case CE_REG_EXPAND_SZ:
            Result = "REG_EXPAND_SZ";
            break;
        case CE_REG_LINK:
            Result = "REG_LINK";
            break;
        case CE_REG_MUI_SZ:
            Result = "REG_MUI_SZ";
            break;
        case CE_REG_BINARY:
            Result = "REG_BINARY";
            break;
        case CE_REG_DWORD:
            Result = "REG_DWORD";
            break;
        case CE_REG_MULTI_SZ:
            Result = "REG_MULTI_SZ";
            break;
    }
    return Result;

}

QString Data2Brief( QByteArray& data, RegistryValueDataType type ) {
    QString Result = "(none)";
    switch (type) {
        case CE_REG_SZ:
        case CE_REG_EXPAND_SZ:
        case CE_REG_LINK:
        case CE_REG_MUI_SZ:
            Result = QString::fromUtf16((ushort*)data.data(), data.size() / 2 - 1); // End
            break;
        case CE_REG_BINARY:
            Result = data.toHex().toUpper();
            break;
        case CE_REG_DWORD:
            Q_ASSERT(data.size() == 4);
            Result = QString("%1").arg(quint32(*PDWORD(data.data())), 8, 16, QChar('0')).toUpper();
            break;
        case CE_REG_MULTI_SZ:
            Result = QString::fromUtf16((ushort*)data.data(), data.size() / 2 - 1).replace(QChar(0), "\\n"); // EndNULL
            break;
    }
    return Result;
}