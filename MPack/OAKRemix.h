#ifndef _OAK_REMIX_H
#define _OAK_REMIX_H

// from c:\local\wince420\PUBLIC/COMMON/OAK/INC/PEHDR.H
struct info {                       /* Extra information header block      */
    unsigned long   rva;            /* Virtual relative address of info    */
    unsigned long   size;           /* Size of information block           */
};

// from U:\WINCE600\PUBLIC\COMMON\OAK\INC\ROMLDR.H
#define ROM_SIGNATURE_OFFSET   0x40         // Offset from the image's physfirst address to the ROM signature.
#define ROM_SIGNATURE          0x43454345
#define ROM_TOC_POINTER_OFFSET 0x44         // Offset from the image's physfirst address to the TOC pointer.
#define ROM_TOC_OFFSET_OFFSET  0x48         // Offset from the image's physfirst address to the TOC offset (from physfirst).

#define ROM_EXTRA 9

typedef struct e32_rom {
    unsigned short  e32_objcnt;         /* Number of memory objects            */
    unsigned short  e32_imageflags;     /* Image flags                         */
    unsigned long   e32_entryrva;       /* Relative virt. addr. of entry point */
    unsigned long   e32_vbase;          /* Virtual base address of module      */
    unsigned short  e32_subsysmajor;    /* The subsystem major version number  */
    unsigned short  e32_subsysminor;    /* The subsystem minor version number  */

    unsigned long   e32_stackmax;       /* Maximum stack size                  */
    unsigned long   e32_vsize;          /* Virtual size of the entire image    */
    unsigned long   e32_sect14rva;      /* section 14 rva */
    unsigned long   e32_sect14size;     /* section 14 size */
    unsigned long   e32_timestamp;      /* Time EXE/DLL was created/modified   */

    struct info     e32_unit[ROM_EXTRA]; /* Array of extra info units      */
    unsigned short  e32_subsys;         /* The subsystem type                  */
} e32_rom;

// o32_flags

#ifndef __GNUC__
#define IMAGE_SCN_TYPE_REGULAR               0x00000000  //
#define IMAGE_SCN_TYPE_DUMMY                 0x00000001  // Reserved.
#define IMAGE_SCN_TYPE_NO_LOAD               0x00000002  // Reserved.
#define IMAGE_SCN_TYPE_GROUPED               0x00000004  // Used for 16-bit offset code.
#define IMAGE_SCN_TYPE_NO_PAD                0x00000008  // Reserved.
#define IMAGE_SCN_TYPE_COPY                  0x00000010  // Reserved.

#define IMAGE_SCN_CNT_CODE                   0x00000020  // Section contains code.
#define IMAGE_SCN_CNT_INITIALIZED_DATA       0x00000040  // Section contains initialized data.
#define IMAGE_SCN_CNT_UNINITIALIZED_DATA     0x00000080  // Section contains uninitialized data.

#define IMAGE_SCN_LNK_OTHER                  0x00000100  // Reserved.
#define IMAGE_SCN_LNK_INFO                   0x00000200  // Section contains comments or some other type of information.
#define IMAGE_SCN_LNK_OVERLAY                0x00000400  // Section contains an overlay.
#define IMAGE_SCN_LNK_REMOVE                 0x00000800  // Section contents will not become part of image.
#define IMAGE_SCN_LNK_COMDAT                 0x00001000  // Section contents comdat.
#endif

#define IMAGE_SCN_COMPRESSED                 0x00002000  // Section is compressed

#ifndef __GNUC__
#define IMAGE_SCN_ALIGN_1BYTES               0x00100000  //
#define IMAGE_SCN_ALIGN_2BYTES               0x00200000  //
#define IMAGE_SCN_ALIGN_4BYTES               0x00300000  //
#define IMAGE_SCN_ALIGN_8BYTES               0x00400000  //
#define IMAGE_SCN_ALIGN_16BYTES              0x00500000  // Default alignment if no others are specified.
#define IMAGE_SCN_ALIGN_32BYTES              0x00600000  //
#define IMAGE_SCN_ALIGN_64BYTES              0x00700000  //

#define IMAGE_SCN_MEM_DISCARDABLE            0x02000000  // Section can be discarded.
#define IMAGE_SCN_MEM_NOT_CACHED             0x04000000  // Section is not cachable.
#define IMAGE_SCN_MEM_NOT_PAGED              0x08000000  // Section is not pageable.
#define IMAGE_SCN_MEM_SHARED                 0x10000000  // Section is shareable.
#define IMAGE_SCN_MEM_EXECUTE                0x20000000  // Section is executable.
#define IMAGE_SCN_MEM_READ                   0x40000000  // Section is readable.
#define IMAGE_SCN_MEM_WRITE                  0x80000000  // Section is writeable.
#endif


typedef struct o32_rom {
    unsigned long       o32_vsize;      /* Virtual memory size              */
    unsigned long       o32_rva;        /* Object relative virtual address  */
    unsigned long       o32_psize;      /* Physical file size of init. data */
    unsigned long       o32_dataptr;    /* Image pages offset               */
    unsigned long       o32_realaddr;   /* pointer to actual                */
    unsigned long       o32_flags;      /* Attribute flags for the object   */
} o32_rom;

typedef struct ROMHDR {
    ULONG   dllfirst;               // first DLL address
    ULONG   dlllast;                // last DLL address
    ULONG   physfirst;              // first physical address
    ULONG   physlast;               // highest physical address
    ULONG   nummods;                // number of TOCentry's
    ULONG   ulRAMStart;             // start of RAM
    ULONG   ulRAMFree;              // start of RAM free space
    ULONG   ulRAMEnd;               // end of RAM
    ULONG   ulCopyEntries;          // number of copy section entries
    ULONG   ulCopyOffset;           // offset to copy section
    ULONG   ulProfileLen;           // length of PROFentries RAM
    ULONG   ulProfileOffset;        // offset to PROFentries
    ULONG   numfiles;               // number of FILES
    ULONG   ulKernelFlags;          // optional kernel flags from ROMFLAGS .bib config option
    ULONG   ulFSRamPercent;         // Percentage of RAM used for filesystem
                                        // from FSRAMPERCENT .bib config option
                                        // byte 0 = #4K chunks/Mbyte of RAM for filesystem 0-2Mbytes 0-255
                                        // byte 1 = #4K chunks/Mbyte of RAM for filesystem 2-4Mbytes 0-255
                                        // byte 2 = #4K chunks/Mbyte of RAM for filesystem 4-6Mbytes 0-255
                                        // byte 3 = #4K chunks/Mbyte of RAM for filesystem > 6Mbytes 0-255

    ULONG   ulDrivglobStart;        // device driver global starting address
    ULONG   ulDrivglobLen;          // device driver global length
    USHORT  usCPUType;              // CPU (machine) Type
    USHORT  usMiscFlags;            // Miscellaneous flags
    PVOID   pExtensions;            // pointer to ROM Header extensions
    ULONG   ulTrackingStart;        // tracking memory starting address
    ULONG   ulTrackingLen;          // tracking memory ending address
} ROMHDR, *PROMHDR;

typedef struct ROMChain_t {
    struct ROMChain_t *pNext;
    ROMHDR *pTOC;
} ROMChain_t;

//
// ROM Header extension: PID
//
#define PID_LENGTH 10
typedef struct ROMPID {
    union{
        DWORD dwPID[PID_LENGTH];        // PID
        struct{
            char  name[(PID_LENGTH - 4) * sizeof(DWORD)];
            DWORD type;
            PVOID pdata;
            DWORD length;
            DWORD reserved;
        };
    };
    PVOID pNextExt;                 // pointer to next extension if any
} ROMPID, EXTENSION;


//
//  Module Table of Contents - follows ROMHDR in image
//
// followed by nummods <TOCentry>'s
typedef struct TOCentry {           // MODULE BIB section structure
    DWORD dwFileAttributes;
    FILETIME ftTime;
    DWORD nFileSize;
    LPSTR   lpszFileName;
    ULONG   ulE32Offset;            // Offset to E32 structure
    ULONG   ulO32Offset;            // Offset to O32 structure
    ULONG   ulLoadOffset;           // MODULE load buffer offset
} TOCentry, *LPTOCentry;

//
//  Files Section Structure
//
// followed by numfiles <TOCentry>'s
typedef struct FILESentry {         // FILES BIB section structure
    DWORD dwFileAttributes;
    FILETIME ftTime;
    DWORD nRealFileSize;
    DWORD nCompFileSize;
    LPSTR   lpszFileName;
    ULONG   ulLoadOffset;           // FILES load buffer offset
} FILESentry, *LPFILESentry;

//
//  Copy Section Structure
//
typedef struct COPYentry {
    ULONG   ulSource;               // copy source address
    ULONG   ulDest;                 // copy destination address
    ULONG   ulCopyLen;              // copy length
    ULONG   ulDestLen;              // copy destination length
    // (zero fill to end if > ulCopyLen)
} COPYentry;

//
//  Profile Code Section Structure
//
typedef struct PROFentry {          // code section profile entry
    ULONG   ulModNum;               // module number in table of contents
    ULONG   ulSectNum;              // section number in o32
    ULONG   ulStartAddr;            // starting address of section
    ULONG   ulEndAddr;              // ending address of section
    ULONG   ulHits;                 // number of hits in section
    ULONG   ulNumSym;               // number of symbols in section
    ULONG   ulHitAddress;           // address to hit table 
    ULONG   ulSymAddress;           // address to symbol table 
} PROFentry;

//
//  Profile Symbol Section
//
//  Profile symbol section contains function address and hit counter for
//  each function followed by ASCIIZ symbols.
//
typedef struct SYMentry {
    ULONG   ulFuncAddress;          // function starting address
    ULONG   ulFuncHits;             // function hit counter
} SYMentry;


//
// Multiple-XIP data structures
//
#define MAX_ROM                 32      // max numbler of XIPs
#define XIP_NAMELEN             32      // max name length of XIP
#define ROM_CHAIN_OFFSET        0       // offset for XIPCHAIN_INFO

//
// flags for XIP entries
//
#define ROMXIP_OK_TO_LOAD       0x0001
#define ROMXIP_IS_SIGNED        0x0002

//
// XIP chain entry
//
#pragma pack(push, 1)
typedef struct _XIPCHAIN_ENTRY {
    LPVOID  pvAddr;                 // address of the XIP
    DWORD   dwLength;               // the size of the XIP
    DWORD   dwMaxLength;            // the biggest it can grow to
    USHORT  usOrder;                // where to put into ROMChain_t
    USHORT  usFlags;                // flags/status of XIP
    DWORD   dwVersion;              // version info
    CHAR    szName[XIP_NAMELEN];    // Name of XIP, typically the bin file's name, w/o .bin
    DWORD   dwAlgoFlags;            // algorithm to use for signature verification
    DWORD   dwKeyLen;               // length of key in byPublicKey
    BYTE    byPublicKey[596];       // public key data
} XIPCHAIN_ENTRY, *PXIPCHAIN_ENTRY;

typedef struct _XIPCHAIN_SUMMARY {
    LPVOID  pvAddr;                 // address of the XIP
    DWORD   dwMaxLength;            // the biggest it can grow to
    USHORT  usOrder;                // where to put into ROMChain_t
    USHORT  usFlags;                // flags/status of XIP
    DWORD   reserved;               // for future use
} XIPCHAIN_SUMMARY, *PXIPCHAIN_SUMMARY;

typedef struct _XIPCHAIN_INFO {
    DWORD cXIPs;
    //
    // may contain more than one entry, but we only need the address of first one
    //
    XIPCHAIN_ENTRY xipEntryStart;
} XIPCHAIN_INFO, *PXIPCHAIN_INFO;
#pragma pack(pop)

//
// Compressed RAM-image data structures
//
#define RAMIMAGE_COMPRESSION_VERSION       5
#define RAMIMAGE_COMPRESSION_BLOCK_SIZE    4096
#define RAMIMAGE_COMPRESSION_SIGNATURE     0x58505253

typedef struct __COMPRESSED_RAMIMAGE_HEADER {
    BYTE bReserved[48];
    DWORD dwVersion;
    DWORD dwHeaderSize;
    DWORD dwCompressedBlockCount;
    DWORD dwCompressedBlockSize;
    DWORD dwSignature;              // keep at ROM_OFFSET (64 bytes)
    WORD  wBlockSizeTable[2];       // block size table
} COMPRESSED_RAMIMAGE_HEADER, *PCOMPRESSED_RAMIMAGE_HEADER;


//----------------output structures [ how pe-exe files are structured ]
//
//  file starts with IMAGE_DOS_HEADER
// 0000000: 5a4d  0090  0003  0000  0004  0000  ffff  0000   MZ..............
// 0000010: 00b8  0000  0000  0000  0040  0000  0000  0000   ........@.......
// 0000020: 0000  0000  0000  0000  0000  0000  0000  0000   ................
// 0000030: 0000  0000  0000  0000  0000  0000  000000c0     ................
//
// followed by some dummy code
// 0000040: 0e 1f ba 0e 00 b4 09 cd 21 b8 01 4c cd 21 54 68  ........!..L.!Th
// 0000050: 69 73 20 70 72 6f 67 72 61 6d 20 63 61 6e 6e 6f  is program canno
// 0000060: 74 20 62 65 20 72 75 6e 20 69 6e 20 44 4f 53 20  t be run in DOS
// 0000070: 6d 6f 64 65 2e 0d 0d 0a 24 00 00 00 00 00 00 00  mode....$.......
//
// followed by something unknown
// 0000080: bf 1a f4 da fb 7b 9a 89 fb 7b 9a 89 fb 7b 9a 89  .....{...{...{..
// 0000090: fb 7b 9b 89 fa 7b 9a 89 66 5b ba 89 f8 7b 9a 89  .{...{..f[...{..
// 00000a0: 82 5a be 89 fa 7b 9a 89 52 69 63 68 fb 7b 9a 89  .Z...{..Rich.{..
// 00000b0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................
//
// followed by a 'e32_exe' struct
// followed by <e32_objcnt> 'o32_obj' structs
// followed by NUL's to align to next 512-byte boundary
// followed by data-sections, each NUL-padded to the next 512-byte boundary
//
// followed by the debug-directory


// also defined in c:\local\WINCE420\PUBLIC\COMMON\SDK\INC\winnt.h
#ifndef IMAGE_DOS_SIGNATURE
#define IMAGE_DOS_SIGNATURE                 0x5A4D      // MZ

typedef struct _IMAGE_DOS_HEADER {      // DOS .EXE header
    WORD   e_magic;                     // Magic number
    WORD   e_cblp;                      // Bytes on last page of file
    WORD   e_cp;                        // Pages in file
    WORD   e_crlc;                      // Relocations
    WORD   e_cparhdr;                   // Size of header in paragraphs
    WORD   e_minalloc;                  // Minimum extra paragraphs needed
    WORD   e_maxalloc;                  // Maximum extra paragraphs needed
    WORD   e_ss;                        // Initial (relative) SS value
    WORD   e_sp;                        // Initial SP value
    WORD   e_csum;                      // Checksum
    WORD   e_ip;                        // Initial IP value
    WORD   e_cs;                        // Initial (relative) CS value
    WORD   e_lfarlc;                    // File address of relocation table
    WORD   e_ovno;                      // Overlay number
    WORD   e_res[4];                    // Reserved words
    WORD   e_oemid;                     // OEM identifier (for e_oeminfo)
    WORD   e_oeminfo;                   // OEM information; e_oemid specific
    WORD   e_res2[10];                  // Reserved words
    LONG   e_lfanew;                    // File address of new exe header
} IMAGE_DOS_HEADER, *PIMAGE_DOS_HEADER;

#define IMAGE_FILE_RELOCS_STRIPPED           0x0001  // Relocation info stripped from file.

#define IMAGE_SCN_CNT_CODE                   0x00000020  // Section contains code.
#define IMAGE_SCN_CNT_INITIALIZED_DATA       0x00000040  // Section contains initialized data.
#define IMAGE_SCN_CNT_UNINITIALIZED_DATA     0x00000080  // Section contains uninitialized data.
#endif
#define STD_EXTRA       16

// see c:\local\WINCE420\PUBLIC\COMMON\OAK\INC\pehdr.h
// indexes into 'e32_unit'
#define EXP             0           /* Export table position               */
#define IMP             1           /* Import table position               */
#define RES             2           /* Resource table position             */
#define EXC             3           /* Exception table position            */
#define SEC             4           /* Security table position             */
#define FIX             5           /* Fixup table position                */
#define DEB             6           /* Debug table position                */
#define IMD             7           /* Image description table position    */
#define MSP             8           /* Machine specific table position     */
#define TLS             9           /* Thread Local Storage                */
#define CBK            10           /* Callbacks                           */
#define RS1            11           /* Reserved                            */
#define RS2            12           /* Reserved                            */
#define RS3            13           /* Reserved                            */
#define RS4            14           /* Reserved                            */
#define RS5            15           /* Reserved                            */

// value for e32_cpu
#define IMAGE_FILE_MACHINE_ARM               0x01c0  // ARM Little-Endian

// this is the 'FILE HEADER VALUES' in dumpbin output
typedef struct e32_exe {            /* PE 32-bit .EXE header               */
    unsigned char   e32_magic[4];   /* Magic number E32_MAGIC              */
    unsigned short  e32_cpu;        /* The CPU type                        */
    unsigned short  e32_objcnt;     /* Number of memory objects            */
    unsigned long   e32_timestamp;  /* Time EXE file was created/modified  */
    unsigned long   e32_symtaboff;  /* Offset to the symbol table          */

    unsigned long   e32_symcount;   /* Number of symbols                   */
    unsigned short  e32_opthdrsize; /* Optional header size                */
    unsigned short  e32_imageflags; /* Image flags                         */
    unsigned short  e32_coffmagic;  /* Coff magic number (usually 0x10b)   */
    unsigned char   e32_linkmajor;  /* The linker major version number     */
    unsigned char   e32_linkminor;  /* The linker minor version number     */
    unsigned long   e32_codesize;   /* Sum of sizes of all code sections   */

    unsigned long   e32_initdsize;  /* Sum of all initialized data size    */
    unsigned long   e32_uninitdsize;/* Sum of all uninitialized data size  */
    unsigned long   e32_entryrva;   /* Relative virt. addr. of entry point */
    unsigned long   e32_codebase;   /* Address of beginning of code section*/

    unsigned long   e32_database;   /* Address of beginning of data section*/
    unsigned long   e32_vbase;      /* Virtual base address of module      */
    unsigned long   e32_objalign;   /* Object Virtual Address align. factor*/
    unsigned long   e32_filealign;  /* Image page alignment/truncate factor*/

    unsigned short  e32_osmajor;    /* The operating system major ver. no. */
    unsigned short  e32_osminor;    /* The operating system minor ver. no. */
    unsigned short  e32_usermajor;  /* The user major version number       */
    unsigned short  e32_userminor;  /* The user minor version number       */
    unsigned short  e32_subsysmajor;/* The subsystem major version number  */
    unsigned short  e32_subsysminor;/* The subsystem minor version number  */
    unsigned long   e32_res1;       /* Reserved bytes - must be 0  */

    unsigned long   e32_vsize;      /* Virtual size of the entire image    */
    unsigned long   e32_hdrsize;    /* Header information size             */
    unsigned long   e32_filechksum; /* Checksum for entire file            */
    unsigned short  e32_subsys;     /* The subsystem type                  */
    unsigned short  e32_dllflags;   /* DLL flags                           */

    unsigned long   e32_stackmax;   /* Maximum stack size                  */
    unsigned long   e32_stackinit;  /* Initial committed stack size        */
    unsigned long   e32_heapmax;    /* Maximum heap size                   */
    unsigned long   e32_heapinit;   /* Initial committed heap size         */

    unsigned long   e32_res2;       /* Reserved bytes - must be 0  */
    unsigned long   e32_hdrextra;   /* Number of extra info units in header*/
    struct info     e32_unit[STD_EXTRA]; /* Array of extra info units      */
} e32_exe, *LPe32_exe;


// this is the 'section header' in dumpbin output
#define E32OBJNAMEBYTES 8               /* Name bytes                       */

typedef struct o32_obj {                /* .EXE memory object table entry   */
    unsigned char       o32_name[E32OBJNAMEBYTES];/* Object name            */
    unsigned long       o32_vsize;      /* Virtual memory size              */
    unsigned long       o32_rva;        /* Object relative virtual address  */
    unsigned long       o32_psize;      /* Physical file size of init. data */
    unsigned long       o32_dataptr;    /* Image pages offset               */
    unsigned long       o32_realaddr;   /* pointer to actual                */
    unsigned long       o32_access;     /* assigned access                  */
    unsigned long       o32_temp3;
    unsigned long       o32_flags;      /* Attribute flags for the object   */
} o32_obj, *LPo32_obj;

#endif
