#ifndef _NEKO_DRIVER_UNIT_H
#define _NEKO_DRIVER_UNIT_H

#include "XipStore.h"
#include "MainUnt.h"
#include <QFile>
#include <QTreeWidget>
#include <QSet>

class TNekoDriver {
public:
    explicit TNekoDriver(TMainFrm *frm);
protected:
    ROMHelper fXipHelper;
private:
    QString fProjectViewNameFilter;
    QSet < TFilterType > fTranslateViewFilter;
    QHash < QByteArray, int > fNameIndexLookupTable;
    QIcon fStdIcon, fExeIcon, fDllIcon, fLinkIcon, fCertIcon, fImageIcon, fMP3Icon, fDelIcon, fCloakingIcon;
public:
    bool IsProjectEmpty();
    bool IsProjectModified();
    bool BuildProjectFromXip(QString xipfilename);
    bool LoadProjectFromFile(QString mpkfilename);
    bool SaveProjectToFile(QString mpkfilename);
    bool KeepProjectView(QTreeWidget *treeview);
    bool ApplyProjectViewNameFilter(QString newfilter);
    bool ApplyTranslateViewFilter(TFilterType filter, bool enabled);
    bool DumpRefBinaray(int index, QIODevice *file, bool onlystock);
    bool DumpProjectContent(QString outputpath, QLabel *label = 0, QProgressBar* bar = 0);
    TBundleRecItem* GetBundleItemInfo(int index);
    int GetBundleIndexByName(QString itemname);
    DWORD GetROMInfo(TROMInfoIndex index);
    TMeizuOEMTag GetMeizuTag();
    bool SetMeizuTag(TMeizuOEMTag newtag);
    bool BuildXipFromBuffer(QString xipfilename, int compression, QProgressBar* bar = 0);
    bool PrepareBufferStock(QLabel *label = 0, QProgressBar* bar = 0);
    bool ReplaceBundleRecContent(int index, QByteArray& newcontent);
    bool MarkBundleRecAsDeleted(int index);
    bool ClearBundleRecDeletedMark(int index);
    bool RollbackToStockSlot(int index);
    bool RenameBundleRec(int index, QByteArray newname);
    bool AddUserFileToProject(int areaindex, QByteArray filename, QByteArray& newcontent, FILETIME modifytime);
    bool RemoveUserFile(int index);
};

typedef TNekoDriver* PNekoDriver;
extern PNekoDriver nekodriver;

#endif