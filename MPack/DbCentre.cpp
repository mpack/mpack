#include <QApplication>
#include <QSettings>
#include <QFile>
#include "DbCentre.h"

TPathRecItem PathSetting; // imp
TStateRecItem StateSetting; // imp

void LoadAppSettings( void )
{
    QSettings settings(QApplication::applicationDirPath() + "/ConfMPack.ini",
        QSettings::IniFormat);
    PathSetting.LastProjectFolder = settings.value("Path/LastProjectFolder", "").toString();
    PathSetting.LastStockXipFolder = settings.value("Path/LastStockXipFolder", "").toString();
    PathSetting.LastCustomXipFolder = settings.value("Path/LastCustomXipFolder", "").toString();
    PathSetting.LastDumpFolder = settings.value("Path/LastDumpFolder", "").toString();
    PathSetting.LastSourceFolder = settings.value("Path/LastSourceFolder", "").toString();
    PathSetting.LastSelectedItemIndex = settings.value("Path/LastSelectedItemIndex", 0).toInt();

    StateSetting.WindowMaxium = settings.value("State/WindowMaxium", true).toBool();
    StateSetting.MainFrmState = settings.value("State/MainFrmState", QByteArray()).toByteArray();
    StateSetting.ProjectLayoutState = settings.value("State/ProjectLayoutState", QByteArray()).toByteArray();
    StateSetting.MessageLayoutState = settings.value("State/MessageLayoutState", QByteArray()).toByteArray();

    StateSetting.RegEditorMaxium = settings.value("State/RegEditorMaxium", true).toBool();
    StateSetting.RegFrmState = settings.value("State/RegEditorMaxium", QByteArray()).toByteArray();
    StateSetting.KeyLayoutState = settings.value("State/KeyLayoutState", QByteArray()).toByteArray();
}

void SaveAppSettings( void )
{
    QSettings settings(QApplication::applicationDirPath() + "/ConfMPack.ini",
        QSettings::IniFormat);
    settings.beginGroup("Path");
    settings.setValue("LastProjectFolder", PathSetting.LastProjectFolder);
    settings.setValue("LastStockXipFolder", PathSetting.LastStockXipFolder);
    settings.setValue("LastCustomXipFolder", PathSetting.LastCustomXipFolder);
    settings.setValue("LastDumpFolder", PathSetting.LastDumpFolder);
    settings.setValue("LastSourceFolder", PathSetting.LastSourceFolder);
    settings.setValue("LastSelectedItemIndex", PathSetting.LastSelectedItemIndex);
    settings.endGroup();

    settings.beginGroup("State");
    settings.setValue("WindowMaxium", StateSetting.WindowMaxium);
    settings.setValue("MainFrmState", StateSetting.MainFrmState);
    settings.setValue("ProjectLayoutState", StateSetting.ProjectLayoutState);
    settings.setValue("MessageLayoutState", StateSetting.MessageLayoutState);

    settings.setValue("RegEditorMaxium", StateSetting.RegEditorMaxium);
    settings.setValue("RegFrmState", StateSetting.RegFrmState);
    settings.setValue("KeyLayoutState", StateSetting.KeyLayoutState);
    settings.endGroup();
}
