#ifndef LITEXIPBUILDERUNT_H
#define LITEXIPBUILDERUNT_H

#include <QtGUI/QDialog>
#include <QtGUI/QFileDialog>

namespace Ui {
    class TLiteXipBuilderFrm;
}

class TLiteXipBuilderFrm : public QDialog {
    Q_OBJECT
public:
    TLiteXipBuilderFrm(QWidget *parent = 0);
    ~TLiteXipBuilderFrm();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::TLiteXipBuilderFrm *ui;

protected:
    QFileDialog OpenSaveExeDialog, OpenSaveXipDialog;

private:
    unsigned fOriginalBaseAddress, fTargetBaseAddress;
    unsigned fOriginalOEP, fTargetOEP;

private:
    void RefreshEditor();

private slots:
    void onBrowseClicked();
    void onOverlayClicked();
    void onStockXipClicked();
    void onBuildClicked();
    void onBaseAddressEdited(QString NewValue);
    void onLoadAddressEdited(QString NewValue);
    void onUserdefineChanged(int);
    void onInjectorChanged(int);
};

bool GenerateXipFromFlatBuffer( void * Data, unsigned Size, unsigned BaseAddress, unsigned LoadAddress, QString XipFilename, int Compression, bool KeepBeginEmpty );
unsigned GetMainAreaStorageTail( QByteArray& FlatBuffer, unsigned ROMBaseAddress, unsigned ROMSize, unsigned& FreeSpace );

#endif // LITEXIPBUILDERUNT_H
