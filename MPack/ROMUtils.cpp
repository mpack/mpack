#include <windows.h>
#include <stdio.h>
#include "ROMUtils.h"
#include "XipStore.h"
//#include <vector>

//using std::vector;

#ifdef USENKCOMP
CEDECOMPRESSFN cedecompress= CEDecompress;
CECOMPRESSFN cecompress = CECompress;
#else
CEDECOMPRESS cedecompress = CEDecompress;
CECOMPRESS cecompress = CECompress;
#endif
//bool g_iswince3rom= false;

void WriteDummyMZHeader( QIODevice *qfile )
{
    IMAGE_DOS_HEADER dos;
    memset(&dos, 0, sizeof(dos));

    dos.e_magic = IMAGE_DOS_SIGNATURE;
    dos.e_cblp = 0x90;        // Bytes on last page of file
    dos.e_cp   = 3;           // Pages in file
    dos.e_cparhdr = 0x4;      // Size of header in paragraphs
    dos.e_maxalloc = 0xffff;  // Maximum extra paragraphs needed
    dos.e_sp = 0xb8;          // Initial SP value
    dos.e_lfarlc = 0x40;      // File address of relocation table
    dos.e_lfanew = 0xc0;      // File address of new exe header

    BYTE doscode[] = {
        0x0e, 0x1f, 0xba, 0x0e, 0x00, 0xb4, 0x09, 0xcd, 0x21, 0xb8, 0x01, 0x4c, 0xcd, 0x21, 0x54, 0x68,
        0x69, 0x73, 0x20, 0x70, 0x72, 0x6f, 0x67, 0x72, 0x61, 0x6d, 0x20, 0x63, 0x61, 0x6e, 0x6e, 0x6f,
        0x74, 0x20, 0x62, 0x65, 0x20, 0x72, 0x75, 0x6e, 0x20, 0x69, 0x6e, 0x20, 0x44, 0x4f, 0x53, 0x20,
        0x6d, 0x6f, 0x64, 0x65, 0x2e, 0x0d, 0x0d, 0x0a, 0x24, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    };
    //fwrite(&dos, 1, sizeof(dos), f);
    //fwrite(&doscode, 1, sizeof(doscode), f);
    qfile->write((char*)&dos, sizeof(dos));
    qfile->write((char*)&doscode, sizeof(doscode));

    //WriteBlanks(f, 0x40);
    qfile->write(QByteArray(0x40, 0));

    // ftell should be 0xc0 here.
}

DWORD FindFirstSegment(const o32_rom *o32, DWORD objcnt, int segtypeflag) {
    for (DWORD i = 0 ; i < objcnt ; i++) {
        if (o32[i].o32_flags&segtypeflag)
            return o32[i].o32_rva;
    }
    return 0;
}
DWORD CalcSegmentSizeSum(const o32_rom *o32, DWORD objcnt, int segtypeflag) {
    DWORD size = 0;
    for (DWORD i = 0 ; i < objcnt ; i++) {
        // vsize is not entirely correct, I should use the uncompressed size,
        // but, I don't know that here yet.
        if (o32[i].o32_flags&segtypeflag)
            size += o32[i].o32_vsize;
    }

    return size;
}

DWORD FiletimeToTime_t(const FILETIME *pft) {
    __int64 t;
    t = pft->dwHighDateTime;
    t <<= 32;
    t |= pft->dwLowDateTime;

    t /= 10000000LL;
    t -= 11644473600LL;

    return (DWORD)t;
}

void WriteE32Header(QIODevice *f, const ROMHDR *rom, const e32_rom *e32, const TOCentry *t, const o32_rom *o32) {
    e32_exe pe32;
    memset(&pe32, 0, sizeof(pe32));

    pe32.e32_magic[0] = 'P';
    pe32.e32_magic[1] = 'E';
    pe32.e32_cpu = rom->usCPUType;
    pe32.e32_objcnt = e32->e32_objcnt;
    pe32.e32_timestamp = FiletimeToTime_t(&t->ftTime);
    pe32.e32_symtaboff = 0;
    pe32.e32_symcount = 0;
    pe32.e32_opthdrsize = 0xe0;
    pe32.e32_imageflags = e32->e32_imageflags | IMAGE_FILE_RELOCS_STRIPPED;
    pe32.e32_coffmagic = 0x10b;
    pe32.e32_linkmajor = 6;
    pe32.e32_linkminor = 1;
    pe32.e32_codesize = CalcSegmentSizeSum(o32, e32->e32_objcnt, IMAGE_SCN_CNT_CODE);
    pe32.e32_initdsize = CalcSegmentSizeSum(o32, e32->e32_objcnt, IMAGE_SCN_CNT_INITIALIZED_DATA);
    pe32.e32_uninitdsize = CalcSegmentSizeSum(o32, e32->e32_objcnt, IMAGE_SCN_CNT_UNINITIALIZED_DATA);
    pe32.e32_entryrva = e32->e32_entryrva;
    pe32.e32_codebase = FindFirstSegment(o32, e32->e32_objcnt, IMAGE_SCN_CNT_CODE);
    pe32.e32_database = FindFirstSegment(o32, e32->e32_objcnt, IMAGE_SCN_CNT_INITIALIZED_DATA);
    pe32.e32_vbase = e32->e32_vbase;
    pe32.e32_objalign = 0x1000;
    pe32.e32_filealign = 0x200;
    pe32.e32_osmajor = 4;
    pe32.e32_osminor = 0;
    pe32.e32_usermajor;
    pe32.e32_userminor;
    pe32.e32_subsysmajor = e32->e32_subsysmajor;
    pe32.e32_subsysminor = e32->e32_subsysminor;
    pe32.e32_res1;
    pe32.e32_vsize = e32->e32_vsize;
    pe32.e32_hdrsize;    // *** set at a later moment - after alignment is known
    pe32.e32_filechksum = 0;
    pe32.e32_subsys = e32->e32_subsys;
    pe32.e32_dllflags;
    pe32.e32_stackmax = e32->e32_stackmax;
    pe32.e32_stackinit = 0x1000; // ?
    pe32.e32_heapmax = 0x100000; // ?
    pe32.e32_heapinit = 0x1000; // ?

    pe32.e32_res2;
    pe32.e32_hdrextra = STD_EXTRA; // nr of directories

    pe32.e32_unit[EXP] = e32->e32_unit[EXP];
    pe32.e32_unit[IMP] = e32->e32_unit[IMP];
    pe32.e32_unit[RES] = e32->e32_unit[RES];
    pe32.e32_unit[EXC] = e32->e32_unit[EXC];
    pe32.e32_unit[SEC] = e32->e32_unit[SEC]; // always 0

    // relocation info is always missing
    // pe32.e32_unit[FIX]= e32->e32_unit[FIX];

    // deb struct has pointer outside known filearea
    //pe32.e32_unit[DEB]= e32->e32_unit[DEB];
    pe32.e32_unit[IMD] = e32->e32_unit[IMD]; // always 0
    pe32.e32_unit[MSP] = e32->e32_unit[MSP]; // always 0

    pe32.e32_unit[RS4].rva = e32->e32_sect14rva;
    pe32.e32_unit[RS4].size = e32->e32_sect14size;

    //fwrite(&pe32, 1, sizeof(pe32), f);
    f->write((char*)&pe32, sizeof(pe32));
}


DWORD g_segmentNameUsage[5];
char *g_segmentNames[5] = { ".text", ".data", ".pdata", ".rsrc", ".other" };
DWORD WriteO32Header(QIODevice *f, const e32_rom *e32, const o32_rom *o32) {
    o32_obj po32;
    memset(&po32, 0, sizeof(po32));

    int segtype;
    // TODO: mix resource idata rdata check n complier for CODE/RSRC
    //if (e32->e32_unit[RES].rva == o32->o32_rva && e32->e32_unit[RES].size <= o32->o32_vsize)
    if (e32->e32_unit[RES].rva >= o32->o32_rva && e32->e32_unit[RES].rva < o32->o32_rva + o32->o32_vsize)
        segtype = ST_RSRC;
    else if (e32->e32_unit[EXC].rva == o32->o32_rva && e32->e32_unit[EXC].size == o32->o32_vsize)
        segtype = ST_PDATA;
    else if (o32->o32_flags&IMAGE_SCN_CNT_CODE)
        segtype = ST_TEXT;
    else if (o32->o32_flags&IMAGE_SCN_CNT_INITIALIZED_DATA)
        segtype = ST_DATA;
    else if (o32->o32_flags&IMAGE_SCN_CNT_UNINITIALIZED_DATA)
        segtype = ST_PDATA;
    else
        segtype = ST_OTHER;

    if (g_segmentNameUsage[segtype]) {
        _snprintf_s((char*)po32.o32_name, _countof(po32.o32_name), 8, "%s%ld", g_segmentNames[segtype], g_segmentNameUsage[segtype]);
    } else {
        _snprintf_s((char*)po32.o32_name, _countof(po32.o32_name), 8, "%s", g_segmentNames[segtype]);
    }

    g_segmentNameUsage[segtype]++;

    po32.o32_vsize =         o32->o32_vsize;
    po32.o32_rva =           /*false  ? o32->o32_rva :*/ o32->o32_realaddr - e32->e32_vbase;
    po32.o32_psize =         0; // *** set at a later moment - after uncompressed size is known
    po32.o32_dataptr =       0; // *** set at a later moment - after uncompressed size is known
    po32.o32_realaddr =      0; // file pointer to relocation table
    po32.o32_access =        0; // file pointer to line numbers
    po32.o32_temp3 =         0; // number of relocations + number of line numbers
    po32.o32_flags =         o32->o32_flags & ~IMAGE_SCN_COMPRESSED;

    f->write((char*)&po32, sizeof(po32));

    return po32.o32_rva;
}

void WriteAlignment(QIODevice *f, DWORD pagesize)
{
    DWORD curofs= f->pos();
    if (curofs%pagesize) {
        //WriteBlanks(f, pagesize-(curofs%pagesize));
        f->write(QByteArray(pagesize-(curofs%pagesize), 0));
    }
}

bool isObjectContainsSection(const o32_rom& o32, const info& inf) {
    return o32.o32_rva <= inf.rva && inf.rva + inf.size <= o32.o32_rva + o32.o32_vsize;
}

QByteArray qDECompressMSLZX( const uchar* data, int nbytes, int outputbytes )
{
    if (!data) {
        qWarning("qDECompressMSLZX: Data is null");
        return QByteArray();
    }
    if (nbytes <= 0) {
        if (nbytes < 0 || (outputbytes != 0))
            qWarning("qDECompressMSLZX: Input data is corrupted");
        return QByteArray();
    }
    ulong expectedSize = outputbytes;
    ulong len = qMax(expectedSize, 1ul);

    QByteArray Result;
    Result.resize(outputbytes + 4096);
    int buflen = cedecompress(LPBYTE(data), nbytes, LPBYTE(Result.data()), outputbytes, 0, 1, 4096);

    if (unsigned(buflen) != CEDECOMPRESS_FAILED) {
        Result.resize(buflen);
    } else {
        Result.clear();
    }

    return Result;
}

QByteArray qCompressMSLZX(const uchar* data, int nbytes) {
    if (!data) {
        qWarning("qCompressMSLZX: Data is null");
        return QByteArray();
    }
    if (nbytes <= 0) {
        if (nbytes < 0)
            qWarning("qCompressMSLZX: Input data is corrupted");
        return QByteArray();
    }
    ulong sourceSize = nbytes;
    ulong len = qMax(sourceSize, 1ul);

    QByteArray Result;
    Result.resize(sourceSize + 4096);
    int buflen = cecompress(LPBYTE(data), sourceSize, LPBYTE(Result.data()), sourceSize, 1, 4096);

    if (buflen != CEDECOMPRESS_FAILED) {
        Result.resize(qMin(buflen, int(sourceSize)));
        if (buflen >= sourceSize) {
            // qMin let Result shrink to sourceSize
            memcpy_s(Result.data(), sourceSize, data, sourceSize); // Failure safe. else keep
        }
    } else {
        Result.resize(sourceSize);
        memcpy_s(Result.data(), sourceSize, data, sourceSize);
    }

    return Result;
}

#ifdef __GNUC__
/*
quint64 __fastcall SearchGap(LPVOID Buffer, quint32 Size) {
    __asm__ __volatile__ (
        "push  %%edi;"
        "mov   %%ecx, %%edi;" // Buffer
        "mov   %%edx, %%ecx;" // Size
        "xor   %%eax, %%eax;"
        "repne scasl;"
        "push  %%edi;"
        "repe  scasl;"
        "mov   %%edi, %%edx;"
        "pop   %%eax;"
        "pop   %%edi;"
        :
        :
        : "eax", "ecx", "edx"
    );
}*/
#else
// CD
// ECX = Buffer EDX = Size
// EAX:EDX Start:End
__declspec( naked ) quint64 __fastcall SearchGap(LPVOID Buffer, quint32 Size) {
    __asm {
        PUSH  EDI
        MOV   EDI, ECX // Buffer
        MOV   ECX, EDX // Size
        XOR   EAX, EAX
        REPNE SCASD
        PUSH  EDI
        REPE  SCASD
        MOV   EDX, EDI
        POP   EAX
        POP   EDI
        RET
    }
}
#endif

#ifdef __GNUC__
/*
quint32 __fastcall CalcChecksum32(LPVOID Buffer, quint32 Size) {
    __asm__ __volatile__ (
        "test  %%edx, %%edx;"
        "jnz   loc_ok;"
        "xor   %%eax, %%eax;"
        "jmp   loc_ret;"
    "loc_ok:"
        //PUSH  ECX
        "push  %%esi;"
        "push  %%edi;"
        "mov   %%ecx, %%edi;"  // EDI := Buffer
        "mov   %%edx, %%esi;"  // ESI := Size;
        "xor   %%ecx, %%ecx;"  // I := 0;
        "xor   %%eax, %%eax;"  // Result := 0;


    "loc_addbytes:"
        "movzbl (%%ecx, %%edi), %%edx;"
        "add   %%edx, %%eax;"
        "inc   %%ecx;"
        "cmp   %%esi, %%ecx;"
        "jb    loc_addbytes;" // Check if we've reached the end of the stream

        "pop   %%edi;"
        "pop   %%esi;"
        //POP   ECX
     "loc_ret:"
        :
        :
        : "eax", "ecx", "edx"
    );
}
*/
#else
// CD
// ECX = Buffer, EDX = Size
// EAX Result
__declspec( naked ) quint32 __fastcall CalcChecksum32(LPVOID Buffer, quint32 Size) {
    __asm {
        TEST  EDX, EDX
        JNZ   loc_ok
        XOR   EAX, EAX
        JMP   loc_ret
    loc_ok:
        //PUSH  ECX
        PUSH  ESI
        PUSH  EDI
        MOV   EDI, ECX  // EDI := Buffer
        MOV   ESI, EDX  // ESI := Size;
        XOR   ECX, ECX  // I := 0;
        XOR   EAX, EAX  // Result := 0;


    loc_addbytes:
        XOR   EDX, EDX
        MOV   DL, [ECX+EDI]
        ADD   EAX, EDX
        INC   ECX
        CMP   ECX, ESI
        JB    loc_addbytes // Check if we've reached the end of the stream

        POP   EDI
        POP   ESI
        //POP   ECX
    loc_ret:
        RET
    }
}
#endif

int AlignByBlocksize(int basesize, int blocksize) {
    if (blocksize == 1) {
        return basesize;
    }
    int result = ((basesize + (basesize >  0? (blocksize - 1) : 0)) / blocksize) * blocksize;
    return result;
}

int AlignAddressByBlocksizeCrazy(DWORD baseaddress, int basesize, int newsize, int blocksize) {
    if (blocksize == 1) {
        return newsize - basesize;
    }
    bool shrink = newsize < basesize;
    int result;

    DWORD baseend = baseaddress + basesize;
    DWORD orgnextbegin = ((baseend + blocksize - 1) / blocksize) * blocksize; // we trust every begin start at 4 8 C 0
    DWORD newnextbegin = ((baseaddress + newsize + blocksize - 1) / blocksize) * blocksize; // and we follow the semi rule

    result = (long long)newnextbegin - (long long)orgnextbegin;

    return result;
}

bool LoadFlatBufferFromXip( QIODevice *xipfile, QByteArray& FlatBuffer, DWORD& ROMBaseAddress, DWORD& ROMSize, DWORD& ROMLoadAddress )
{
    bool Result = false;

    // Detect B00FF n RLE
    xipfile->open(QIODevice::ReadOnly);
    TRLEXIPMBR MBR;

    if (sizeof(MBR) != xipfile->read(LPSTR(&MBR), sizeof(MBR))) {
        //perror("fread");
        xipfile->close();
        return false;
    }
    DWORD filesize = xipfile->size();

    if (_strnicmp((char*)&MBR.Magic[0], "B000FF", 6) == 0) {
        // Maybe M8
        Result = true;
        //logprintf("Try to converting M8 xip to nb0 format...\n");
        //logprintf("RomAddress: 0x%08X\tRomSize: 0x%08X\n", MBR.RomAddress, MBR.RomSize);
        ROMBaseAddress = MBR.RomAddress;
        ROMSize = MBR.RomSize;

        FlatBuffer.resize(MBR.RomSize);
        FlatBuffer.fill(0);

        while (xipfile->pos() < filesize && FlatBuffer.size() <= MBR.RomSize) {
            TChunkHeader Chunk;
            xipfile->read(LPSTR(&Chunk), sizeof(Chunk));
            if (Chunk.TargetAddress == 0) {
                // Tail
                //logprintf("RomLoadAddress: 0x%08X\nPlease take these arguments for xip.bin's generation.\n", Chunk.SourceSize);
                ROMLoadAddress = Chunk.SourceSize;
                break;
            }
            if (FlatBuffer.size() < Chunk.TargetAddress - MBR.RomAddress + Chunk.SourceSize) {
                // will never here
                FlatBuffer.resize(Chunk.TargetAddress - MBR.RomAddress + Chunk.SourceSize);
            }
            char *dangerpos = LPSTR(FlatBuffer.constData());
            dangerpos += (Chunk.TargetAddress - MBR.RomAddress);
            xipfile->read(dangerpos, Chunk.SourceSize);
        }
        if (FlatBuffer.size() < MBR.RomSize) {
            FlatBuffer.resize(MBR.RomSize);
        }
    }
    xipfile->close();
    return Result;
    // TODO: Move to ROMUtils
}