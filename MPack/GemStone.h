#ifndef _GEM_STONE_H
#define _GEM_STONE_H

#include <QMap>

typedef QMap < quint32, int > DualIntMap;

class TBlockKeeper {
protected:
    DualIntMap fFreeSpaces, fAppliedSpace, fBrickHolders;
public:
    bool AddFreeSpace(quint32 address, int size);
    bool ApplyBlock(int size, quint32& address);
    bool PlaceBrick(quint32 address, int size);
#ifdef _DEBUG
    DualIntMap FreeForDump();
#endif
};

#endif