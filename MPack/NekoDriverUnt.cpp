#include "NekoDriverUnt.h"
#include "MainUnt.h"
#include "ROMUtils.h"
#include <QtGUI/QIcon>
#include <QtCore/QFileInfo>
#include <QtCore/QTranslator>
#include <QtCore/QCryptographicHash>
#include <QtGUI/QPainter>
#include <LibMUIMaker.h>

PNekoDriver nekodriver;

bool TNekoDriver::BuildProjectFromXip( QString xipfilename ) {
    bool Result = false;
    Result = fXipHelper.LoadFlatBufferFromXip(xipfilename);
    if (Result == false) {
        return Result;
    }
    Result = fXipHelper.ParseFlatBuffer();

    if (Result) {
        fXipHelper.UpdateProjectModifiedStatus(true);
    }

    return Result;
}

bool TNekoDriver::KeepProjectView( QTreeWidget *treeview )
{
    bool Result = false;
    TBundleRec& rec = fXipHelper.GetSortedBuffer();

    treeview->clear();
    QList<QTreeWidgetItem *> treeitems;
    int bundleindex = 0;
    for (TBundleRec::iterator recit = rec.begin(); recit != rec.end(); recit++) {
        TSourceSlot sourceslot = GetActiveSlotForUser(*recit);
        TEntryType stocktype = BundleType2EntryType((*recit).ItemType[sourceslot]);

        QString entryname = (*recit).Filename[sourceslot];
        if (fProjectViewNameFilter.isEmpty() == false) {
            // try to continue
            if (entryname.contains(fProjectViewNameFilter, Qt::CaseInsensitive) == false) {
                bundleindex++; // dangerous
                continue;
            }
        }
        bool skip = true;
        if (fTranslateViewFilter.contains(ftOriginal) && sourceslot == stStock){
            skip = false;
        }
        if (fTranslateViewFilter.contains(ftModified) && (sourceslot != stStock || (*recit).ItemNuked[sourceslot] || (*recit).StockPresent == false)) {
            skip = false;
        }
        if (fTranslateViewFilter.contains(ftSession) && (sourceslot == stIncoming)) {
            skip = false;
        }
        if (skip) {
            bundleindex++; // dangerous
            continue;
        }
        QString entrytype = stocktype==etFile?QObject::tr("File"):(stocktype==etModule?QObject::tr("Module"):QObject::tr("Memory"));
        QString entrystage = (*recit).Stage[sourceslot]==0?QObject::tr("Main"):QObject::tr("Addon");
        QStringList itemtext;
        itemtext << entryname << entrytype << entrystage;
        QTreeWidgetItem *treeitem = new QTreeWidgetItem((QTreeWidget*)0, itemtext);
        QIcon fileicon;
        if ((*recit).ItemType[sourceslot] == btPEModule || (*recit).ItemType[sourceslot] == btPEFile) {
            if (entryname.endsWith(".exe", Qt::CaseInsensitive)) {
                fileicon = fExeIcon;
            } else {
                fileicon = fDllIcon;
            }
        } else {
            if (entryname.endsWith(".lnk", Qt::CaseInsensitive)) {
                fileicon = fLinkIcon;
            } else if (entryname.endsWith(".p7b", Qt::CaseInsensitive)) {
                fileicon = fCertIcon;
            } else if (entryname.endsWith(".png", Qt::CaseInsensitive) || entryname.endsWith(".jpg", Qt::CaseInsensitive)) {
                fileicon = fImageIcon;
            } else if (entryname.endsWith(".wav", Qt::CaseInsensitive) || entryname.endsWith(".mp3", Qt::CaseInsensitive)) {
                fileicon = fMP3Icon;
            } else {
                fileicon = fStdIcon;
            }
        }
        if ((*recit).ItemNuked[sourceslot]) {
            // TODO: cache
            QPixmap pixmap = fileicon.pixmap(16, 16);
            QPainter painter(&pixmap);
            fCloakingIcon.paint(&painter, 0, 0, 16, 16, Qt::AlignCenter, QIcon::Normal, QIcon::Off);
            fileicon.addPixmap(pixmap);
        }
        treeitem->setIcon(0, fileicon);
        treeitem->setData(0, Qt::UserRole, bundleindex);
        if (sourceslot != stStock) {
            treeitem->setData(0, Qt::ForegroundRole, QColor(sourceslot == stPrevious?0x199A00:0x52E634));
        }
        if ((*recit).ItemNuked[sourceslot]) {
            treeitem->setData(0, Qt::BackgroundRole, QColor(sourceslot == stStock?0xC4C4C4:0x878787));
        }
        treeitems.append(treeitem);
        bundleindex++;
    }
    treeview->insertTopLevelItems(0, treeitems);
    treeview->resizeColumnToContents(0);
    treeview->resizeColumnToContents(1);
    treeview->resizeColumnToContents(2);
    treeview->header()->setClickable(true);

    return Result;
}

bool TNekoDriver::SaveProjectToFile( QString mpkfilename )
{
    bool Result = false;

    // TODO: Safer for dual slot
    QFile originalfile(mpkfilename);
    bool keepbackup = originalfile.exists();
    if (keepbackup) {
        originalfile.rename(mpkfilename + ".bak");
    }
    Result = fXipHelper.SaveParsedProjectToFile(mpkfilename);
    if (keepbackup && Result) {
        originalfile.remove();
    }

    if (Result) {
        fXipHelper.UpdateProjectModifiedStatus(false);
    }

    return Result;
}


bool TNekoDriver::LoadProjectFromFile( QString mpkfilename )
{
    bool Result = false;

    Result = fXipHelper.LoadProjectFromFile(mpkfilename);

    return Result;
}

TBundleRecItem* TNekoDriver::GetBundleItemInfo( int index )
{
    TBundleRecItem* Result = NULL;
    TBundleRec& rec = fXipHelper.GetSortedBuffer();

    if (index < 0 || index >= rec.size()) {
        return Result;
    }
    Result = &rec[index];

    return Result;
}

bool TNekoDriver::DumpRefBinaray( int index, QIODevice *file, bool onlystock )
{
    bool Result = false;
    TBundleRec& rec = fXipHelper.GetSortedBuffer();

    TBundleRecItem item = rec[index];

    if (item.StockPresent == false && onlystock) {
        return Result; 
    }
    TSourceSlot sourceslot = onlystock?stStock:GetActiveSlotForUser(item);

    if (item.ItemType[sourceslot] == btRawFile) {
        if (item.FileInfo[sourceslot].nCompFileSize != item.FileInfo[sourceslot].nRealFileSize) {
            QByteArray uncompressedstream = qDECompressMSLZX((uchar*)item.RawBuffer[sourceslot].data(), item.RawBuffer[sourceslot].size(), item.FileInfo[sourceslot].nRealFileSize);
            file->write(uncompressedstream);
        } else {
            file->write(item.RawBuffer[sourceslot]);
        }
    }
    if (item.ItemType[sourceslot] == btPEModule) {
        file->write(item.RawBuffer[sourceslot]);
    }
    if (item.ItemType[sourceslot] == btRawCode) {
        file->write(item.RawBuffer[sourceslot]);
    }
    return Result;
}

bool TNekoDriver::DumpProjectContent( QString outputpath, QLabel* label, QProgressBar* bar )
{
    bool Result = false;
    TBundleRec& rec = fXipHelper.GetSortedBuffer();

    int dumpprecent = 0;
    int i = 0;
    for (TBundleRec::iterator it = rec.begin(); it != rec.end(); it++) {
        QString subdir;
        if (it->ItemType[stStock] == btRawFile) {
            subdir = "files";
        } else if (it->ItemType[stStock] == btPEModule) {
            subdir = "modules";
        } else if (it->ItemType[stStock] == btRawCode) {
            subdir = "codes";
        }
        QDir targetdir(outputpath + "/" + subdir);
        if (targetdir.exists() == false) {
            targetdir.mkpath(targetdir.path());
        }
        if ((*it).StockPresent == false) {
            i++;
            continue;
        }
        if (bar != NULL) {
            int currpercent = (i * 100) / rec.size();
            if (currpercent != dumpprecent) {
                dumpprecent = currpercent;
                bar->setValue(dumpprecent);
            }
        }
        if (label != NULL) {
            label->setText(it->Filename[stStock]);
        }
        QFile file(outputpath + "/" + subdir + "/" + it->Filename[stStock]);
        file.open(QFile::ReadWrite);
        DumpRefBinaray(i, &file, true);
        file.close();
        //QFileInfo info(file.fileName());
#ifdef _LINUX
        // TODO: filetime to unixutctime?
        _utime(file.fileName(), it->FileInfo[stStock].ftTime);
#else
        HANDLE dummyHandle = CreateFileW(
            (WCHAR*)file.fileName().utf16(), FILE_WRITE_ATTRIBUTES, FILE_SHARE_WRITE, NULL, OPEN_EXISTING,
            FILE_ATTRIBUTE_NORMAL, 0
        );
        //DateTime2FileTime(DateTime_System2Local(fLastModify), FT);
        if (it->ItemType[stStock] == btRawFile) {
            SetFileTime(dummyHandle, NULL, NULL, &it->FileInfo[stStock].ftTime);
        } else if (it->ItemType[stStock] == btPEModule) {
            // timestamp show sec from 1970 1 1
            FILETIME ft;
            *((quint64 *)(&ft)) = qint64(it->E32Info[stStock].e32_timestamp) * 10000000LL + 0x019DB1DED53E8000LL;
            //*((unsigned long long *)(&ft)) = __int64(it->E32Info.e32_timestamp) * 10000000LL + ((1970-1601)*365+89)*24*60*60*1000*1000LL;
            SetFileTime(dummyHandle, NULL, NULL, &ft);
        }
        CloseHandle(dummyHandle);
#endif
        i++;
    }
    return Result;
}

bool TNekoDriver::BuildXipFromBuffer( QString xipfilename, int compression, QProgressBar* bar )
{
    bool Result = false;
    if (bar != NULL) {
        QObject::connect(&fXipHelper, SIGNAL(xipProgressChanged(int)),
            bar, SLOT(setValue(int)));
    }
    Result = fXipHelper.GenerateXipFromFlatBuffer(xipfilename, compression); // 36
    if (bar != NULL) {
        QObject::disconnect(&fXipHelper, SIGNAL(xipProgressChanged(int)),
            bar, SLOT(setValue(int)));
    }
    return Result;
}

bool TNekoDriver::PrepareBufferStock( QLabel *label, QProgressBar* bar )
{
    bool Result = false;
    if (label != NULL) {
        QObject::connect(&fXipHelper, SIGNAL(prepareBufferStageChanged(QString)),
            label, SLOT(setText(QString)));
    }
    if (bar != NULL) {
        QObject::connect(&fXipHelper, SIGNAL(prepareBufferProgressChanged(int)),
            bar, SLOT(setValue(int)));
    }
    Result = fXipHelper.PrepareFlatBufferFromProjectSemiStock();
    if (label != NULL) {
        QObject::disconnect(&fXipHelper, SIGNAL(prepareBufferStageChanged(QString)),
            label, SLOT(setText(QString)));
    }
    if (bar != NULL) {
        QObject::disconnect(&fXipHelper, SIGNAL(prepareBufferProgressChanged(int)),
            bar, SLOT(setValue(int)));
    }
    return Result;
}

bool TNekoDriver::ReplaceBundleRecContent( int index, QByteArray& newcontent )
{
    bool Result = false;
    TBundleRec& rec = fXipHelper.GetSortedBuffer();
    if (index < 0 || index >= rec.size()) {
        return Result;
    }

    TBundleRecItem& item = rec[index]; // modify qua
    TSourceSlot sourceslot = GetActiveSlotForUser(item);
    TEntryType entrytype = BundleType2EntryType(item.ItemType[sourceslot]);
    if (entrytype == etUnkown) {
        return Result;
    }

#ifdef USE_HASH_COMPARE
    if (item.RawHash[sourceslot].isEmpty() && item.RawBuffer[sourceslot].isEmpty() == false) {
        item.RawHash[sourceslot] = QCryptographicHash::hash(item.RawBuffer[sourceslot], QCryptographicHash::Sha1);
    }
    QByteArray newcontentlzx;
    if (item.ItemType[sourceslot] == btRawFile && item.FileInfo[sourceslot].nCompFileSize != item.FileInfo[sourceslot].nRealFileSize) {
        newcontentlzx = qCompressMSLZX((uchar*)newcontent.data(), newcontent.size());
    }
    QByteArray newhash = QCryptographicHash::hash(newcontentlzx.isEmpty()?newcontent:newcontentlzx, QCryptographicHash::Sha1);

    if (newhash == item.RawHash[sourceslot]) {
        // newcontent dropped
        return Result;
    }
#else
    bool different = false;
    if (entrytype == etFile && item.FileInfo[sourceslot].nRealFileSize != newcontent.size()) {
        different = true;
    }
    QByteArray newcontentlzx;
    if (entrytype == etFile && different == false) {
        // size matched
        QByteArray sourceuncompressed;
        if (item.ItemType[sourceslot] == btRawFile && item.FileInfo[sourceslot].nCompFileSize != item.FileInfo[sourceslot].nRealFileSize) {
            sourceuncompressed = qDECompressMSLZX((uchar*)item.RawBuffer[sourceslot].data(), item.RawBuffer[sourceslot].size(), item.FileInfo[sourceslot].nRealFileSize);
            different = sourceuncompressed.size() != newcontent.size();
            if (different == false) {
                // size equal
                different = memcmp(sourceuncompressed.data(), newcontent.data(), qMin(sourceuncompressed.size(), newcontent.size())) != 0;
            }
        }
        sourceuncompressed.clear();
        if (different) {
            newcontentlzx = qCompressMSLZX((uchar*)newcontent.data(), newcontent.size());
        }
    }
    if (entrytype == etModule && different == false) {
        different = item.RawBuffer[sourceslot].size() != newcontent.size();
        if (different == false) {
            // size equal
            different = memcmp(item.RawBuffer[sourceslot].data(), newcontent.data(), qMin(item.RawBuffer[sourceslot].size(), newcontent.size())) != 0;
        }
    }
    if (entrytype == etCopy && different == false) {
        Q_ASSERT(item.RawBuffer[sourceslot].size() == item.CopyInfo[sourceslot].ulCopyLen);
        different = item.RawBuffer[sourceslot].size() != newcontent.size();
        if (different == false) {
            // size equal
            different = memcmp(item.RawBuffer[sourceslot].data(), newcontent.data(), qMin(item.RawBuffer[sourceslot].size(), newcontent.size())) != 0;
        }
    }
    if (different == false) {
        return Result; // untouch 
    }
#endif


    if (item.StockPresent) {
        item.CopyMetaData(stStock, stWork);
    } else {
        item.CopyMetaData(GetActiveSlotForUser(item), stWork);
    }

    // we totally use stWork for delta check
    if (entrytype == etFile) {
        if (item.FileInfo[stWork].nCompFileSize != item.FileInfo[stWork].nRealFileSize) {
            if (newcontentlzx.isEmpty()) {
                item.RawBuffer[stWork] = qCompressMSLZX((uchar*)newcontent.data(), newcontent.size());
            } else {
                item.RawBuffer[stWork] = newcontentlzx;
            }
            item.FileInfo[stWork].nCompFileSize = item.RawBuffer[stWork].size();
            item.FileInfo[stWork].nRealFileSize = newcontent.size();
        } else {
            item.RawBuffer[stWork] = newcontent;
            item.FileInfo[stWork].nCompFileSize = item.FileInfo[stWork].nRealFileSize = item.RawBuffer[stWork].size();
        }
        Result = true;
    }
    if (entrytype == etModule) {
        // TODO: Check stock rules
        if (newcontent.size() >= 2) {
            if (newcontent.startsWith("MZ")) {
                // quickly check done, start detail analyst
                TPEDirector director(newcontent.data(), newcontent.size());
                IMAGE_NT_HEADERS header = director.GetNTHeaders();
                Q_ASSERT(item.E32Info[stWork].e32_vbase % 0x1000UL == 0); // short than align by address?
                DWORD maxsize = AlignAddressByBlocksizeCrazy(item.E32Info[stWork].e32_vbase, 0, item.E32Info[stWork].e32_vsize, 0x1000UL);
                // what a fuck delta == 0?
                if (header.OptionalHeader.AddressOfEntryPoint == item.E32Info[stWork].e32_vbase && header.OptionalHeader.SizeOfImage <= maxsize) {
                    Result = true; // we will fix size of sections later?
                } else {
                    // case1: may shrink by remove reloc section
                    // case2: wanna relocation
                    // case3: pure resource without OEP. can do deep drop
                    BOOL Dropable;
                    int relocindex = director.GetPureRelocSectionIndex(Dropable);
                    if (relocindex != -1 && Dropable == TRUE) {
                        if (header.OptionalHeader.SizeOfImage - director.GetSectionHeaders()[relocindex].Misc.VirtualSize <= maxsize) {
                            Result = true;
                        }
                    }
                }
            }
        }
        item.RawBuffer[stWork] = newcontent;
        // wait for merge
        Result = true;
    }
    if (entrytype == etCopy) {
        Q_ASSERT(item.CopyInfo[stWork].ulDestLen >= newcontent.size());
        item.RawBuffer[stWork] = newcontent;
        item.CopyInfo[stWork].ulCopyLen = item.RawBuffer[stWork].size();
        Result = true;
    }
    if (Result) {
        if (item.IncomingPresent) {
            // do backup
            item.CopyMetaData(stIncoming, stPrevious);
            item.CopyBuffer(stIncoming, stPrevious);
            item.PreviousPresent = true;
        } else {
            // Init
            item.IncomingPresent = true;
        }
        item.CopyMetaData(stWork, stIncoming);
        item.CopyBuffer(stWork, stIncoming);
        fXipHelper.UpdateProjectModifiedStatus(true);
    }
    item.Filename[stWork].clear();
    item.RawBuffer[stWork].clear();
    item.RawHash[stWork].clear();
    return Result;
}


bool TNekoDriver::MarkBundleRecAsDeleted( int index )
{
    bool Result = false;
    TBundleRec& rec = fXipHelper.GetSortedBuffer();
    if (index < 0 || index >= rec.size()) {
        return Result;
    }

    TBundleRecItem& item = rec[index]; // modify qua
    if (item.IncomingPresent) {
        // fake incoming first
        item.ItemNuked[stIncoming] = true; // wait for delta
        Result = true;
    } else if (item.PreviousPresent) {
        // Init
        item.ItemNuked[stPrevious] = true; // wait for delta
        Result = true;
    } else if (item.StockPresent) {
        item.ItemNuked[stStock] = true;
        Result = true;
    }
    if (Result) {
        fXipHelper.UpdateProjectModifiedStatus(true);
    }
    return Result;
}

bool TNekoDriver::ClearBundleRecDeletedMark( int index )
{
    bool Result = false;
    TBundleRec& rec = fXipHelper.GetSortedBuffer();
    if (index < 0 || index >= rec.size()) {
        return Result;
    }

    TBundleRecItem& item = rec[index]; // modify qua
    if (item.IncomingPresent) {
        // fake incoming first
        item.ItemNuked[stIncoming] = false; // wait for delta
        Result = true;
    } else if (item.PreviousPresent) {
        // Init
        item.ItemNuked[stPrevious] = false; // wait for delta
        Result = true;
    } else if (item.StockPresent) {
        item.ItemNuked[stStock] = false;
        Result = true;
    }
    if (Result) {
        fXipHelper.UpdateProjectModifiedStatus(true);
    }
    return Result;
}


bool TNekoDriver::RollbackToStockSlot( int index )
{
    bool Result = false;
    TBundleRec& rec = fXipHelper.GetSortedBuffer();
    if (index < 0 || index >= rec.size()) {
        return Result;
    }

    TBundleRecItem& item = rec[index]; // modify qua
    if (item.IncomingPresent) {
        // fake incoming first
        item.IncomingPresent = false;
        item.RawBuffer[stIncoming].clear();
        item.Filename[stIncoming].clear();
        Result = true;
    } else if (item.PreviousPresent) {
        // only fake second turn
        item.PreviousPresent = false;
        item.RawBuffer[stPrevious].clear();
        item.Filename[stPrevious].clear();
        Result = true;
    }
    if (Result) {
        fXipHelper.UpdateProjectModifiedStatus(true);
    }
    return Result;
}

bool TNekoDriver::ApplyProjectViewNameFilter( QString newfilter )
{
    if (fProjectViewNameFilter.compare(newfilter, Qt::CaseInsensitive) == 0) {
        return false;
    }
    fProjectViewNameFilter = newfilter;
    return true;
}

bool TNekoDriver::ApplyTranslateViewFilter( TFilterType filter, bool enabled )
{
    if (fTranslateViewFilter.contains(filter) != enabled) {
        if (enabled) {
            fTranslateViewFilter.insert(filter);
        } else {
            fTranslateViewFilter.remove(filter);
        }
        return true;
    }
    return false;
}

bool TNekoDriver::IsProjectEmpty()
{
    return fXipHelper.GetProjectEmpty();
}

bool TNekoDriver::IsProjectModified()
{
    return fXipHelper.GetProjectModified();
}

TNekoDriver::TNekoDriver(TMainFrm* frm)
{
    // we create from MainFrm.
    QObject::connect(&fXipHelper, SIGNAL(logStored(QString, TLogType)),
        frm, SLOT(writeLog(QString, TLogType)));
    fTranslateViewFilter.insert(ftOriginal);
    fTranslateViewFilter.insert(ftModified);
    fTranslateViewFilter.insert(ftSession);
    fTranslateViewFilter.insert(ftText);
    fTranslateViewFilter.insert(ftImage);

    fStdIcon.addFile(":/MainFrm/PpsDES/16x16-tango/empty.png"); // TODO: IconProvider, fIkonStd
    fExeIcon.addFile(":/MainFrm/PpsDES/16x16-tango/exec.png");
    fDllIcon.addFile(":/MainFrm/PpsDES/16x16-tango/emblem-system.png");
    fLinkIcon.addFile(":/MainFrm/PpsDES/16x16-tango/emblem-symbolic-link.png");
    fCertIcon.addFile(":/MainFrm/PpsDES/16x16-tango/application-certificate.png");
    fImageIcon.addFile(":/MainFrm/PpsDES/16x16-tango/image.png");
    fMP3Icon.addFile(":/MainFrm/PpsDES/16x16-tango/sound.png");
    fDelIcon.addFile(":/MainFrm/PpsDES/16x16-tango/trashcan_full.png");
    fCloakingIcon.addFile(":/MainFrm/PpsDES/Cloaking.png");
}

int TNekoDriver::GetBundleIndexByName( QString itemname )
{
    // DONE: an reverse lookup table
    // TODO: sync on rename
    int Result = -1;
    TBundleRec& rec = fXipHelper.GetSortedBuffer();
    if (rec.isEmpty()) {
        return Result;
    }
    QByteArray itemnamelatin = itemname.toAscii().toLower();
    if (fNameIndexLookupTable.isEmpty()) {
        TBundleRec::iterator recit = rec.end();
        int recindex = rec.size();
        while (recit != rec.begin()) {
            recit--;
            recindex--;
            TSourceSlot slot = GetActiveSlotForUser(*recit);
            QByteArray key = recit->Filename[slot].toLower();
            fNameIndexLookupTable.insertMulti(key, recindex);
        }
    }
    QList < int > indexes = fNameIndexLookupTable.values(itemnamelatin);
    if (indexes.size() == 1) {
        Result = indexes.first();
    } else if (indexes.size() > 1) {
        // last file will cloak all old same file
        // sort
        qSort(indexes);
        Result = indexes.last();
    }
    return Result;
}

bool TNekoDriver::RenameBundleRec( int index, QByteArray newname )
{
    bool Result = false;
    TBundleRec& rec = fXipHelper.GetSortedBuffer();
    if (index < 0 || index >= rec.size()) {
        return Result;
    }

    TBundleRecItem& item = rec[index]; // modify qua
    TSourceSlot sourceslot = GetActiveSlotForUser(item);
    TEntryType entrytype = BundleType2EntryType(item.ItemType[sourceslot]);
    if (entrytype == etFile || entrytype == etModule) {
        if (item.IncomingPresent) {
            item.CopyMetaData(stIncoming, stPrevious);
            item.CopyBuffer(stIncoming, stPrevious);
            item.PreviousPresent = true;
        } else {
            // Init
            item.IncomingPresent = true;
            item.CopyMetaData(stStock, stIncoming);
            item.CopyBuffer(stStock, stIncoming);
        }
        // Sync lookup table
        QByteArray newnamelatin = newname.toLower();
        QList < QByteArray > oldnames = fNameIndexLookupTable.keys(index); // already lowered
        foreach(QByteArray oldname, oldnames) {
            fNameIndexLookupTable.remove(oldname);
            fNameIndexLookupTable.insert(newnamelatin, index);
        }
        item.Filename[stIncoming] = newname;
        Result = true;
    }

    if (Result) {
        fXipHelper.UpdateProjectModifiedStatus(true);
    }

    return Result;
}

bool TNekoDriver::AddUserFileToProject( int areaindex, QByteArray filename, QByteArray& newcontent, FILETIME modifytime )
{
    bool Result = false;

    TBundleRec& rec = fXipHelper.GetSortedBuffer();
    TBundleRecItem item;
    item.Stage[stIncoming] = areaindex;
    item.Filename[stIncoming] = filename;
    item.RawBuffer[stIncoming] = qCompressMSLZX((uchar*)newcontent.data(), newcontent.size());
    item.StockPresent = false;
    item.IncomingPresent = true;
    item.ItemNuked[stIncoming] = false;
    item.FileInfo[stIncoming].nRealFileSize = newcontent.size();
    item.FileInfo[stIncoming].nCompFileSize = item.RawBuffer[stIncoming].size();
    item.FileInfo[stIncoming].dwFileAttributes = FILE_ATTRIBUTE_READONLY;
    if (item.FileInfo[stIncoming].nRealFileSize != item.FileInfo[stIncoming].nCompFileSize) {
        item.FileInfo[stIncoming].dwFileAttributes |= FILE_ATTRIBUTE_COMPRESSED;
    }
    item.FileInfo[stIncoming].ftTime = modifytime;
    item.ItemType[stIncoming] = btRawFile;
    item.CopyMetaData(stIncoming, stStock);
    rec.push_back(item);

    Result = true;

    if (Result) {
        fXipHelper.UpdateProjectModifiedStatus(true);
    }

    return Result;
}

bool TNekoDriver::RemoveUserFile( int index )
{
    bool Result = false;
    TBundleRec& rec = fXipHelper.GetSortedBuffer();
    if (index < 0 || index >= rec.size()) {
        return Result;
    }

    TBundleRecItem& item = rec[index]; // modify qua
    TSourceSlot sourceslot = GetActiveSlotForUser(item);
    TEntryType entrytype = BundleType2EntryType(item.ItemType[sourceslot]);
    if (entrytype == etFile && item.StockPresent == false) {
        rec.remove(index);
        Result = true;
    }

    if (Result) {
        fXipHelper.UpdateProjectModifiedStatus(true);
    }

    return Result;
}

DWORD TNekoDriver::GetROMInfo( TROMInfoIndex index )
{
    return fXipHelper.ReadROMInfo(index);
}

TMeizuOEMTag TNekoDriver::GetMeizuTag()
{
    return fXipHelper.ReadOEMTag();
}

bool TNekoDriver::SetMeizuTag( TMeizuOEMTag newtag )
{
    bool Result = fXipHelper.WriteOEMTag(newtag);
    if (Result) {
        fXipHelper.UpdateProjectModifiedStatus(true);
    }
    return Result;
}
