#ifndef _COMMON_H_
#define _COMMON_H_

enum TLogType { 
    ltHint, ltDebug, ltMessage, ltError
};

enum TFilterType {
    ftOriginal, ftModified, ftSession, ftImage, ftText
};

#endif
