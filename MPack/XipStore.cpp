#include "XipStore.h"
#include "ROMUtils.h"
#include "GemStone.h"
#include "LibMUIMaker\LibMUIMaker.h"
#include <FvQKOL.h>
#include <set>
#include <vector>
#include <map>
#include <QSet>
#include <QBuffer>
#include <QTextStream>
#include <QCoreApplication>

#include <ucl/ucl.h>
#if defined(UCL_USE_ASM)
#  include <ucl/ucl_asm.h>
#endif

using std::set;
using std::vector;
using std::map;

#define IMGOFSINCREMENT 0x1000

bool ROMHelper::LoadFlatBufferFromXip( QString XipFilename )
{
    bool Result = false;

    // Detect B00FF n RLE
    QFile xipfile(XipFilename);
    xipfile.open(QIODevice::ReadOnly);
    TRLEXIPMBR MBR;

    if (sizeof(MBR) != xipfile.read(LPSTR(&MBR), sizeof(MBR))) {
        //perror("fread");
        xipfile.close();
        return false;
    }
    DWORD filesize = xipfile.size();

    if (_strnicmp((char*)&MBR.Magic[0], "B000FF", 6) == 0) {
        // Maybe M8
        Result = true;
        logprintf("Try to converting M8 xip to nb0 format...\n");
        logprintf("RomAddress: 0x%08X\tRomSize: 0x%08X\n", MBR.RomAddress, MBR.RomSize);
        fROMBaseAddress = MBR.RomAddress;
        fROMSize = MBR.RomSize;

        fFlatBuffer.resize(MBR.RomSize);
        fFlatBuffer.fill(0);

        while (xipfile.pos() < filesize && fFlatBuffer.size() <= MBR.RomSize) {
            TChunkHeader Chunk;
            xipfile.read(LPSTR(&Chunk), sizeof(Chunk));
            if (Chunk.TargetAddress == 0) {
                // Tail
                logprintf("RomLoadAddress: 0x%08X\nPlease take these arguments for xip.bin's generation.\n", Chunk.SourceSize);
                fROMLoadAddressOverided = fROMLoadAddress = Chunk.SourceSize;
                break;
            }
            if (fFlatBuffer.size() < Chunk.TargetAddress - MBR.RomAddress + Chunk.SourceSize) {
                // will never here
                fFlatBuffer.resize(Chunk.TargetAddress - MBR.RomAddress + Chunk.SourceSize);
            }
            char *dangerpos = LPSTR(fFlatBuffer.constData());
            dangerpos += (Chunk.TargetAddress - MBR.RomAddress);
            xipfile.read(dangerpos, Chunk.SourceSize);
        }
        if (fFlatBuffer.size() < MBR.RomSize) {
            fFlatBuffer.resize(MBR.RomSize);
        }
    }
    xipfile.close();
    return Result;
    // TODO: Move to ROMUtils
}


bool ROMHelper::GenerateXipFromFlatBuffer( QString XipFilename, int Compression ) {
    bool Result = false;

    // this function translate from pascal version released by kingvera
    vector <TChunkHeader> Chunks;
    LPSTR BuffStart, BuffCurr, BuffPrev, BuffEnd;
    tagAddressBundle Address;
    int GapGates[5] = {0x40, 0x36, 0x30, 0x18, 0xC};

    BuffStart = fFlatBuffer.data();
    BuffEnd   = LPSTR(DWORD(BuffStart) + fFlatBuffer.size());
    BuffCurr  = BuffStart;
    BuffPrev  = BuffStart;
    Compression = qMax(Compression, 4);
    Compression = qMin(Compression, 0);
    int GapFilter = GapGates[Compression];
    int runpercent = 0;
    while (true) {
        // TODO: check 128K/4K Chunk
        Address.Union = SearchGap(BuffCurr, (DWORD(BuffEnd) - DWORD(BuffCurr)) / 4);
        if (Address.Stop < DWORD(BuffEnd)) {
            // Check valuable
            if (Address.Stop - Address.Start > GapFilter ) { // $36
                TChunkHeader item;
                item.TargetAddress = DWORD(BuffPrev) - DWORD(BuffStart);
                item.SourceSize = Address.Start - DWORD(BuffPrev) - 4;
                item.Checksum32 = CalcChecksum32(&BuffStart[item.TargetAddress], item.SourceSize);
                Chunks.push_back(item); // Expand
                BuffCurr = LPSTR(Address.Stop - 4);
                BuffPrev = BuffCurr;
            } else {
                BuffCurr = LPSTR(Address.Stop - 4);
                // Keep BuffPrev
            }
            int curprecent = (BuffCurr - BuffStart) * 20 / (BuffEnd - BuffStart);
            if (curprecent > runpercent) {
                runpercent = curprecent;
                emit xipProgressChanged(runpercent);
            }
        }  else {
            // End Reached
            TChunkHeader item;
            item.TargetAddress = DWORD(BuffPrev) - DWORD(BuffStart);
            item.SourceSize = Address.Start - DWORD(BuffPrev) - 4;
            item.Checksum32 = CalcChecksum32(&BuffStart[item.TargetAddress], item.SourceSize);
            Chunks.push_back(item);
            break;
        }
    }
    runpercent = 20;
    emit xipProgressChanged(runpercent);

    QFile xipfile(XipFilename);
    xipfile.open(QIODevice::WriteOnly);

    TRLEXIPMBR MBR;
    strcpy_s(&MBR.Magic[0], sizeof(MBR.Magic), "B000FF");
    MBR.Magic[6] = 0x0A;
    MBR.RomAddress = fROMBaseAddress;
    MBR.RomSize = fROMSize;
    xipfile.write((char*)&MBR, sizeof(TRLEXIPMBR));
    xipfile.seek(0x0F);
    quint32 SourcePos;
    for (int Index = 0; Index < Chunks.size(); Index++) {
        SourcePos = Chunks[Index].TargetAddress;
        Chunks[Index].TargetAddress += MBR.RomAddress;
        xipfile.write((char*)&Chunks[Index], sizeof(TChunkHeader));
        xipfile.write((char*)&BuffStart[SourcePos], Chunks[Index].SourceSize);
        int curprecent = (SourcePos + Chunks[Index].SourceSize - DWORD(BuffStart)) * 80 / (BuffEnd - BuffStart) + 20;
        if (curprecent > runpercent) {
            runpercent = curprecent;
            emit xipProgressChanged(runpercent);
        }
    }
    TChunkHeader Tail;
    Tail.TargetAddress = 0;
    Tail.SourceSize = fROMLoadAddress;
    Tail.Checksum32 = 0;
    xipfile.write((char*)&Tail, sizeof(TChunkHeader));
    runpercent = 100;
    emit xipProgressChanged(runpercent);

    //Free_And_Nil(OutputFStream);
    xipfile.close();
    //SetLength(WorkingBuf, 0);
    Chunks.clear();

    Result = true;

#ifdef _DEBUG
    QFile nb0file(XipFilename + ".nb0");
    nb0file.open(QIODevice::WriteOnly);
    nb0file.write(fFlatBuffer);
    nb0file.close();
#endif

    return Result;
}


bool ROMHelper::ParseFlatBuffer(bool update)
{
    bool Result = false;

    // TODO: Update
    if (update == false) {
        fSortedBuffer.clear();
        fROMHeaders.clear();
        fRegionRecs.clear();
        fExtRecs.clear();
        fXipChainRecs.clear();
    }

    set < DWORD > headerentrys;
    set < DWORD > knownpid;
        // future: fix iterating over memblocks, now it does not handle 'holes' in the memory range very well.
    int areaindex = 0;
    for (DWORD romofs = (fROMBaseAddress + IMGOFSINCREMENT - 1) & ~(IMGOFSINCREMENT - 1); romofs < fROMBaseAddress + fROMSize; romofs += IMGOFSINCREMENT) {
        DWORD *rom = (DWORD*)GetVAData(romofs); // we do not need work as dumprom because M8 have flat layout
        if (rom == NULL)
            continue;
        if (rom[ROM_SIGNATURE_OFFSET/sizeof(DWORD)] == ROM_SIGNATURE) {
            if (rom[0] == 0xea0003fe) {
                MarkRegion(romofs, 4, "JUMP to kernel start");
            }
            MarkRegion(DataToVA(&rom[16]), 12, "'ECEC' -> %08lx %08lx", rom[17], rom[18]);

            if (headerentrys.find(rom[17]) == headerentrys.end()) {
                // We should have 2 headers for M8
                //DumpRomHdr(areaindex++, rom[17]);
                PROMHDR header = PROMHDR(GetVAData(rom[17]));

                TAreaRec rec;
                memcpy_s(&rec.AreaMark, sizeof(rec.AreaMark), &rom[0], sizeof(rec.AreaMark));
                int paddingsum = 0;
                for (int i = 0; i < _countof(rec.AreaMark.Padding); i++) {
                    paddingsum += rec.AreaMark.Padding[i];
                }
                rec.PureSignature = paddingsum == 0;
                paddingsum = 0;
                for (int i = 0x13; i < 0x3FF; i++) {
                    paddingsum += rom[i];
                }
                rec.PureECEC = paddingsum == 0;
                Q_ASSERT(rec.PureECEC && (rec.PureSignature || areaindex > 0));
                rec.Header = *header; // @CopyRecord
                rec.MarkAddress = DataToVA(&rom[0]);
                fROMHeaders.push_back(rec);

                if (header->pExtensions) {
                    DWORD dwPidOffset = (DWORD)header->pExtensions;
                    // Mark only?
                    if (knownpid.find(dwPidOffset) == knownpid.end()) {
                        knownpid.insert(dwPidOffset);

                        ROMPID *pid = (ROMPID *)GetVAData(dwPidOffset);

                        if (pid != NULL) {
                            // first is inside nk.exe
                            pid = (ROMPID*)GetVAData(dwPidOffset = (DWORD)pid->pNextExt);
                            while (pid) {
                                MarkRegion(dwPidOffset, sizeof(ROMPID), "rom extension entry %s", pid->name);
                                MarkRegion((DWORD)pid->pdata, pid->length, "rom extension data %s", pid->name);

                                TROMExtRecItem item;
                                item.HeaderIndex = areaindex;
                                item.EntryAddress = DataToVA(pid);
                                item.ExtEntry = *pid; // @CopyRecord
                                item.ExtData = QByteArray(GetVAData(DWORD(pid->pdata)), pid->length);
                                fExtRecs.push_back(item); // Address stored in pExtsnsions

                                pid = (ROMPID*)GetVAData(dwPidOffset = (DWORD)pid->pNextExt);
                            }
                        }
                    }
                }

                DWORD i;
                TOCentry *tocm0 = (TOCentry *) & header[1];
                for (i = 0 ; i < header->nummods; i++) {
                    //DumpModuleTOCentry(header, i, g_mem.GetOfs(&tocm0[i]));

                    DWORD ofs = DWORD(DataToVA(&tocm0[i]));
                    TOCentry *mrec = (TOCentry *)GetVAData(ofs);
                    if (mrec == NULL) {
                        logprintf("invalid modtoc ofs %08lx\n", ofs);
                        return Result;
                    }

                    char *filename = (char *)GetVAData((DWORD)mrec->lpszFileName);

                    if (filename == NULL)
                        return Result;

                    MarkRegion(ofs, sizeof(TOCentry), "modent %3d %08lx %08lx%08lx %8d %08lx %s",
                        i, mrec->dwFileAttributes, mrec->ftTime.dwHighDateTime, mrec->ftTime.dwLowDateTime, mrec->nFileSize, mrec->ulLoadOffset, filename);

                    MarkRegion((DWORD)mrec->lpszFileName, strlen(filename) + 1, "modname %s", filename);

                    e32_rom *e32 = (e32_rom *)GetVAData((DWORD)mrec->ulE32Offset);
                    if (e32 == NULL)
                        return Result;
                    //MemRegion &m =  g_regions.MarkRegion((DWORD)t->ulE32Offset, sizeof(e32_rom) + (b_wm2005_rom ? 4 : 0), "e32 struct %d objs, img=%04x entrypt=%08lx base=%08lx v%d.%d tp%d %s",
                    //    e32->e32_objcnt, e32->e32_imageflags, e32->e32_entryrva, e32->e32_vbase, e32->e32_subsysmajor, e32->e32_subsysminor, e32->e32_subsys, filename);

                    o32_rom *o32 = (o32_rom *)GetVAData((DWORD)mrec->ulO32Offset);
                    if (o32 == NULL)
                        return Result;
                    //if (g_verbose) {
                    //    *m.description += dworddumpasstring(t->ulE32Offset, t->ulE32Offset + sizeof(e32_rom) + (b_wm2005_rom ? 4 : 0));
                    //}

                    //m = g_regions.MarkRegion((DWORD)t->ulO32Offset, e32->e32_objcnt * sizeof(o32_rom), "o32 struct %s", filename);
                    for (int i = 0 ; i < e32->e32_objcnt ; ++i) {
                        //m = g_regions.MarkRegion(o32[i].o32_dataptr ? o32[i].o32_dataptr : ofs, min(o32[i].o32_vsize, o32[i].o32_psize),
                        //    "o32 region_%d rva=%08lx vsize=%08lx real=%08lx psize=%08lx f=%08lx for %s", i, o32[i].o32_rva, o32[i].o32_vsize, o32[i].o32_realaddr, o32[i].o32_psize, o32[i].o32_flags, filename);

                        //Q_ASSERT((o32[i].o32_flags & IMAGE_SCN_COMPRESSED) == 0);
                        //        if (g_outputdirectory)
                        //            UncompressAndWrite(m.start, m.end, filename, i, o32[i].o32_flags&IMAGE_SCN_COMPRESSED, o32[i].o32_vsize, o32[i].o32_realaddr);
                    }

                    TBundleRecItem item;
                    item.StockPresent = true;
                    item.Filename[stStock] = filename;
                    item.Stage[stStock] = areaindex;
                    item.ItemType[stStock] = btPEModule;
                    item.E32Info[stStock] = *e32;
                    item.O32Info[stStock].resize(e32->e32_objcnt);
                    for (int i = 0; i < e32->e32_objcnt; i++) {
                        item.O32Info[stStock][i] = o32[i];
                    }
                    item.TocInfo[stStock] = *mrec;
                    item.EntryAddress[stStock] = DataToVA(mrec);
                    //if (g_outputdirectory)
                    //    CreateOriginalFile(rom, t, filename, e32, o32);
                    QBuffer buffer(&item.RawBuffer[stStock]);
                    buffer.open(QBuffer::ReadWrite);
                    CreateOriginalFile(&buffer, item.Filename[stStock], header, mrec, e32, o32);
                    
                    // Module must be PE. Detect PureResource directly
                    TPEDirector director(item.RawBuffer[stStock].data(), item.RawBuffer[stStock].size());
                    if (director.IsNull() == false) {
                        IMAGE_NT_HEADERS ntheaders = director.GetNTHeaders();
                        if (ntheaders.OptionalHeader.AddressOfEntryPoint == 0 && ntheaders.OptionalHeader.NumberOfRvaAndSizes > IMAGE_DIRECTORY_ENTRY_RESOURCE) {
                            item.PureResource[stStock] = ntheaders.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_RESOURCE].VirtualAddress > 0;
                        }
                    }
                    fSortedBuffer.push_back(item);

                    //if (bQuit) return;
                }
                FILESentry *filesm0 = (FILESentry *) & tocm0[header->nummods];
                for (i = 0 ; i < header->numfiles; i++) {
                    //DumpFileTOCentry(i, g_mem.GetOfs(&filesm0[i]));
                    //if (bQuit) return;

                    DWORD ofs = DWORD(DataToVA(&filesm0[i]));

                    FILESentry *frec = (FILESentry *)GetVAData(ofs);
                    if (frec == NULL) {
                        logprintf("invalid filetoc ofs %08lx\n", ofs);
                        return Result;
                    }

                    char *filename = (char *)GetVAData((DWORD)frec->lpszFileName);
                    if (filename == NULL)
                        return Result;

                    //g_regions.MarkRegion(ofs, sizeof(FILESentry), "filent %3d %08lx %08lx%08lx %8d %8d %08lx %s",
                    //    filenr, t->dwFileAttributes, t->ftTime.dwHighDateTime, t->ftTime.dwLowDateTime, t->nRealFileSize, t->nCompFileSize, t->ulLoadOffset, filename);
                    //g_regions.MarkRegion((DWORD)t->lpszFileName, strlen(filename) + 1, "filename %s", filename);
                    //MemRegion &m = g_regions.MarkRegion((DWORD)t->ulLoadOffset, t->nCompFileSize, "filedata %s", filename);


                    //if (g_outputdirectory)
                    //    UncompressAndWrite(m.start, m.end, filename, -1, t->nCompFileSize!=t->nRealFileSize, t->nRealFileSize, t->ulLoadOffset);

                    TBundleRecItem item;
                    item.StockPresent = true;
                    item.Filename[stStock] = filename;
                    item.Stage[stStock] = areaindex;
                    item.ItemType[stStock] = btRawFile;
                    item.FileInfo[stStock] = *frec;
                    item.EntryAddress[stStock] = DataToVA(frec);
                    item.RawBuffer[stStock].resize(frec->nCompFileSize);
                    memcpy_s(item.RawBuffer[stStock].data(), item.RawBuffer[stStock].size(), GetVAData(frec->ulLoadOffset), frec->nCompFileSize);
                    fSortedBuffer.push_back(item);
                }

                if (header->ulCopyEntries) {
                    COPYentry *crec = (COPYentry *)GetVAData(header->ulCopyOffset);
                    if (crec == NULL) {
                        return Result;
                    }
                    //MemRegion &m = g_regions.MarkRegion(r->ulCopyOffset, sizeof(COPYentry) * r->ulCopyEntries, "rom_%02d copy to ram: ", areaindex);
                    for (DWORD i = 0 ; i < header->ulCopyEntries ; ++i) {
                        char buf[64];
                        _snprintf_s(buf, _countof(buf), 64, " %08lxL%06lx -> %08lxL%06lx", crec->ulSource, crec->ulCopyLen, crec->ulDest, crec->ulDestLen);
                        //*m.description += buf;
                        TBundleRecItem item;
                        item.StockPresent = true;
                        item.Filename[stStock] = QString("code %1").arg(crec->ulDest, 8, 16).toAscii();
                        item.Desription[stStock] = QString(buf);
                        item.Stage[stStock] = areaindex;
                        item.ItemType[stStock] = btRawCode;
                        item.RawBuffer[stStock].resize(crec->ulCopyLen);
                        memcpy_s(item.RawBuffer[stStock].data(), item.RawBuffer[stStock].size(), GetVAData(crec->ulSource), crec->ulCopyLen);
                        item.CopyInfo[stStock] = *crec;
                        item.EntryAddress[stStock] = DataToVA(crec);
                        fSortedBuffer.push_back(item);
                        
                        crec++;
                    }
                }
                //if (bQuit) return;

                headerentrys.insert(rom[17]);    // keep track of multiple pointers to same header.

                areaindex++;
            }
        }
    }
    Result = headerentrys.size() > 0; // May true

    // XipEntry?
    //fROMXipChainAddress = fROMBaseAddress + 0x07F00000;
    // we consider unaligned bytes inside rom maybe xipchain
    if (fROMSize % 0x1000 != 0) {
        fROMXipChainAddress = fROMBaseAddress + fROMSize - (fROMSize % 0x1000);
    } else {
        if (fROMSize == 0x7F00528UL) {
            fROMXipChainAddress = fROMBaseAddress + 0x07F00000;
        } else if (fROMSize = 0x3F00264UL) {
            fROMXipChainAddress = fROMBaseAddress + 0x03F00000;
        }
    }
    XIPCHAIN_INFO *xipchain = (XIPCHAIN_INFO *)GetVAData(fROMXipChainAddress);
    Q_ASSERT(xipchain != NULL);

    if (xipchain->cXIPs > MAX_ROM) {
        logprintf("ERROR - invalid xipchain\n");
        //return false;
        fNoChainMode = true;
    } else {
        MarkRegion(fROMXipChainAddress, sizeof(DWORD), "xipchain head");

        XIPCHAIN_ENTRY *xip = &xipchain->xipEntryStart;

        for (DWORD i = 0 ; i < xipchain->cXIPs ; ++i) {
            //DumpXIPChainEntry(i, &xip[i]);
            fXipChainRecs.push_back(xip[i]);
        }
        Result = Result && fXipChainRecs.size() > 0;

        Q_ASSERT(fXipChainRecs.size() == fROMHeaders.size());
        fNoChainMode = false;
    }

    return Result;
}

void ROMHelper::CreateOriginalFile(QIODevice* f, QString filename, const ROMHDR *rom, const TOCentry *t, const e32_rom *e32, const o32_rom *o32)
{
    if (f==NULL)
    {
        //perror(fn);
        return;
    }

    f->open(QIODevice::ReadWrite);

    WriteDummyMZHeader(f);

    DWORD dwE32Ofs= f->pos();
    WriteE32Header(f, rom, e32, t, o32);

    vector<DWORD> o32ofslist;       // list of file offsets to o32_obj structs
    vector<DWORD> dataofslist;      // list of file offsets to start of data objects
    vector<DWORD> datalenlist;      
    typedef map<DWORD, std::pair<DWORD,DWORD> > RvaRvaSizeMap;
    RvaRvaSizeMap rvamap;           // map of rva -> adjusted rva

    memset(g_segmentNameUsage, 0, sizeof(g_segmentNameUsage));
    int i;
    for (i=0 ; i<e32->e32_objcnt ; i++)
    {
        o32ofslist.push_back(f->pos());
        DWORD rva= WriteO32Header(f, e32, &o32[i]);

        if (rva != o32[i].o32_rva) {
            logprintf("NOTE: section at %08lx iso %08lx for %s\n", rva, o32[i].o32_rva, filename.isEmpty()?"QIODevice":filename.toAscii().data());
        }
        rvamap[o32[i].o32_rva]= std::pair<DWORD,DWORD>(rva, o32[i].o32_vsize);
    }

    WriteAlignment(f, 0x200);

    DWORD dwHeaderSize= f->pos();

    for (i = 0; i < e32->e32_objcnt; i++)
    {
        dataofslist.push_back(f->pos());
        // TODO: WriteUncompressedData broken psize
        DWORD datalen= WriteUncompressedData(f, o32[i].o32_dataptr, qMin(o32[i].o32_vsize, o32[i].o32_psize), o32[i].o32_flags & IMAGE_SCN_COMPRESSED, o32[i].o32_vsize);
        if (o32[i].o32_flags & IMAGE_SCN_COMPRESSED) {
            datalenlist.push_back(datalen);
        } else {
            datalenlist.push_back(o32[i].o32_psize);
        }

        WriteAlignment(f, 0x200);
    }
    DWORD dwTotalFileSize= f->pos();

    // fix rawdatalen + dataoffsets in segment list
    for (i=0 ; i<e32->e32_objcnt ; i++)
    {
        f->seek(o32ofslist[i]+16); //SEEK_SET
        f->write((char*)&datalenlist[i], sizeof(DWORD));   // ofs to o32_psize
        f->write((char*)&dataofslist[i], sizeof(DWORD));   // ofs to o32_dataptr

        // update imp_address's in IMP section
        if (true && isObjectContainsSection(o32[i], e32->e32_unit[IMP])) {
            f->seek(dataofslist[i]+ e32->e32_unit[IMP].rva -o32[i].o32_rva+0x10); // SEEK_SET
            while (1) {
                DWORD imp_addr;
                f->read((char*)&imp_addr, sizeof(DWORD));  // read ImpHdr.imp_address
                if (imp_addr==0)
                    break;

                // this finds the next rva 
                RvaRvaSizeMap::iterator s= rvamap.upper_bound(imp_addr);
                if (s!=rvamap.end() && s!=rvamap.begin()) {
                    s--;
                    if (imp_addr < (*s).first+(*s).second.second) {
 //                     printf("moving imp %08lx from rva[%08lx-%08lx] -> %08lx-%08lx  : %08lx\n",
 //                             imp_addr, (*s).first, (*s).first+(*s).second.second,
 //                             (*s).second.first, (*s).second.first+(*s).second.second,
 //                             imp_addr-(*s).first +(*s).second.first);
                        imp_addr -= (*s).first -(*s).second.first;
                        f->seek(f->pos() - 4); // SEEK_CUR
                        f->write((char*)&imp_addr, sizeof(DWORD));
                        f->seek(f->pos() + 0x10); // SEEK_CUR
                    }
                    else {
                        logprintf("!!! %08lx after %08lx but not before %08lx\n", imp_addr, (*s).first, (*s).first+(*s).second.second);
                    }
                }
            }
        }
    }

    f->seek(dwE32Ofs + 0x54);// SEEK_SET  // ofs to e32_hdrsize
    f->write((char*)&dwHeaderSize, sizeof(DWORD));

    f->seek(dwTotalFileSize); // SEEK_SET
    f->close();

    //todo: set fileattributes + datetime.
}


DWORD ROMHelper::WriteUncompressedData(QIODevice *f, DWORD dataptr, DWORD datasize, BOOL bCompressed, DWORD maxUncompressedSize)
{
    BYTE *buf= (BYTE*)GetVAData(dataptr);
    if (buf==NULL)
        return 0;
    DWORD buflen= datasize;
    if (bCompressed) {
        BYTE *dcbuf= new BYTE[maxUncompressedSize+4096];
        buflen= cedecompress(buf, datasize, dcbuf, maxUncompressedSize, 0, 1, 4096);

        if (buflen!=CEDECOMPRESS_FAILED)
        {
            buf= dcbuf;
        }
        else {
            logprintf("error decompressing %08lxL%08lx\n", dataptr, datasize);
            buflen= datasize;
            bCompressed= false;
            delete dcbuf;
        }
    }

    size_t nWritten= f->write((char*)buf, buflen);
    if (nWritten!=buflen)
    {
        //perror("fwrite");
        logprintf("error writing uncompressed data\n");
    }

    if (bCompressed)
        delete buf;

    return nWritten;
}


bool ROMHelper::SaveParsedProjectToFile( QString mpkfilename, bool compress )
{
    bool Result = false;

    QFile file(mpkfilename);
    file.open(QFile::WriteOnly);
    // TODO: UCL/BZ2 Compression
    if (compress) {
        QByteArray uclbuf;
        QBuffer uclfile(&uclbuf);
        uclfile.open(QIODevice::WriteOnly);
        QDataStream writer(&uclfile);
        writer << fROMBaseAddress << fROMSize << fROMLoadAddress << fROMLoadAddressOverided;
        writer << fROMXipChainAddress;
        writer << fROMHeaders;
        writer << fExtRecs;
        writer << fXipChainRecs << fReadOnlyMode << fNoChainMode;
        writer << fSortedBuffer;

        int r;
        ucl_bytep in;
        ucl_bytep out;
        ucl_uint in_len;
        ucl_uint out_len;
        ucl_uint new_len;
        int level = 5;                  /* compression level (1-10) */

        if (ucl_init() != UCL_E_OK) {
            logprintf("internal error - ucl_init() failed !!!\n");
            logprintf("(this usually indicates a compiler bug - try recompiling\nwithout optimizations, and enable `-DUCL_DEBUG' for diagnostics)\n");
            return Result;
        }

        in_len = uclbuf.size();

        out_len = in_len + in_len / 8 + 256;

        in = (ucl_bytep)uclbuf.data(); //(ucl_bytep) ucl_malloc(in_len);
        out = (ucl_bytep) ucl_malloc(out_len);
        if (in == NULL || out == NULL) {
            logprintf("out of memory\n");
            return Result;
        }

        //ucl_memset(in, 0, in_len);

        r = ucl_nrv2b_99_compress(in, in_len, out, &out_len, NULL, level, NULL, NULL);
        if (r == UCL_E_OUT_OF_MEMORY) {
            logprintf("out of memory in compress\n");
            return Result;
        }
        if (r == UCL_E_OK) {
            logprintf("compressed %lu bytes into %lu bytes\n",
            (unsigned long) in_len, (unsigned long) out_len);
        } else {
            /* this should NEVER happen */
            logprintf("internal error - compression failed: %d\n", r);
            return Result;
        }
        /* check for an incompressible block */
        if (out_len >= in_len) {
            logprintf("This block contains incompressible data.\n");
            return Result;
        }

        uclbuf.resize(out_len);
        memcpy_s(uclbuf.data(), uclbuf.size(), out, out_len);

        ucl_free(out);
        //ucl_free(in);
        logprintf("\n");
#if defined(UCL_USE_ASM)
        logprintf("i386 assembler version is enabled.\n");
#endif
        //logprintf("Simple compression test passed.\n");
        //return Result;

        QDataStream blockwriter(&file);
        DWORD magic = 'MPU1';
        blockwriter << magic;
        blockwriter << in_len;
        blockwriter << uclbuf;
        file.close();
        Result = true;
    } else {
        QDataStream writer(&file);
        DWORD magic = 'MP02';
        writer << magic;
        writer << fROMBaseAddress << fROMSize << fROMLoadAddress << fROMLoadAddressOverided;
        writer << fROMXipChainAddress;
        writer << fROMHeaders;
        writer << fExtRecs;
        writer << fXipChainRecs << fReadOnlyMode << fNoChainMode;
        writer << fSortedBuffer;
        file.close();
        Result = true;
    }

    return Result;
}

bool ROMHelper::LoadProjectFromFile( QString mpkfilename )
{
    bool Result = false;

    QFile file(mpkfilename);
    file.open(QFile::ReadOnly);
    // TODO: MIME check
    QDataStream reader(&file);

    DWORD magic;

    reader >> magic;

    if (magic == 'MP02' || magic == 'MPU1' || magic == 'MPB1') {
        if (magic == 'MP02') {
            reader >> fROMBaseAddress >> fROMSize >> fROMLoadAddress >> fROMLoadAddressOverided;
            reader >> fROMXipChainAddress;
            reader >> fROMHeaders;
            reader >> fExtRecs;
            reader >> fXipChainRecs >> fReadOnlyMode >> fNoChainMode;
            reader >> fSortedBuffer;
        } else if (magic == 'MPU1') {
            int r;
            ucl_bytep in;
            ucl_bytep out;
            ucl_uint in_len;
            ucl_uint out_len;
            ucl_uint new_len;

            QByteArray uclbuf;
            reader >> out_len;
            reader >> uclbuf;
            in = (ucl_bytep)uclbuf.data();
            in_len = uclbuf.size();
            out = (ucl_bytep)ucl_malloc(out_len);

            new_len = out_len;
#if defined(UCL_USE_ASM)
            r = ucl_nrv2b_decompress_asm_8(in, in_len, out, &out_len, NULL);
#else
            r = ucl_nrv2b_decompress_8(in, in_len, out, &out_len, NULL);
#endif
            if (r == UCL_E_OK && new_len == out_len)
                logprintf("decompressed %lu bytes back into %lu bytes\n",
                (unsigned long) in_len, (unsigned long) out_len);
            else
            {
                /* this should NEVER happen */
                logprintf("internal error - decompression failed: %d\n", r);
                return Result;
            }
            uclbuf.resize(out_len);
            memcpy_s(uclbuf.data(), uclbuf.size(), out, out_len);
            QBuffer uclfile(&uclbuf);
            uclfile.open(QFile::ReadOnly);
            reader.setDevice(&uclfile);

            reader >> fROMBaseAddress >> fROMSize >> fROMLoadAddress >> fROMLoadAddressOverided;
            reader >> fROMXipChainAddress;
            reader >> fROMHeaders;
            reader >> fExtRecs;
            reader >> fXipChainRecs >> fReadOnlyMode >> fNoChainMode;
            reader >> fSortedBuffer;

            ucl_free(out);
        }
    } else {
        fROMBaseAddress = magic; // legacy raw mpkfile
        reader >> fROMSize >> fROMLoadAddress >> fROMLoadAddressOverided;
        reader >> fROMXipChainAddress;
        reader >> fROMHeaders;
        reader >> fExtRecs;
        reader >> fXipChainRecs;
        reader >> fSortedBuffer;
        fReadOnlyMode = false;
        fNoChainMode = false;
    }

    file.close();
    Result = true;

    if (fROMLoadAddressOverided == 0 || fROMLoadAddressOverided == 0xCDCDCDCDUL) {
        fROMLoadAddressOverided = fROMLoadAddress;
    }

    return Result;
}

bool ROMHelper::PrepareFlatBufferFromProjectGroovy()
{
    bool Result = false;

    // we sort bundle rec and though them back to buffer
    fFlatBuffer.resize(fROMSize);
    fFlatBuffer.fill(0);

    return Result;
}

// GCC can't use local template
typedef struct tagCollectorRecItem {
    int AreaIndex;
    int ChainIndex;
    DWORD StorageEnd;
    int AreaSpace;
    int UsedSpace;
    int FreeSpace;
    int ModCost;
    int DeltaCount;
} TCollectorRecItem, *PCollectorRecItem;

bool ROMHelper::PrepareFlatBufferFromProjectSemiStock()
{
    bool Result = false;

    // we merge back bundle rec back to ROM like stock
    fFlatBuffer.resize(fROMSize);
    fFlatBuffer.fill(0);

    fStepMoveRecs.clear();
    fAreaMoveRecs.clear(); // for next round

    QVector < TAreaRec > WorkROMHeaders = fROMHeaders; // copy one. maybe used shrink second for suite first?
    QVector < XIPCHAIN_ENTRY > WorkXipChainRecs = fXipChainRecs;
    if (fNoChainMode == false) {
        Q_ASSERT(WorkROMHeaders.size() == WorkXipChainRecs.size());
    }
    QMap < int, int > ChainLookup;

    // Resize check
    QMap < int, QMap < int, QByteArray > > EasySections; // budleindex, secindex, content
    int bundleindex = 0;
    int runpercent = 0;
    emit prepareBufferStageChanged(tr("Try realloc space..."));
    for (TBundleRec::iterator recit = fSortedBuffer.begin(); recit != fSortedBuffer.end(); recit++) {
        // dangerous
        TSourceSlot sourceslot = GetActiveSlotForUser(*recit);
        int curpercent = bundleindex * 10 / fSortedBuffer.size();
        if (curpercent > runpercent) {
            runpercent = curpercent;
            emit prepareBufferProgressChanged(runpercent);
        }
        // use non save slot as temporary work target
        // use RawBuffer[sourceslot] to get untouched raw
        (*recit).CopyMetaData(sourceslot, stWork);
        if ((sourceslot == stStock && (*recit).ItemNuked[stWork] == false) || // stock , non nuke
            (sourceslot != stStock && (*recit).ItemNuked[stWork] && (*recit).StockPresent == false) // added file, nuked
        ) {
            bundleindex++;
            continue; // dangerous
        }
        // we also nuke stStock if we go here.
        TEntryType stocktype = BundleType2EntryType((*recit).ItemType[stWork]);
        if (stocktype == etModule) {
            if ((*recit).ItemNuked[stWork]) {
                // module count, tocentry e32 header o32 array sections
                WorkROMHeaders[(*recit).Stage[stWork]].Header.nummods--; // verified
                int delta = - sizeof((*recit).TocInfo[stStock]); // We assume TocEntry Store without gap.
                RegisterMoveDelta((*recit).EntryAddress[stStock] + sizeof((*recit).TocInfo[stStock]), sizeof((*recit).TocInfo[stStock]), delta); // nuke entry
                delta = AlignAddressByBlocksizeCrazy((*recit).TocInfo[stStock].ulE32Offset, sizeof((*recit).E32Info[stStock]), 0, 4); // E32 is single
                RegisterMoveDelta((*recit).TocInfo[stStock].ulE32Offset + sizeof((*recit).E32Info[stStock]), sizeof((*recit).E32Info[stStock]), delta);
                delta = AlignAddressByBlocksizeCrazy(DWORD((*recit).TocInfo[stStock].lpszFileName), (*recit).Filename[stStock].size() + 1, 0, 4); // "nk.exe", 0
                RegisterMoveDelta(DWORD((*recit).TocInfo[stStock].lpszFileName) + (*recit).Filename[stStock].size() + 1, (*recit).Filename[stStock].size(), delta);

                DWORD dest = (*recit).TocInfo[stStock].ulO32Offset;
                int arraysize = sizeof(o32_rom) * (*recit).E32Info[stStock].e32_objcnt;
                delta = AlignAddressByBlocksizeCrazy(dest, arraysize, 0, 4);
                RegisterMoveDelta(dest + arraysize, arraysize, delta);
                for (int i = 0; i < (*recit).E32Info[stStock].e32_objcnt; i++) {
                    o32_rom& stocko32 = (*recit).O32Info[stStock][i];
                    //delta = AlignByBlocksize( - sizeof(stocko32), 1);
                    //RegisterMoveDelta(dest + sizeof(stocko32), sizeof(stocko32), delta);
                    uint storesize = qMin(stocko32.o32_psize, stocko32.o32_vsize);
                    delta = AlignAddressByBlocksizeCrazy(stocko32.o32_dataptr, storesize, 0, 4);
                    RegisterMoveDelta(stocko32.o32_dataptr + storesize, storesize, delta);
                    dest += sizeof(stocko32);
                }
            } else if ((*recit).StockPresent == false) {
                // TODO: layout incoming modules using GemStone
            } else {
                // time to director. nk.exe safe now
                TPEDirector director((*recit).RawBuffer[sourceslot].data(), (*recit).RawBuffer[sourceslot].size());
                Q_ASSERT(director.IsNull() == false);
                IMAGE_NT_HEADERS ntheaders = director.GetNTHeaders();
                bool shrinked = false;
                if ((*recit).E32Info[stWork].e32_vbase != ntheaders.OptionalHeader.ImageBase) {
                    director.HardRebaseModule((*recit).E32Info[stWork].e32_vbase);
                }
                if ((*recit).E32Info[stWork].e32_vbase != ntheaders.OptionalHeader.ImageBase ||
                    AlignByBlocksize((*recit).E32Info[stWork].e32_vsize, 0x1000) < ntheaders.OptionalHeader.SizeOfImage)
                {
                    // drop section
                    BOOL Dropable;
                    int relocsecindex = director.GetPureRelocSectionIndex(Dropable);
                    if (Dropable == TRUE && relocsecindex != -1) {
                        shrinked = director.ForceRemoveSection(relocsecindex);
                    }
                }
                if (ntheaders.OptionalHeader.AddressOfEntryPoint == 0) {
                    // Maybe resource sections
                    if ((*recit).Filename[stWork].toLower().endsWith("res.dll") || (*recit).Filename[stWork].toLower().endsWith(".mui")) {
                        int resindex = director.GetPureResourceSectionIndex();
                        if (resindex != -1) {
                            for (int i = ntheaders.FileHeader.NumberOfSymbols - 1; i >= 0; i--) {
                                if (i == resindex) {
                                    continue;
                                }
                                if (director.ForceRemoveSection(i)) {
                                    shrinked = true;
                                }
                            }
                        }
                    } else {
                        logprintf("%s with no entry found.", QString((*recit).Filename[stWork]).toAscii().data());
                    }
                }

                if (shrinked) {
                    // take RawBuffer back
                    TByteArray newcontent = director.GetJackedBuf();
                    (*recit).RawBuffer[sourceslot].resize(newcontent.GetSize());
                    memmove_s((*recit).RawBuffer[sourceslot].data(), (*recit).RawBuffer[sourceslot].size(), newcontent[0], newcontent.GetSize());
                    ntheaders = director.GetNTHeaders();
                }
                Q_ASSERT((*recit).E32Info[stWork].e32_vbase == ntheaders.OptionalHeader.ImageBase);
                Q_ASSERT(ntheaders.FileHeader.NumberOfSections == (*recit).E32Info[stWork].e32_objcnt);
                Q_ASSERT((*recit).E32Info[stWork].e32_objcnt == (*recit).O32Info[stWork].size());
                Q_ASSERT(AlignByBlocksize((*recit).E32Info[stWork].e32_vsize, 0x1000) >= ntheaders.OptionalHeader.SizeOfImage);
                // TODO: OEP, Directory
                IMAGE_SECTION_HEADER* sections = director.GetSectionHeaders();
                for (int i = 0; i < (*recit).E32Info[stWork].e32_objcnt; i++) {
                    // store each section?
                    LPBYTE src = LPBYTE((*recit).RawBuffer[sourceslot].data()) + sections[i].PointerToRawData;
                    // take care to set o32_flag when added in?
                    if ((*recit).O32Info[stWork][i].o32_flags & IMAGE_SCN_COMPRESSED) {
                        int filedatesize = qMin(sections[i].SizeOfRawData, sections[i].Misc.VirtualSize);
                        QByteArray packedsec = qCompressMSLZX(src, filedatesize);
                        // o32_psize, LZXed size
                        // stock data size must be equal to o32_psize.
                        int stockdatasize = qMin((*recit).O32Info[stStock][i].o32_psize, (*recit).O32Info[stStock][i].o32_vsize);
                        int delta = AlignByBlocksize(packedsec.size() - stockdatasize, 4);
                        RegisterMoveDelta((*recit).O32Info[stStock][i].o32_dataptr + stockdatasize, stockdatasize, delta);
                        if (sourceslot == stStock) {
                            Q_ASSERT((*recit).O32Info[stWork][i].o32_psize == packedsec.size()); // TODO: enable size change?
                        } else {
                            (*recit).O32Info[stWork][i].o32_psize = packedsec.size();
                            (*recit).O32Info[stWork][i].o32_vsize = sections[i].Misc.VirtualSize;
                        }
                        EasySections[bundleindex][i] = packedsec;
                    } else {
                        // no compression
                        // we will never store it locally.
                        int filedatesize = qMin(sections[i].SizeOfRawData, sections[i].Misc.VirtualSize);
                        int stockdatasize = qMin((*recit).O32Info[stStock][i].o32_psize, (*recit).O32Info[stStock][i].o32_vsize);
                        int delta = AlignByBlocksize(filedatesize - stockdatasize, 4);
                        RegisterMoveDelta((*recit).O32Info[stStock][i].o32_dataptr + stockdatasize, filedatesize, delta);
                        if (sourceslot == stStock) {
                            Q_ASSERT((*recit).O32Info[stWork][i].o32_psize == sections[i].SizeOfRawData);
                        } else {
                            (*recit).O32Info[stWork][i].o32_psize = sections[i].SizeOfRawData;
                            (*recit).O32Info[stWork][i].o32_vsize = sections[i].Misc.VirtualSize;
                        }
                    }
                }
                if (sourceslot != stStock) {
                    // Dangerous
                    (*recit).E32Info[stWork].e32_vsize = ntheaders.OptionalHeader.SizeOfImage;
                    (*recit).E32Info[stWork].e32_entryrva = ntheaders.OptionalHeader.AddressOfEntryPoint;

                    int i;
                    for (i = 0; i < ntheaders.OptionalHeader.NumberOfRvaAndSizes; i++) {
                        if (i < ROM_EXTRA) {
                            // no IAT, no delay
                            (*recit).E32Info[stWork].e32_unit[i].rva = ntheaders.OptionalHeader.DataDirectory[i].VirtualAddress;
                            (*recit).E32Info[stWork].e32_unit[i].size = ntheaders.OptionalHeader.DataDirectory[i].Size;
                        } else if (i == RS4) {
                            (*recit).E32Info[stWork].e32_sect14rva = ntheaders.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_COM_DESCRIPTOR].VirtualAddress;
                            (*recit).E32Info[stWork].e32_sect14size = ntheaders.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_COM_DESCRIPTOR].Size;
                        }
                    }
                    for (; i <= RS4; i++) {
                        if (i < ROM_EXTRA) {
                            (*recit).E32Info[stWork].e32_unit[i].rva = 0;
                            (*recit).E32Info[stWork].e32_unit[i].size = 0;
                        } else if (i == RS4) {
                            (*recit).E32Info[stWork].e32_sect14rva = 0;
                            (*recit).E32Info[stWork].e32_sect14size = 0;
                        } 
                    }
                    if ((*recit).E32Info[stWork].e32_unit[RES].rva + (*recit).E32Info[stWork].e32_unit[RES].size > 0) {
                        // TODO: more effective resource fix
                        for (int i = 0; i < (*recit).E32Info[stWork].e32_objcnt; i++) {
                            int offset = (*recit).E32Info[stWork].e32_unit[RES].rva - (*recit).O32Info[stWork][i].o32_rva;
                            if (offset >= 0 && offset <  + (*recit).O32Info[stWork][i].o32_vsize) {
                                if ((*recit).E32Info[stWork].e32_unit[RES].size > (*recit).O32Info[stWork][i].o32_vsize - offset) {
                                    logprintf("Fix overlapped resource directory size for module %d (%s).", bundleindex, (*recit).Filename[stWork]); // operator const char*
                                    (*recit).E32Info[stWork].e32_unit[RES].size = (*recit).O32Info[stWork][i].o32_vsize - offset;
                                }
                            }
                        }
                    }
                }
                // TODO: RawBuffer Link for rename
                int delta = AlignByBlocksize((*recit).Filename[stWork].size() - (*recit).Filename[stStock].size(), 4); // rename check
                RegisterMoveDelta(DWORD((*recit).TocInfo[stStock].lpszFileName) + (*recit).Filename[stStock].size() + 1, (*recit).Filename[stStock].size(), delta);
            }
        } else if (stocktype == etFile) {
            if ((*recit).ItemNuked[stWork]) {
                if ((*recit).StockPresent) {
                    WorkROMHeaders[(*recit).Stage[stWork]].Header.numfiles--;
                    int delta = - sizeof((*recit).FileInfo[stStock]); // FilesEntry is Array too
                    RegisterMoveDelta((*recit).EntryAddress[stStock] + sizeof((*recit).FileInfo[stStock]), sizeof((*recit).FileInfo[stStock]), delta); // nuke entry
                    delta = AlignAddressByBlocksizeCrazy((*recit).FileInfo[stStock].ulLoadOffset, (*recit).FileInfo[stStock].nCompFileSize, 0, 4);
                    RegisterMoveDelta((*recit).FileInfo[stStock].ulLoadOffset + (*recit).FileInfo[stStock].nCompFileSize, (*recit).FileInfo[stStock].nCompFileSize, delta);
                    delta = AlignAddressByBlocksizeCrazy(DWORD((*recit).FileInfo[stStock].lpszFileName), (*recit).Filename[stStock].size() + 1, 0, 4);
                    RegisterMoveDelta(DWORD((*recit).FileInfo[stStock].lpszFileName) + (*recit).Filename[stStock].size() + 1, (*recit).Filename[stStock].size(), delta);
                } else {
                    // treat as non exist
                }
            } else if ((*recit).StockPresent == false) {
                // not nuked n inserted (user file)
                // TODO: insert filename / filedata at end of physics store
                // we use workaround (begin + 1) for space reserve
                bool addconfirmed = false;
                // explore in original chain?
                for (QVector < XIPCHAIN_ENTRY >::iterator entryit = WorkXipChainRecs.begin(); entryit != WorkXipChainRecs.end(); entryit++) {
                    // usOrder in 1, 2
                    if ((*entryit).usOrder == (*recit).Stage[stWork] + 1) {
                        (*entryit).dwLength += AlignByBlocksize((*recit).Filename[stWork].size() + 1, 4); // same safe as crazy
                        (*entryit).dwLength += AlignByBlocksize((*recit).RawBuffer[sourceslot].size(), 4);
                        addconfirmed = true;
                    }
                }
                //(*recit).EntryAddress[stWork] = 0x80100000; // iLastFileEntry + size of FileEntry
                DWORD ilastfileentry = fROMHeaders[(*recit).Stage[stWork]].AreaMark.HeaderAddress + sizeof(ROMHDR)
                    + sizeof(TOCentry) * fROMHeaders[(*recit).Stage[stWork]].Header.nummods
                    + sizeof(FILESentry) * WorkROMHeaders[(*recit).Stage[stWork]].Header.numfiles;
                (*recit).EntryAddress[stWork] = ilastfileentry - 1; // workaround
                RegisterMoveDelta(ilastfileentry - 1, 1, 1); // move back
                RegisterMoveDelta(ilastfileentry, sizeof(FILESentry) - 1, sizeof(FILESentry) - 1); // single run
                WorkROMHeaders[(*recit).Stage[stWork]].Header.numfiles++;
                (*recit).FileInfo[stWork].lpszFileName = (LPSTR)WorkROMHeaders[(*recit).Stage[stWork]].Header.physlast;
                WorkROMHeaders[(*recit).Stage[stWork]].Header.physlast += AlignByBlocksize((*recit).Filename[stWork].size() + 1, 4);
                (*recit).FileInfo[stWork].ulLoadOffset = WorkROMHeaders[(*recit).Stage[stWork]].Header.physlast;
                WorkROMHeaders[(*recit).Stage[stWork]].Header.physlast += AlignByBlocksize((*recit).RawBuffer[sourceslot].size(), 4);

                Q_ASSERT(addconfirmed);
            } else {
                int delta = AlignAddressByBlocksizeCrazy((*recit).FileInfo[stStock].ulLoadOffset, (*recit).FileInfo[stStock].nCompFileSize, (*recit).RawBuffer[sourceslot].size(), 4);
                RegisterMoveDelta((*recit).FileInfo[stStock].ulLoadOffset + (*recit).FileInfo[stStock].nCompFileSize, (*recit).RawBuffer[sourceslot].size(), delta);
                Q_ASSERT((*recit).FileInfo[stWork].nCompFileSize == (*recit).RawBuffer[sourceslot].size());
                delta = AlignAddressByBlocksizeCrazy(DWORD((*recit).FileInfo[stStock].lpszFileName), (*recit).Filename[stStock].size() + 1, (*recit).Filename[stWork].size() + 1, 4);
                RegisterMoveDelta(DWORD((*recit).FileInfo[stStock].lpszFileName) + (*recit).Filename[stStock].size() + 1, (*recit).Filename[stStock].size() + 1, delta);
            }
        } else if (stocktype == etCopy) {
            if ((*recit).ItemNuked[stWork]) {
                WorkROMHeaders[(*recit).Stage[stWork]].Header.ulCopyEntries--; // arraycount
                int delta = - sizeof((*recit).CopyInfo[stStock]); // array, treat as no gap
                RegisterMoveDelta((*recit).EntryAddress[stStock] + sizeof((*recit).CopyInfo[stStock]), sizeof((*recit).CopyInfo[stStock]), delta); // nuke entry
                delta = AlignAddressByBlocksizeCrazy((*recit).CopyInfo[stStock].ulSource, (*recit).CopyInfo[stStock].ulCopyLen, 0, 4);
                RegisterMoveDelta((*recit).CopyInfo[stStock].ulSource + (*recit).CopyInfo[stStock].ulCopyLen, (*recit).CopyInfo[stStock].ulCopyLen, delta);
            } else {
                int delta =  AlignByBlocksize((*recit).RawBuffer[sourceslot].size() - (*recit).CopyInfo[stStock].ulCopyLen, 4);
                RegisterMoveDelta((*recit).CopyInfo[stStock].ulSource + (*recit).CopyInfo[stStock].ulCopyLen, (*recit).RawBuffer[sourceslot].size(), delta);
                Q_ASSERT((*recit).CopyInfo[stWork].ulCopyLen == (*recit).RawBuffer[sourceslot].size());
            }
        }
    }
    // TODO: StepDelta to CombineDelta with ROMHDR correct
    // TODO: add file expand
    QMap < DWORD,  TCollectorRecItem > DeltaCollector;
    int areaindex = 0;
    int totalfreeleft = 0;
    for (QVector < TAreaRec >::iterator areait = WorkROMHeaders.begin(); areait != WorkROMHeaders.end(); areait++) {
        DWORD collectorkey = (*areait).Header.physfirst;
        TCollectorRecItem& collitem = DeltaCollector[collectorkey]; // silent?
        collitem.AreaIndex = areaindex;
        collitem.StorageEnd = (*areait).Header.physlast;
        collitem.UsedSpace = (*areait).Header.physlast - (*areait).Header.physfirst; // 0~F used, last is 10
        if (fNoChainMode) {
            // Guess available size from ROMHDR
            collitem.ChainIndex = -1;
            QVector < TAreaRec >::iterator nextareait = areait + 1;
            if (nextareait == WorkROMHeaders.end()) {
                collitem.AreaSpace = fROMSize - (areait->Header.physfirst - fROMBaseAddress);
            } else {
                collitem.AreaSpace = nextareait->MarkAddress - areait->Header.physfirst;
            }
        } else {
            // Query available size from chain?
            for (QVector < XIPCHAIN_ENTRY >::iterator entryit = WorkXipChainRecs.begin(); entryit != WorkXipChainRecs.end(); entryit++) {
                // usOrder in 1, 2
                if ((*entryit).usOrder == areaindex + 1) {
                    collitem.ChainIndex = entryit - WorkXipChainRecs.begin(); // distance
                    collitem.AreaSpace = (*entryit).dwMaxLength;
                    Q_ASSERT(collitem.UsedSpace == (*entryit).dwLength);
                }
            }
        }
        Q_ASSERT(collitem.AreaSpace > 0);
        collitem.FreeSpace = collitem.AreaSpace - collitem.UsedSpace;
        collitem.ModCost = 0;
        for (TMoveRec::iterator moveit = fStepMoveRecs.begin(); moveit!= fStepMoveRecs.end(); moveit++) {
            // 0 >= 0~F < 10
            if (moveit.key() >= collectorkey && moveit.key() < collectorkey + collitem.AreaSpace) {
                collitem.ModCost += (*moveit).Delta;
            }
        }
        totalfreeleft += (collitem.FreeSpace - collitem.ModCost);
        areaindex++;
    }
    Q_ASSERT(DeltaCollector.size() == WorkROMHeaders.size());
    // we try to extend previous xip if they overlapped next
    // original address wanna translate can safety use old area, we only do additionally entire move at start
    Q_ASSERT(totalfreeleft >= 0);
    logprintf("Space check done. %s bytes (%d) left.\n", Num2Bytes(totalfreeleft).toAscii().data(), totalfreeleft);
    // check ROMHDR in nk.exe
    int bootrecindex = -1;
    for (TBundleRec::iterator recit = fSortedBuffer.begin(); recit != fSortedBuffer.end(); recit++) {
        TEntryType stocktype = BundleType2EntryType((*recit).ItemType[stWork]);
        if ((*recit).ItemNuked[stWork] == true) {
            continue;
        }
        if (stocktype == etModule) {
            // check each o32?
            // we treat o32/e32 as fixed one from RawBuffer in some lines ago
            for (int i = 0; i < (*recit).E32Info[stWork].e32_objcnt; i++) {
                int offset = fROMLoadAddressOverided - ((*recit).O32Info[stWork][i].o32_rva + (*recit).E32Info[stWork].e32_vbase);
                if (offset >= 0 && offset < (*recit).O32Info[stWork][i].o32_vsize) {
                    int newbootrecindex = recit - fSortedBuffer.begin();
                    if (bootrecindex != -1) {
                        // bootrecindex override
                        logprintf("new module rec %d replaced %d.", newbootrecindex, bootrecindex);
                    }
                    bootrecindex = newbootrecindex; // distance
                }
            }
        } else if (stocktype == etFile) {
            // TODO: boot from file?!
        } else if (stocktype == etCopy) {
            //
            int offset = fROMLoadAddressOverided - ((*recit).CopyInfo[stWork].ulDest + (*recit).CopyInfo[stStock].ulDestLen);
            if (offset >= 0 && offset < (*recit).CopyInfo[stStock].ulDestLen) {
                int newbootrecindex = recit - fSortedBuffer.begin();
                if (bootrecindex != -1) {
                    logprintf("new copy rec %d replaced %d.", newbootrecindex, bootrecindex);
                }
                bootrecindex = newbootrecindex; // distance
            }
        }
    }
    if (bootrecindex != -1) {
        // check ROMHDR fix
        TBundleRecItem& rec = fSortedBuffer[bootrecindex];
        TSourceSlot sourceslot = GetActiveSlotForUser(rec);
        TEntryType stocktype = BundleType2EntryType(rec.ItemType[stWork]);
        int delta = QueryStepMoveDelta(WorkROMHeaders[rec.Stage[stWork]].AreaMark.HeaderAddress); // ECEC +4
        if (delta != 0) {
            logprintf("try to fix rec %d boot bundle for hard coded ROMHDR hack.", bootrecindex);
            Q_ASSERT(rec.ItemNuked[stWork] == false); // don't nuke boot items.
            bool mayfix = false;
            if (stocktype == etModule) {
                for (int i = 0; i < rec.E32Info[stWork].e32_objcnt; i++) {
                    // TODO: find code pattern
                    // .text:8010EFB4                 LDR     R5, =dword_8010101C
                    // .text:8010EFB8                 LDR     R0, [R5]
                    // TODO: 80101020 detection (pExtension)
                    int offset = 0x8010101CUL - (rec.O32Info[stWork][i].o32_rva + rec.E32Info[stWork].e32_vbase);
                    if (offset >= 0 && offset < rec.O32Info[stWork][i].o32_vsize) {
                        if (sourceslot == stStock) {
                            rec.CopyMetaData(stStock, stIncoming);
                            rec.CopyBuffer(stStock, stIncoming);
                            rec.PureResource[stIncoming] = false;
                            rec.IncomingPresent = true;
                            sourceslot = stIncoming;
                        }
                        TPEDirector director(rec.RawBuffer[sourceslot].data(), rec.RawBuffer[sourceslot].size());
                        IMAGE_NT_HEADERS ntheaders = director.GetNTHeaders();
                        IMAGE_SECTION_HEADER* sections = director.GetSectionHeaders();
                        LPBYTE src = LPBYTE(rec.RawBuffer[sourceslot].data()) + sections[i].PointerToRawData;
                        delta = QueryStepMoveDelta(WorkROMHeaders[rec.Stage[stWork]].AreaMark.HeaderAddress); // ECEC +4
                        DWORD newHDR = WorkROMHeaders[rec.Stage[stWork]].AreaMark.HeaderAddress + delta;
                        memcpy_s(src + offset, rec.O32Info[stWork][i].o32_psize - offset, &newHDR, sizeof(newHDR)); // RawBuffer[stIncoming] modified for stWork
                        // TODO: loop
                        int loopcount = 0;
                        while (loopcount < 10) {
                            if (rec.O32Info[stWork][i].o32_flags & IMAGE_SCN_COMPRESSED) {
                                // FIXME: never verified
                                // no qMin for Misc?
                                int filedatasize = qMin(sections[i].SizeOfRawData, sections[i].Misc.VirtualSize);
                                QByteArray packedsec = qCompressMSLZX(src, sections[i].SizeOfRawData);
                                // o32_psize, lzxed size
                                int workdatasize = qMin(rec.O32Info[stWork][i].o32_psize, rec.O32Info[stWork][i].o32_vsize);
                                delta = AlignByBlocksize(packedsec.size() - workdatasize, 4);
                                RegisterMoveDelta(rec.O32Info[stStock][i].o32_dataptr + rec.O32Info[stStock][i].o32_psize, sections[i].SizeOfRawData, delta);
                                rec.O32Info[stWork][i].o32_psize = packedsec.size(); // Min?

                                EasySections[bundleindex][i] = packedsec;
                            } else {
                                // no compression
                                // TODO: more safe gap detection
                                int workdatasize = qMin(rec.O32Info[stWork][i].o32_psize, rec.O32Info[stWork][i].o32_vsize);
                                int filedatasize = qMin(sections[i].SizeOfRawData, sections[i].Misc.VirtualSize);
                                delta = AlignByBlocksize(filedatasize - workdatasize, 4);
                                RegisterMoveDelta(rec.O32Info[stStock][i].o32_dataptr + workdatasize, workdatasize, delta);
#ifdef _DEBUG
                                o32_rom or = rec.O32Info[stWork][i];
#endif // _DEBUG
                                rec.O32Info[stWork][i].o32_psize = sections[i].SizeOfRawData; // Min?
                            }
                            if (delta == 0) {
                                mayfix = true;
                                logprintf("applied boot module hack, your xip may bootable.");
                                break;
                            }
                            loopcount++;
                        }
                    }

                }
            } else if (stocktype == etFile) {
                // assert
            } else if (stocktype == etCopy) {
                // assert
            }
            if (mayfix == false) {
                // use workaround near ROMHDR
                delta = QueryStepMoveDelta(WorkROMHeaders[rec.Stage[stWork]].AreaMark.HeaderAddress); // ECEC +4
                if (delta < 0) {
                    // insert a NULL
                    RegisterMoveDelta(fROMHeaders[rec.Stage[stStock]].AreaMark.HeaderAddress, 0, -delta);
                    logprintf("applied ROMHDR brick workaround, your xip may bootable.");
                    mayfix = true;
                }
            }
            if (mayfix == false) {
                logprintf("your xip should broken. please fix boot module (nk.exe) manually.");
            }
        }
    }
    runpercent = 13;
    emit prepareBufferStageChanged(tr("Merge move table."));
    emit prepareBufferProgressChanged(runpercent);
    for (QMap < DWORD,  TCollectorRecItem >::iterator colit = DeltaCollector.begin(); colit != DeltaCollector.end(); colit++) {
        // Combine current area move rec
        int deltasum = 0;
        for (TMoveRec::iterator smovit = fStepMoveRecs.lowerBound(colit.key()); smovit != fStepMoveRecs.end(); smovit++) {
            // 8384,A58F >= 8010,0000 + 01B0,0000
            // physfirst + dwMaxLength = area original end + 1 (0+10=F+1)
            if (smovit.key() >= colit.key() + (*colit).AreaSpace) {
                // we hope build an faster query map now
                break;
            }
            deltasum += (*smovit).Delta;
            TBundleMoveRecItem item;
            item.Size  = (*smovit).Size;
            item.Delta = deltasum;
            fAreaMoveRecs.insert(smovit.key(), item);
        }
        // partial move ready for use
        if ((*colit).FreeSpace < (*colit).ModCost) {
            // get next to check
            QMap < DWORD,  TCollectorRecItem >::iterator nextcolit = colit + 1; // colit could be last
            Q_ASSERT(nextcolit != DeltaCollector.end()); // I consider extend last area is crazy. don't fake an ISO into xip 
            if (nextcolit == DeltaCollector.end()) {
                break;
            } else {
                // dangerous. you have reached the deadline.
                // we must do lots of modification to Header n XipChain.
                // I can't promise that your M8 can startup with produced xip. Use at your own risk.
                int piratebytes = AlignByBlocksize((*colit).ModCost -(*colit).FreeSpace, 0x1000); // IMGOFSINCREMENT
                // an typical ROMHDR should place in n040 position.
                // maybe you can get an working ROM with nonstandard by specific a unaligned one in chain,
                // but I can't make sure it won't cause other problem, or performance lost.
                nextcolit->ModCost += piratebytes; // It's time to take your space for your brother
                // we don't touch 0~40 now
                // TODO: take care connpnl.cpl in 0~3C on extend (implement an move method)
                DWORD NextMarkAddress = WorkROMHeaders[(*nextcolit).AreaIndex].MarkAddress;
                DWORD NextEECCAddress = NextMarkAddress + (INT32(&WorkROMHeaders[(*nextcolit).AreaIndex].AreaMark.ECEC) - INT32(&WorkROMHeaders[(*nextcolit).AreaIndex].AreaMark.Signature));
                // I just wanna though a assert failure if already something relocated for n000 ~ n040 before our EECC brick
                for (TMoveRec::iterator tmovit = fStepMoveRecs.begin(); tmovit != fStepMoveRecs.end(); tmovit++) {
                    Q_ASSERT(tmovit.key() < NextMarkAddress && tmovit.key() > NextEECCAddress + 12);
                }
                RegisterMoveDelta(NextMarkAddress, 0x1000, piratebytes); // will combine in next turn for only content use
                // TODO: Direct modify ROMHDR
                //WorkROMHeaders[(*nextcolit).AreaIndex].Header.
                WorkXipChainRecs[(*colit).ChainIndex].dwMaxLength += piratebytes;
                WorkXipChainRecs[(*nextcolit).ChainIndex].dwMaxLength -= piratebytes;
            }
        } else {
            // nothing to do. space manman
            // TODO: insert an OK brick in 81C0,0000
            // we may go here one or twice
            // RegisterMoveDelta(fROMHeaders[(*colit).AreaIndex].MarkAddress, 0x1000, 0, true);
            // I add some stuff here for temporary emit merge code merge... 
            if (fAreaMoveRecs.contains(fROMHeaders[(*colit).AreaIndex].MarkAddress) == false) {
                TBundleMoveRecItem brickitem;
                brickitem.Delta = 0;
                brickitem.Size = 0x1000;
                fAreaMoveRecs.insert(fROMHeaders[(*colit).AreaIndex].MarkAddress, brickitem);
            }
        }
        // we assume these rules from M$ rom tools:
        // 1. content of area never place out of used space
        // 2. mod / file space align by 4
        // 3. physlast is next byte follow by space
        // 4. 41C (8010101C) in nk.exe point to main ROMHDR (followed by ECEC)
        // TODO: try to fix delta in nk.exe more earlier -- may cause other problem
        // current Mark Address, Safe. ECEC Address, Safe. physfirst safe.
#ifdef _DEBUG
        QFile qfile(QCoreApplication::applicationDirPath() + "/fMoveArea.txt");
        if (qfile.open(QFile::WriteOnly | QFile::Append)) {
            QTextStream out(&qfile);
            out << "Combined Area Move:\n";
            int index = 0;
            for (TMoveRec::iterator tmovit = fAreaMoveRecs.begin(); tmovit != fAreaMoveRecs.end(); tmovit++) {
                QString line = QString("rec[%1]: %2, %3%4 (%5)\n").arg(index).arg(tmovit.key(), 8, 16, QChar('0'))
                    .arg((*tmovit).Delta>0?"":"-").arg(abs((*tmovit).Delta), 0, 16, QChar('0')).arg((*tmovit).Delta);
                out << line;
                index++;
            }
            qfile.close();
        }
#endif
        // we modify local ROMHeaders, use fresh Merged lookup table
        int delta;
        int curarea = (*colit).AreaIndex;
        int curchain = (*colit).ChainIndex;
        delta = ApplyAreaMoveDelta(WorkROMHeaders[curarea].AreaMark.HeaderAddress); // for +44, +48
        if (delta != 0) {
            logprintf("Moving ROMHDR %s0x%X (%d)\n0x%X -> 0x%X", delta < 0?"-":"", abs(delta), delta,
                WorkROMHeaders[curarea].AreaMark.HeaderAddress - delta, WorkROMHeaders[curarea].AreaMark.HeaderAddress);
        }
        WorkROMHeaders[curarea].AreaMark.HeaderOffset += delta; // apply same delta to offset like address
        delta = ApplyAreaMoveDelta(WorkROMHeaders[curarea].MarkAddress); // 8010,0000
        delta = ApplyAreaMoveDelta((DWORD&)WorkROMHeaders[curarea].Header.pExtensions); // 8010,1020 point to nk.exe and target section should not be compressed
        // TODO: fix Extension boost chain in nk.exe
        delta = ApplyAreaMoveDelta(WorkROMHeaders[curarea].Header.physfirst); // 8010,0000
        delta = ApplyAreaMoveDelta(WorkROMHeaders[curarea].Header.physlast); // may same as ModCost
        if ((*colit).FreeSpace < (*colit).ModCost) {
            Q_ASSERT(delta == (*colit).ModCost);
        }
        delta = ApplyAreaMoveDelta(WorkROMHeaders[curarea].Header.ulCopyOffset);
        //delta = ApplyAreaMoveDelta(WorkROMHeaders[(*colit).AreaIndex].Header.ulProfileOffset); // unused
        if (fNoChainMode == false) {
            delta = ApplyAreaMoveDelta((DWORD&)WorkXipChainRecs[curchain].pvAddr);
            WorkXipChainRecs[curchain].dwLength += (*colit).ModCost;
        }
    }
    // TODO: process all offset merge
    fStepMoveRecs.clear(); // end of last RegisterMoveDelta for AreaMoveRec

    runpercent = 16;
    emit prepareBufferStageChanged(tr("Flush bundle rec."));
    emit prepareBufferProgressChanged(runpercent);
    //TBlockKeeper ModLoadKeeper;
    DualIntMap StockModLayout;
    // TODO: Merge to better circle
    for (TBundleRec::iterator recit = fSortedBuffer.begin(); recit != fSortedBuffer.end(); recit++) {
        // we sort from stock
        TEntryType stocktype = BundleType2EntryType((*recit).ItemType[stStock]);
        if (stocktype == etModule) {
            int imagesize = (*recit).E32Info[stWork].e32_vsize;
            StockModLayout.insertMulti((*recit).TocInfo[stStock].ulLoadOffset, imagesize);
        }
    }
    for (TBundleRec::iterator recit = fSortedBuffer.begin(); recit != fSortedBuffer.end(); recit++) {
        TEntryType stocktype = BundleType2EntryType((*recit).ItemType[stStock]);
        if (stocktype == etModule) {
            int sourceslot = GetActiveSlotForUser(*recit);
            if (BundleType2EntryType((*recit).ItemType[sourceslot]) == etModule && (*recit).ItemNuked[sourceslot] == true && (*recit).E32Info[stWork].e32_vsize > 0) {
                DualIntMap::iterator loadit = StockModLayout.find((*recit).TocInfo[stWork].ulLoadOffset);
                if (loadit != StockModLayout.end()) {
                    DualIntMap::iterator nextit = loadit + 1;
                    if (nextit != StockModLayout.end()) {
                        //layoutdelta -= (nextit.key() - loadit.key()); // = ulLoadOffset
                        RegisterMoveDelta(loadit.key(), nextit.key() - loadit.key(), loadit.key() - nextit.key());
                    }
                }
            }
        }
    }
    int deltasum = 0;
    DualIntMap NewModDelta;
    for (TMoveRec::iterator smovit = fStepMoveRecs.begin(); smovit != fStepMoveRecs.end(); smovit++) {
        // 8384,A58F >= 8010,0000 + 01B0,0000
        // physfirst + dwMaxLength = area original end + 1 (0+10=F+1)
        deltasum += (*smovit).Delta;
        
        NewModDelta.insert(smovit.key(), deltasum);
    }
    // we working on work slot. we compare it to stock
    // writing back entrys n contents
    bundleindex = 0;
    for (TBundleRec::iterator recit = fSortedBuffer.begin(); recit != fSortedBuffer.end(); recit++) {
        // dangerous
        TBundleRecItem& currec = *recit; // Writable rec, Debugger friendly
        int sourceslot = GetActiveSlotForUser(currec);
        if (currec.ItemNuked[stWork]) {
            bundleindex++;
            continue; // don't store recycled rec
        }
        TEntryType stocktype = BundleType2EntryType(currec.ItemType[sourceslot]);
        if (stocktype == etModule) {
            // time to director. nk.exe safe now
            TPEDirector director(currec.RawBuffer[sourceslot].data(), currec.RawBuffer[sourceslot].size());
            Q_ASSERT(director.IsNull() == false);
            IMAGE_NT_HEADERS ntheaders = director.GetNTHeaders();
            Q_ASSERT(ntheaders.FileHeader.NumberOfSections == (*recit).E32Info[stWork].e32_objcnt);
            Q_ASSERT(currec.E32Info[stWork].e32_objcnt == currec.O32Info[stWork].size());
            Q_ASSERT(currec.E32Info[stWork].e32_vbase == ntheaders.OptionalHeader.ImageBase);
            Q_ASSERT(currec.E32Info[stWork].e32_vsize >= ntheaders.OptionalHeader.SizeOfImage);
            int delta = ApplyAreaMoveDelta(currec.EntryAddress[stWork]); // don't touch source
            delta = ApplyAreaMoveDelta(currec.TocInfo[stWork].ulE32Offset);
            delta = ApplyAreaMoveDelta(currec.TocInfo[stWork].ulO32Offset);
            //delta = ApplyAreaMoveDelta((*recit).TocInfo[stWork].ulLoadOffset); // TODO: filedata calc, loadoffset resort
            DualIntMap::iterator loadit = NewModDelta.find(currec.TocInfo[stWork].ulLoadOffset);
            if (loadit != NewModDelta.end()) {
                currec.TocInfo[stWork].ulLoadOffset += (*loadit);
            }
            delta = ApplyAreaMoveDelta((DWORD&)currec.TocInfo[stWork].lpszFileName);
            //delta = ApplyAreaMoveDelta((*recit).E32Info[stWork].e32_vbase);
            for (int i = 0; i < currec.E32Info[stWork].e32_objcnt; i++) {
                delta = ApplyAreaMoveDelta(currec.O32Info[stWork][i].o32_dataptr);
                //delta = ApplyAreaMoveDelta((*recit).O32Info[sourceslot][i].o32_realaddr);
                // store each section?
                IMAGE_SECTION_HEADER* sections = director.GetSectionHeaders();
                LPBYTE src = LPBYTE(currec.RawBuffer[sourceslot].data()) + sections[i].PointerToRawData;
                // take care to set o32_flag when added in?
                if (currec.O32Info[stWork][i].o32_flags & IMAGE_SCN_COMPRESSED) {
                    QByteArray packedsec;
                    if (sourceslot == stStock) {
                        packedsec = qCompressMSLZX(src, sections[i].SizeOfRawData);
                    } else {
                        packedsec = EasySections[bundleindex][i];
                    }
                    // o32_psize, compressed
                    Q_ASSERT(packedsec.size() == currec.O32Info[stWork][i].o32_psize);
#ifdef _DEBUG
                    if (packedsec.size() != (*recit).O32Info[sourceslot][i].o32_psize) {
                        o32_rom or = (*recit).O32Info[sourceslot][i];
                    }
#endif
                    char* dest = GetVAData(currec.O32Info[stWork][i].o32_dataptr); // 8010xxxx
                    memcpy_s(dest, fROMSize + fROMBaseAddress - currec.O32Info[stWork][i].o32_dataptr, packedsec.data(), packedsec.size());
                } else {
                    // no compression
                    // o32_psize, rawsize
                    char* dest = GetVAData(currec.O32Info[stWork][i].o32_dataptr); // 8010xxxx
                    int filedatalen = qMin(sections[i].SizeOfRawData, sections[i].Misc.VirtualSize);
                    memcpy_s(dest, fROMSize + fROMBaseAddress - currec.O32Info[stWork][i].o32_dataptr, src, filedatalen);
                }
            }
            // Write back Meta
            char* dest = GetVAData(currec.EntryAddress[stWork]); // item of top group
            memcpy_s(dest, sizeof(currec.TocInfo[stWork]), &(currec.TocInfo[stWork]), sizeof(currec.TocInfo[stWork]));
            dest = GetVAData(currec.TocInfo[stWork].ulE32Offset);
            memcpy_s(dest, sizeof(currec.E32Info[stWork]), &(currec.E32Info[stWork]), sizeof(currec.E32Info[stWork]));
            dest = GetVAData(currec.TocInfo[stWork].ulO32Offset);
            for (int i = 0; i < currec.E32Info[stWork].e32_objcnt; i++) {
                memcpy_s(dest, sizeof(currec.O32Info[stWork][i]), &(currec.O32Info[stWork][i]), sizeof(currec.O32Info[stWork][i]));
                dest += sizeof(currec.O32Info[stWork][i]);
            }
            dest = GetVAData(DWORD(currec.TocInfo[stWork].lpszFileName));
            memcpy_s(dest, currec.Filename[stWork].size(), currec.Filename[stWork].data(), currec.Filename[stWork].size());
            dest += currec.Filename[stWork].size();
            memset(dest, 0, 1);
        } else if (stocktype == etFile) {
            // file back
            // may have user files
            Q_ASSERT(currec.RawBuffer[sourceslot].size() == currec.FileInfo[stWork].nCompFileSize);
            int delta = ApplyAreaMoveDelta(currec.EntryAddress[stWork]); // safe stock / incoming
            delta = ApplyAreaMoveDelta(currec.FileInfo[stWork].ulLoadOffset);
            delta = ApplyAreaMoveDelta((DWORD&)currec.FileInfo[stWork].lpszFileName);
            char* dest = GetVAData(currec.FileInfo[stWork].ulLoadOffset);
            memcpy_s(dest, currec.RawBuffer[sourceslot].size(), currec.RawBuffer[sourceslot].data(), currec.RawBuffer[sourceslot].size());
            // Write back Meta
            dest = GetVAData(currec.EntryAddress[stWork]); // item of top group
            memcpy_s(dest, sizeof(currec.FileInfo[stWork]), &(currec.FileInfo[stWork]), sizeof(currec.FileInfo[stWork]));
            dest = GetVAData(DWORD(currec.FileInfo[stWork].lpszFileName));
            memcpy_s(dest, currec.Filename[stWork].size(), currec.Filename[stWork].data(), currec.Filename[stWork].size());
            dest += currec.Filename[stWork].size();
            memset(dest, 0, 1);
        } else if (stocktype == etCopy) {
            // dangerous
            Q_ASSERT(currec.RawBuffer[sourceslot].size() == currec.CopyInfo[stWork].ulCopyLen);
            int delta = ApplyAreaMoveDelta(currec.EntryAddress[stWork]); // ok
            delta = ApplyAreaMoveDelta(currec.CopyInfo[stWork].ulSource);
            char* dest = GetVAData(currec.CopyInfo[stWork].ulSource);
            memcpy_s(dest, currec.RawBuffer[sourceslot].size(), currec.RawBuffer[sourceslot].data(), currec.RawBuffer[sourceslot].size());
            // Write back Meta
            dest = GetVAData(currec.EntryAddress[stWork]); // Header
            memcpy_s(dest, sizeof(currec.CopyInfo[stWork]), &(currec.CopyInfo[stWork]), sizeof(currec.CopyInfo[stWork]));
        }
        int curpercent = bundleindex * 75 / fSortedBuffer.size() + 16;
        if (curpercent > runpercent) {
            runpercent = curpercent;
            emit prepareBufferProgressChanged(runpercent);
        }
        bundleindex++;
    }
#ifdef _DEBUG
    QFile qfile2(QCoreApplication::applicationDirPath() + "/ModLoadKeeper.txt");
    if (qfile2.open(QFile::WriteOnly | QFile::Append)) {
        QTextStream out(&qfile2);
        out << "Module load Layout:\n";
        int index = 0;
        for (DualIntMap::iterator tloadit = StockModLayout.begin(); tloadit != StockModLayout.end(); tloadit++) {
            QString line = QString("rec[%1]: %2, %3%4 (%5)\n").arg(index).arg(tloadit.key(), 8, 16, QChar('0'))
                .arg((*tloadit)>0?"":"-").arg(abs((*tloadit)), 0, 16, QChar('0')).arg((*tloadit));
            out << line;
            DualIntMap::iterator nextit = tloadit + 1;
            if (nextit != StockModLayout.end()) {
                int gap = nextit.key() - (tloadit.key() + (*tloadit));
                if (gap != 0) {
                    line = QString("gap[%1]: %2, %3%4 (%5)\n").arg(index).arg(tloadit.key() + (*tloadit), 8, 16, QChar('0'))
                        .arg(gap>0?"":"-").arg(abs(gap), 0, 16, QChar('0')).arg(gap);
                    out << line;
                }
            }
            index++;
        }
        qfile2.close();
    }
#endif // _DEBUG
    runpercent = 91;
    emit prepareBufferStageChanged(tr("Write back ROM Header."));
    emit prepareBufferProgressChanged(runpercent);
    // move delta data ready to use after here
    // ROMHDR back to FlatBuffer
    QSet < DWORD > ExtensionBoosts;
    for (QVector < TAreaRec >::iterator areait = WorkROMHeaders.begin(); areait != WorkROMHeaders.end(); areait++ ) {
        TAreaRec arearec = *areait; // a temp one

        char* dest = GetVAData(arearec.MarkAddress); // 8010xxxx
        if (arearec.PureSignature) {
            memcpy_s(dest, sizeof(arearec.AreaMark.Signature), &(arearec.AreaMark.Signature), sizeof(arearec.AreaMark.Signature));
        }
        dest += (INT32(&arearec.AreaMark.ECEC) - INT32(&arearec.AreaMark.Signature));
        if (arearec.PureECEC) {
            // TODO: size of
            memcpy_s(dest, 12, &(arearec.AreaMark.ECEC), 12);
        }
        dest = GetVAData(arearec.AreaMark.HeaderAddress);
        memcpy_s(dest, sizeof(arearec.Header), &(arearec.Header), sizeof(arearec.Header));

        // TODO: pExtsnsions
        if (arearec.Header.pExtensions > 0 && ExtensionBoosts.contains(DWORD(arearec.Header.pExtensions)) == false) {
            ExtensionBoosts.insert(DWORD(arearec.Header.pExtensions));
        }
    }
    runpercent = 93;
    emit prepareBufferStageChanged(tr("Write back ext rec."));
    emit prepareBufferProgressChanged(runpercent);
    for (QVector < TROMExtRecItem >::iterator extit = fExtRecs.begin(); extit != fExtRecs.end(); extit++) {
        TROMExtRecItem extitem = *extit; // temporary ExtRecItem
        int delta = ApplyAreaMoveDelta((DWORD&)(extitem.ExtEntry.pdata));
        Q_ASSERT(extitem.HeaderIndex >= 0 && extitem.HeaderIndex < WorkROMHeaders.size());
        delta = ApplyAreaMoveDelta(extitem.EntryAddress); // pExtensions + n * element
        delta = ApplyAreaMoveDelta((DWORD&)extitem.ExtEntry.pNextExt);
        char* dest = GetVAData(extitem.EntryAddress);
        memcpy_s(dest, sizeof(extitem.ExtEntry), &(extitem.ExtEntry), sizeof(extitem.ExtEntry));
        dest = GetVAData((DWORD&)(extitem.ExtEntry.pdata));
        memcpy_s(dest, extitem.ExtData.size(), extitem.ExtData.data(), extitem.ExtData.size());
    }
    foreach(DWORD extaddress, ExtensionBoosts) {
        ROMPID* pid = (ROMPID*)GetVAData(extaddress);
        int delta = ApplyAreaMoveDelta((DWORD&)pid->pNextExt); // inplace mode.
        // TODO: make sure o32 section for extension of nk.exe is unpressed.
    }
    if (bootrecindex != -1) {
        /*TBundleRecItem& rec = fSortedBuffer[bootrecindex];
        // nuke incoming?
        rec.IncomingPresent = false;
        rec.RawBuffer[stIncoming].clear();
        rec.RawHash[stIncoming].clear();
        rec.Filename[stIncoming].clear();*/
    }
    runpercent = 98;
    emit prepareBufferStageChanged(tr("Write back xip chain."));
    emit prepareBufferProgressChanged(runpercent);
    char* dest = GetVAData(fROMXipChainAddress); // do not need relocate chain overlay
    DWORD chaincount = fXipChainRecs.size();
    memcpy_s(dest, sizeof(chaincount), &(chaincount), sizeof(chaincount));
    dest += sizeof(chaincount);
    for (QVector < XIPCHAIN_ENTRY >::iterator xipit = WorkXipChainRecs.begin(); xipit != WorkXipChainRecs.end(); xipit++) {
        XIPCHAIN_ENTRY chainentry = *xipit;
        memcpy_s(dest, sizeof(chainentry), &chainentry, sizeof(chainentry));
        dest += sizeof(chainentry);
    }
    Result = true;

    // TODO: clear
    fStepMoveRecs.clear();
    fAreaMoveRecs.clear();
    EasySections.clear(); // for it?
    WorkROMHeaders.clear();
    WorkXipChainRecs.clear();
    DeltaCollector.clear();
    runpercent = 100;
    emit prepareBufferProgressChanged(runpercent);

    return Result;
}


DWORD ROMHelper::ReadROMInfo( TROMInfoIndex index )
{
    DWORD Result = 0xFFFFFFFFUL;
    switch (index)
    {
    case riiBassAddress:
        Result = fROMBaseAddress;
        break;
    case riiSize:
        Result = fROMSize;
        break;
    case riiLoadAddress:
        Result = fROMLoadAddress;
        break;
    case riiLoadAddressOverided:
        Result = fROMLoadAddressOverided;
        break;
    case riiXipChainAddress:
        Result = fROMXipChainAddress;
    }
    return Result;
}

TMeizuOEMTag ROMHelper::ReadOEMTag()
{
    TMeizuOEMTag Result = {0};
    int nkindex = QueryNKIndex();
    if (nkindex != -1) {
        TBundleRecItem item = fSortedBuffer[nkindex];
        TSourceSlot sourceslot = GetActiveSlotForUser(item);
        TEntryType entrytype = BundleType2EntryType(item.ItemType[sourceslot]);
        if (entrytype == etModule) {
            int meizutagoffset = SearchOEMTagInNK(item.RawBuffer[sourceslot]);
            if (meizutagoffset != 0xFFFFFFFFUL) {
                Result = *PMeizuOEMTag(item.RawBuffer[sourceslot].data() + meizutagoffset + 8);
            }
        }
    }
    return Result;
}

int ROMHelper::SearchOEMTagInNK(QByteArray& data)
{
    int Result = -1;
    // we search for Meizu OEM Tag by SPI_GETPLATFORMVERSION
    //.text:80129998 off_80129998    DCD dword_8010CEF4      ; DATA XREF: sub_80129840+7C
    //.text:8012999C off_8012999C    DCD aSpi_getplatfor     ; DATA XREF: sub_80129840+24
    //.text:8012999C                                         ; "SPI_GETPLATFORMVERSION"
#ifdef __GNUC__
    ushort spiutf16array[] = {L'S',L'P',L'I',L'_',L'G',L'E',L'T',L'P',L'L',L'A',L'T',L'F',L'O',L'R',L'M',L'V',L'E',L'R',L'S',L'I',L'O',L'N', 0};
#else
    ushort spiutf16array[] = L"SPI_GETPLATFORMVERSION";
#endif
    QByteArray spiutf16str((char*)&spiutf16array[0], sizeof(spiutf16array));
    // TODO: search pattern using in StringRipper
    int spioffset = data.indexOf(spiutf16str, 0);
    if (spioffset != -1) {
        // time for director?
        TPEDirector director(data.data(), data.size());
        DWORD spiaddress = director.Offset2VA(DWORD(spioffset));
        int spiptroffset = data.indexOf(QByteArray::fromRawData((char*)&spiaddress, sizeof(spiaddress)), 0);
        if (spiptroffset != -1) {
            DWORD meizutagaddress = *PDWORD32(data.data() + spiptroffset - 4);
            DWORD meizutagoffset = director.VA2Offset(meizutagaddress);
            if (meizutagoffset != 0xFFFFFFFFUL) {
                Result = meizutagoffset;
            }
        }
    }
    return Result;
}

int ROMHelper::QueryNKIndex()
{
    int nkindex = -1;
    QByteArray itemnamelatin("nk.exe");

    TBundleRec::iterator recit = fSortedBuffer.end();
    int recindex = fSortedBuffer.size();
    while (recit != fSortedBuffer.begin()) {
        recit--;
        recindex--;
        TSourceSlot slot = GetActiveSlotForUser(*recit);
        if (recit->ItemNuked[slot]) {
            continue;
        }
        QByteArray key = recit->Filename[slot].toLower();
        if (key == itemnamelatin) {
            nkindex = recindex;
            break;
        }
    }
    return nkindex;
}

bool ROMHelper::WriteOEMTag( const TMeizuOEMTag newtag )
{
    bool Result = false;
    TMeizuOEMTag orgtag = {0};
    int nkindex = QueryNKIndex();
    if (nkindex != -1) {
        TBundleRecItem& item = fSortedBuffer[nkindex];
        TSourceSlot sourceslot = GetActiveSlotForUser(item);
        TEntryType entrytype = BundleType2EntryType(item.ItemType[sourceslot]);
        if (entrytype == etModule) {
            int meizutagoffset = SearchOEMTagInNK(item.RawBuffer[sourceslot]);
            if (meizutagoffset != 0xFFFFFFFFUL) {
                orgtag = *PMeizuOEMTag(item.RawBuffer[sourceslot].data() + meizutagoffset + 8);
                if (memcmp(&orgtag, &newtag, sizeof(orgtag)) == 0) {
                    return Result;
                }
                // write new
                if (sourceslot == stStock) {
                    item.CopyMetaData(sourceslot, stIncoming);
                    item.CopyBuffer(sourceslot, stIncoming);
                    item.IncomingPresent = true;
                } else if (sourceslot == stPrevious) {
                    item.CopyMetaData(sourceslot, stIncoming);
                    item.CopyBuffer(sourceslot, stIncoming);
                    item.IncomingPresent = true;
                }
                *PMeizuOEMTag(item.RawBuffer[stIncoming].data() + meizutagoffset + 8) = newtag;
                // TODO: add loopcount 10 to each module fix
                Result = true;
            }
        }
    }
    if (Result == true) {
        fProjectModified = true;
    }
    return Result;
}

ROMHelper::ROMHelper() : fProjectModified(false)
{

}
