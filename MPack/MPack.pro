TEMPLATE = app
QT += core \
    gui
CONFIG += qt \
    debug_and_relase
CONFIG(debug, debug|release) { 
    TARGET = MPack_d
    DESTDIR = ../Target/GCC/Debug
    MOC_DIR += ./tmp/moc/Debug
    OBJECTS_DIR += ./tmp/obj/Debug
}
else { 
    TARGET = MPack
    DESTDIR = ../Target/GCC/Release
    MOC_DIR += ./tmp/moc/Release
    OBJECTS_DIR += ./tmp/obj/Release
}
DEPENDPATH += .
UI_DIR += ./tmp
RCC_DIR += ./tmp
INCLUDEPATH += ./tmp \
    $MOC_DIR \
    . \
    ./../FvQKOLLite \
    ./LibMUIMaker
LIBS += -L"./PpsDES" \
    -L"./LibMUIMaker" \
    -lcompress_5 \
    -lLibMUIMaker \
    -lcereg400 \
    -lucl
HEADERS += ./Common.h \
    ./DbCentre.h \
    ./MainUnt.h \
    ./MyDropTreeWidget.h \
    ./NekoDriverUnt.h \
    ./OAKRemix.h \
    ./ROMUtils.h \
    ./XipStore.h \
    ./RegEdtUnt.h \
    ./PlatformBuilderImageUtils.h \
    ../FvQKOLLite/FvQKOL.h \
    ../FvQKOLLite/FvQMD5.h \
    ./LibMUIMaker/LibMUIMaker.h \
    ./ucl/ucl.h \
    ./ucl/ucl_asm.h \
    ./ucl/uclconf.h \
    LiteXipBuilderUnt.h

# Source files
SOURCES += ./DbCentre.cpp \
    ./main.cpp \
    ./MainUnt.cpp \
    ./MyDropTreeWidget.cpp \
    ./NekoDriverUnt.cpp \
    ./ROMUtils.cpp \
    ./ROMUtilsGAS.s \
    ./XipStore.cpp \
    ./XipStoreBase.cpp \
    ./XipStoreHelper.cpp \
    ./RegEdtUnt.cpp \
    ./PlatformBuilderImageUtils.cpp \
    ../FvQKOLLite/FvQKOL.cpp \
    ./LibMUIMaker/LibMUIMaker.cpp \
    LiteXipBuilderUnt.cpp

# Forms
FORMS += ./AboutFrm.ui \
    ./MainFrm.ui \
    ./ROMInfoFrm.ui \
    ./RegEdtFrm.ui \
    SingleLineEdtFrm.ui \
    DWORDEdtFrm.ui \
    MultiLineEdtFrm.ui \
    NewValueEdtFrm.ui \
    LiteXipBuilderFrm.ui

# Resource file(s)
RESOURCES += ./MPack.qrc

# Translation files
TRANSLATIONS += ./MPack_chs.ts

# Windows resource file
win32:RC_FILE = MPack.rc

# Compiler arg
QMAKE_CXXFLAGS += -std=gnu++0x
