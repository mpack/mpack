#include "RegEdtUnt.h"
#include "DbCentre.h"
#include "ui_SingleLineEdtFrm.h"
#include "ui_DWORDEdtFrm.h"
#include "ui_MultiLineEdtFrm.h"
#include <QtGUI/QMessageBox>


bool TRegEdtFrm::fCERegInitialized = false;

TRegEdtFrm::TRegEdtFrm( QWidget *parent, Qt::WFlags flags )
    : QMainWindow(parent, flags)
{
    ui.setupUi(this);
    //ui.retranslateUi(this); // dynamic
    fURLEdt.setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
    fURLEdt.setEditable(true);
    fURLEdt.setDuplicatesEnabled(false);
    fAddressLabel.setText(tr("Address:"));
    ui.addressToolbar->addWidget(&fAddressLabel);
    ui.addressToolbar->addWidget(&fURLEdt);
    fGoBtn.setText(tr("Go"));
    ui.addressToolbar->addWidget(&fGoBtn);
    QObject::connect(&fGoBtn, SIGNAL(clicked()), 
                     this, SLOT(onURLGoClicked()));
    fFolderIcon.addFile(":/RegEdtFrm/PpsDES/16x16-gnome2/stock_folder.png");
    fFolderIconOpened.addFile(":/RegEdtFrm/PpsDES/16x16-gnome2/folder_open.png");
    fSingleLineIcon.addFile(":/RegEdtFrm/PpsDES/KeyTypeSingleLine.png");
    fBinaryIcon.addFile(":/RegEdtFrm/PpsDES/KeyTypeBinary.png");
    fMultiLineIcon.addFile(":/RegEdtFrm/PpsDES/KeyTypeMultiLine.png");
    fMUIIcon.addFile(":/RegEdtFrm/PpsDES/KeyTypeMUI.png");

    if (StateSetting.RegEditorMaxium) {
        setWindowState(Qt::WindowMaximized);
    }
    if (StateSetting.RegFrmState.isEmpty() == false) {
        restoreState(StateSetting.RegFrmState);
    }
    if (StateSetting.KeyLayoutState.isEmpty() == false) {
        ui.splitterforkey->restoreState(StateSetting.KeyLayoutState);
    }
}

TRegEdtFrm::~TRegEdtFrm()
{
    SaveAppSettings(); // may close before main?
}

void TRegEdtFrm::closeEvent( QCloseEvent *event )
{
    if (IsPatchExists()) {
        QMessageBox msgBox;
        msgBox.setText(tr("The registry has been modified."));
        msgBox.setInformativeText(tr("Do you want to save your changes?"));
        msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Save);
        int ret = msgBox.exec();
        if (ret == QMessageBox::Save) {
            // Apply patch
            bool updated = ApplyPatchToHive();
            emit hiveUpdated(fHiveFilename, updated, true);
            event->accept();
            this->deleteLater();
        } else if (ret == QMessageBox::Discard) {
            emit hiveUpdated(fHiveFilename, false, true); // only nuke file
            event->accept();
            this->deleteLater();
        } else if (ret == QMessageBox::Cancel) {
            event->ignore();
        }
    } else {
        emit hiveUpdated(fHiveFilename, false, true); // only nuke file
        event->accept();
        this->deleteLater();
    }
    StateSetting.RegEditorMaxium = windowState() == Qt::WindowMaximized;
    StateSetting.RegFrmState = saveState();
    StateSetting.KeyLayoutState = ui.splitterforkey->saveState();
}

bool TRegEdtFrm::IsPatchExists()
{
    bool Result = false;
    Result = !(fValuePatchRec.isEmpty() && fKeyPatchRec.isEmpty());
    return Result;
}

void TRegEdtFrm::onEditorSaveClicked()
{
    // TODO: split open / parse
    if (IsPatchExists()) {
        bool updated = ApplyPatchToHive(); // Debug friendly
        emit hiveUpdated(fHiveFilename, updated, false);
        ParseHiveFromFile(fHiveFilename);
    }
}

void TRegEdtFrm::onEditorCloseClicked()
{
    this->close();
}

bool TRegEdtFrm::ApplyPatchToHive()
{
    bool updated = false;
    // TODO: hive file hash check
    if (IsPatchExists()) {
        // TODO: extract to some function like DoSave
        // TODO: mount hive for write
        CERegistryHive temphiv = CERegistryHive::OpenHive(fHiveFilename, true);
        CERegistryKey classeskey = temphiv.OpenRootKey(CERegistry::ClassesRoot());
        CERegistryKey curusrkey = temphiv.OpenRootKey(CERegistry::CurrentUser());
        CERegistryKey machinekey = temphiv.OpenRootKey(CERegistry::LocalMachine());
        CERegistryKey userskey = temphiv.OpenRootKey(CERegistry::Users());

        foreach(int keyindex, fKeyPatchRec) {
            // only create / delete
            TKeyBundleRecItem item = fKeyBundleRec[keyindex];
            QString ParentPath = fKeyBundleRec[item.ParentKeyIndex].ItemKey.Name();
            int rootkeyend = ParentPath.indexOf('\\');
            QString RootPath = rootkeyend == -1?ParentPath:ParentPath.left(rootkeyend);
            QString TailPath = rootkeyend == -1?"":ParentPath.right(ParentPath.length() - rootkeyend - 1);
            CERegistryKey subkey = GetSubkeyFromPlainPath(RootPath, TailPath, classeskey, curusrkey, machinekey, userskey);
            if (item.KeyNuked) {
                // nuke only
                if (item.StockPresent) {
                    if (subkey.DeleteKey(item.ItemKey.SubName())) {
                        updated = true;
                    }
                }
            } else {
                if (item.StockPresent == false) {
                    // incoming , un-modified?
                    item.ItemKey = subkey.CreateKey(item.ItemKey.SubName());
                    updated = true;
                }
            }
            subkey.Close();
        }
        foreach (int valueindex, fValuePatchRec) {
            TValueBundleRecItem item = fValueBundleRec[valueindex];
            QString ValuePath = fKeyBundleRec[item.ParentKeyIndex].ItemKey.Name();
            int rootkeyend = ValuePath.indexOf('\\');
            QString RootPath = rootkeyend == -1?ValuePath:ValuePath.left(rootkeyend);
            QString TailPath = rootkeyend == -1?"":ValuePath.right(ValuePath.length() - rootkeyend - 1);
            CERegistryKey subkey = GetSubkeyFromPlainPath(RootPath, TailPath, classeskey, curusrkey, machinekey, userskey);
            if (item.ValueNuked) {
                // nuke only
                if (item.StockPresent) {
                    if (subkey.DeleteValue(item.ItemValue.Name())) {
                        updated = true;
                    }
                }
            } else {
                if (item.ValueModified) {
                    CERegistryValue newvalue = item.ItemValue;
                    newvalue.Data() = item.NewValue;
                    if (subkey.SetValue(newvalue)) {
                        updated = true;
                    }
                }
                if (item.StockPresent == false) {
                    // incoming , un-modified?
                    if (subkey.SetValue(item.ItemValue)) {
                        updated = true;
                    }
                }
                if (item.ValueRenamed) {
                    CERegistryValue newvalue(item.NewName, item.ItemValue.Type(), item.ItemValue.Data());
                    if (subkey.SetValue(newvalue)) {
                        if (subkey.DeleteValue(item.ItemValue.Name())) {
                            updated = true;
                        }
                    }
                }
            }
            subkey.Close();
        }
        foreach(int keyindex, fKeyPatchRec) {
            TKeyBundleRecItem item = fKeyBundleRec[keyindex];
            if (item.KeyNuked || item.KeyRenamed == false) {
                continue;
            }
            QString ParentPath = fKeyBundleRec[item.ParentKeyIndex].ItemKey.Name();
            int rootkeyend = ParentPath.indexOf('\\');
            QString RootPath = rootkeyend == -1?ParentPath:ParentPath.left(rootkeyend);
            QString TailPath = rootkeyend == -1?"":ParentPath.right(ParentPath.length() - rootkeyend - 1);
            CERegistryKey subkey = GetSubkeyFromPlainPath(RootPath, TailPath, classeskey, curusrkey, machinekey, userskey);
            // LongList for Create sub keys/values
            subkey.Close();
        }

        classeskey.Close();
        curusrkey.Close();
        machinekey.Close();
        userskey.Close();

        temphiv.Close();
    }
    return updated;
}

CERegistryKey TRegEdtFrm::GetSubkeyFromPlainPath( QString RootPath, QString TailPath, CERegistryKey& classes, CERegistryKey& curusr, CERegistryKey& machine, CERegistryKey& users )
{
    CERegistryKey subkey;
    if (RootPath == classes.Name()) {
        subkey = classes.OpenSubKey(TailPath);
    }
    if (RootPath == curusr.Name()) {
        subkey = curusr.OpenSubKey(TailPath);
    }
    if (RootPath == machine.Name()) {
        subkey = machine.OpenSubKey(TailPath);
    }
    if (RootPath == users.Name()) {
        subkey = users.OpenSubKey(TailPath);
    }
    return subkey;
}

bool TRegEdtFrm::ParseHiveFromFile( QString filename )
{
    fValuePatchRec.clear();
    fKeyPatchRec.clear();
    fKeyBundleRec.clear();
    fValuePatchRec.clear();
    ui.briefView->setRowCount(0);
    ui.briefView->clearContents();
    ui.keyView->clear();

    if (fCERegInitialized == false) {
        CEREG_Initialize();
        fCERegInitialized = true;
    }

    CERegistryHive temphiv = CERegistryHive::OpenHive(filename, false);

    CERegistryKey classeskey = temphiv.OpenRootKey(CERegistry::ClassesRoot());
    TestBuildTreeFromKey(classeskey, ui.keyView->invisibleRootItem());
    classeskey.Close();

    CERegistryKey curusrkey = temphiv.OpenRootKey(CERegistry::CurrentUser());
    TestBuildTreeFromKey(curusrkey, ui.keyView->invisibleRootItem());
    curusrkey.Close();

    CERegistryKey machinekey = temphiv.OpenRootKey(CERegistry::LocalMachine());
    TestBuildTreeFromKey(machinekey, ui.keyView->invisibleRootItem());
    machinekey.Close();

    CERegistryKey userskey = temphiv.OpenRootKey(CERegistry::Users());
    TestBuildTreeFromKey(userskey, ui.keyView->invisibleRootItem());
    userskey.Close();

    temphiv.Close();

    ui.keyView->resizeColumnToContents(0);
    ui.keyView->resizeColumnToContents(1);

    fHiveFilename = filename;

    setWindowModified(IsPatchExists());

    return true;
}

void TRegEdtFrm::TestBuildTreeFromKey( CERegistryKey &topkey, QTreeWidgetItem* topitem )
{
    // parent first
    int topkeyindex = fKeyBundleRec.size();
    uint count = topkey.GetSubKeyCount();
    QStringList cols;
    cols << topkey.SubName() << QString("%1").arg(count);
    QTreeWidgetItem *selfitem = new QTreeWidgetItem(cols);
    selfitem->setIcon(0, fFolderIcon);
    selfitem->setData(0, Qt::UserRole, topkeyindex);
    topitem->addChild(selfitem);

    TKeyBundleRecItem recitem;
    //recitem.StockPresent = true;
    //recitem.KeyNuked = false;
    //recitem.KeyRenamed = false;
    recitem.ItemKey = topkey;
    recitem.ParentKeyIndex = ui.keyView->invisibleRootItem() == topitem? -1 : topitem->data(0, Qt::UserRole).toInt();
    recitem.KeyWidget = selfitem; // lookup
    Q_ASSERT(recitem.ParentKeyIndex < fKeyBundleRec.size());
    // It's a choice. we can instead sort indexes into SubKeyIndexes before next call
    // or insert current indexes to parent callee?
    fKeyBundleRec.push_back(recitem);
    // we use slower way now for test.
    if (recitem.ParentKeyIndex >= 0 && recitem.ParentKeyIndex < fKeyBundleRec.size()) {
        fKeyBundleRec[recitem.ParentKeyIndex].SubKeyIndexes.insert(topkeyindex);
    }
    Q_ASSERT(topkeyindex == fKeyBundleRec.size() - 1);
    foreach (CERegistryValue value, topkey.GetValues() ) {
        TValueBundleRecItem recitem;
        recitem.StockPresent = true;
        recitem.ValueNuked = false;
        recitem.ValueRenamed = false;
        recitem.ValueModified = false;
        recitem.ItemValue = value;
        recitem.ParentKeyIndex = topkeyindex;
        fKeyBundleRec[recitem.ParentKeyIndex].ValueIndexes.insert(fValueBundleRec.size());
        fValueBundleRec.push_back(recitem); // value on sale
    }

    // sub second
    if (count > 0) {
        QStringList subkeynames = topkey.GetSubKeyNames();
        foreach ( QString subkeyname, subkeynames ) {
            CERegistryKey subkey = topkey.OpenSubKey(subkeyname);
            TestBuildTreeFromKey(subkey, selfitem);
        }
    }
    topkey.Close();
}

void TRegEdtFrm::onKeyitemExpanded( QTreeWidgetItem* item )
{
    item->setIcon(0, fFolderIconOpened);
}

void TRegEdtFrm::onKeyitemCollapsed( QTreeWidgetItem* item )
{
    item->setIcon(0, fFolderIcon);
}

void TRegEdtFrm::onKeyitemNavigated( QTreeWidgetItem* current, QTreeWidgetItem* previous )
{
    // Paint value
    // TODO: close previous
    if (previous != NULL) {
        // close?
    }
    if (current == NULL) {
        return;
    }
    int topkeyindex = current->data(0, Qt::UserRole).toInt();
    if (topkeyindex < 0 && topkeyindex >= fKeyBundleRec.size()) {
        return;
    }
    ShowSubContentsForKey(topkeyindex);
    fURLEdt.setEditText(fKeyBundleRec[topkeyindex].ItemKey.Name());
    ui.keyView->resizeColumnToContents(0);
    ui.keyView->resizeColumnToContents(1);
}

void TRegEdtFrm::onURLGoClicked()
{
    QString targetURL = fURLEdt.currentText();
    if (targetURL.endsWith('\\')) {
        targetURL = targetURL.left(targetURL.length());
    }
    for(TKeyBundleRec::iterator keyit = fKeyBundleRec.begin(); keyit != fKeyBundleRec.end(); keyit++) {
        if ((*keyit).ItemKey.Name().compare(targetURL, Qt::CaseInsensitive) == 0) {
            if (fURLEdt.findText(targetURL, Qt::MatchExactly) == -1) {
                fURLEdt.insertItem(0, fURLEdt.currentText());
            }
            // try navigate?
            ui.keyView->setCurrentItem((*keyit).KeyWidget, 0, QItemSelectionModel::SelectCurrent);
        }
    }
}

void TRegEdtFrm::ShowSubContentsForKey( int topkeyindex )
{
    fDiscardRenameCheck = true;
    ui.briefView->setRowCount(fKeyBundleRec[topkeyindex].SubKeyIndexes.size() + fKeyBundleRec[topkeyindex].ValueIndexes.size()); // TODO: size++
    ui.briefView->clearContents();
    int row = 0;
    foreach (int subkeyindex, fKeyBundleRec[topkeyindex].SubKeyIndexes) {
        // Keys first
        if (fKeyBundleRec[subkeyindex].KeyNuked) {
            continue;
        }
        QTableWidgetItem *tabitem = new QTableWidgetItem(fKeyBundleRec[subkeyindex].ItemKey.SubName());
        tabitem->setIcon(fFolderIcon);
        tabitem->setData(Qt::UserRole, subkeyindex);
        tabitem->setData(Qt::UserRole + 1, 0);
        //tabitem->setFlags(tabitem->flags() ^ Qt::ItemIsEditable); // TODO: Keyrename
        ui.briefView->setItem(row, 0, tabitem);
        tabitem = new QTableWidgetItem(tr("Key"));
        tabitem->setFlags(tabitem->flags() ^ Qt::ItemIsEditable);
        ui.briefView->setItem(row, 1, tabitem);

        row++;
    }
    foreach (int valueindex, fKeyBundleRec[topkeyindex].ValueIndexes)  {
        // Then values
        if (fValueBundleRec[valueindex].ValueNuked) {
            continue;
        }
        TValueBundleRecItem valuerec = fValueBundleRec[valueindex]; // TODO: copy?
        QTableWidgetItem *tabitem = new QTableWidgetItem(valuerec.ItemValue.Name());
        tabitem->setData(Qt::UserRole, valueindex);
        tabitem->setData(Qt::UserRole + 1, 1);
        tabitem->setIcon(SelectIconFromType(valuerec.ItemValue.Type()));
        ui.briefView->setItem(row, 0, tabitem);
        tabitem = new QTableWidgetItem(ValueType2Str(valuerec.ItemValue.Type()));
        tabitem->setFlags(tabitem->flags() ^ Qt::ItemIsEditable);
        ui.briefView->setItem(row, 1, tabitem);
        
        if (valuerec.ValueModified == false) {
            tabitem = new QTableWidgetItem(valuerec.ItemValue.Brief());
        } else {
            tabitem = new QTableWidgetItem(Data2Brief(valuerec.NewValue, valuerec.ItemValue.Type()));
        }
        tabitem->setFlags(tabitem->flags() ^ Qt::ItemIsEditable);
        tabitem->setData(Qt::ToolTipRole, valuerec.ItemValue.Data().toHex().toUpper());
        ui.briefView->setItem(row, 2, tabitem);
        tabitem = new QTableWidgetItem(QString("%1").arg(valuerec.ValueModified?valuerec.NewValue.size():valuerec.ItemValue.Data().size()));
        tabitem->setFlags(tabitem->flags() ^ Qt::ItemIsEditable);
        tabitem->setTextAlignment(Qt::AlignRight);
        ui.briefView->setItem(row, 3, tabitem);
        row++;
    }
    ui.briefView->resizeColumnToContents(0);
    ui.briefView->resizeColumnToContents(1);
    ui.briefView->resizeColumnToContents(2);
    fDiscardRenameCheck = false;
}

QIcon TRegEdtFrm::SelectIconFromType( RegistryValueDataType type )
{
    QIcon Result = fFolderIcon;
    switch (type) {
        case CE_REG_SZ:
        case CE_REG_EXPAND_SZ:
        case CE_REG_LINK:
            Result = fSingleLineIcon;
            break;
        case CE_REG_BINARY:
        case CE_REG_DWORD:
            Result = fBinaryIcon;
            break;
        case CE_REG_MULTI_SZ:
            Result = fMultiLineIcon;
            break;
        case CE_REG_MUI_SZ:
            Result = fMUIIcon;
            break;
    }
    return Result;
}

void TRegEdtFrm::onBriefDoubleClicked( QTableWidgetItem* item )
{
    QTableWidgetItem* currentitem = item;
    if (currentitem->column() != 0) {
        currentitem = ui.briefView->item(currentitem->row(), 0);
    }
    int bundleindex = currentitem->data(Qt::UserRole).toInt();
    int bundletype = currentitem->data(Qt::UserRole + 1).toUInt();

    if (bundletype == 1) {
        // Popup Edit box
        onBriefWannaModifyValue();
    }
    if (bundletype == 0) {
        // navigate
        ui.keyView->setCurrentItem(fKeyBundleRec[bundleindex].KeyWidget, 0, QItemSelectionModel::SelectCurrent);
    }
}

void TRegEdtFrm::onBriefViewRightClicked(const QPoint& pos)
{
    QMenu* menu = new QMenu;
    menu->addAction(ui.actionModify);
    menu->addAction(ui.actionDelete);
    menu->addAction(ui.actionRename);
    menu->addSeparator();
    menu->addAction(ui.actionNewKey);
    menu->addMenu(ui.menuNewValue);
    QPoint globalPos = ui.briefView->mapToGlobal(pos);
    menu->exec(globalPos);
}

void TRegEdtFrm::onBriefWannaModifyValue() {
    QTableWidgetItem* currentitem = ui.briefView->item(ui.briefView->currentRow(), 0);
    if (currentitem == NULL) {
        return;
    }
    if (currentitem->column() != 0) {
        currentitem = ui.briefView->item(currentitem->row(), 0);
    }
    int selectbundleindex = currentitem->data(Qt::UserRole).toInt();
    int selectbundletype = currentitem->data(Qt::UserRole + 1).toUInt();

    if (selectbundletype == 1) {
        QDialog *dialog = new QDialog;
        dialog->setWindowIcon(this->SelectIconFromType(fValueBundleRec[selectbundleindex].ItemValue.Type()));
        QPalette palette;
        palette.setColor(QPalette::Base, palette.color(QPalette::Window));
        switch (fValueBundleRec[selectbundleindex].ItemValue.Type()) {
            case CE_REG_SZ:
            case CE_REG_EXPAND_SZ:
            case CE_REG_LINK:
            case CE_REG_MUI_SZ:
                {
                    Ui::SingleLineEdtDialog editfrm;
                    editfrm.setupUi(dialog);
                    CERegistryValue orgvalue = fValueBundleRec[selectbundleindex].ItemValue;
                    QByteArray orgdata = fValueBundleRec[selectbundleindex].ValueModified?fValueBundleRec[selectbundleindex].NewValue:orgvalue.Data();
                    editfrm.nameEdt->setText(orgvalue.Name());
                    editfrm.nameEdt->setPalette(palette);
                    editfrm.valueEdt->setText(Data2Brief(orgdata, orgvalue.Type()));
                    QObject::connect(editfrm.valueEdt, SIGNAL(textChanged(QString)),
                        this, SLOT(onStringValueModified(QString)));

                }
                break;
            case CE_REG_BINARY:
                {

                }
                break;
            case CE_REG_DWORD:
                {
                    Ui::DWORDEdtDialog editfrm;
                    editfrm.setupUi(dialog);
                    CERegistryValue orgvalue = fValueBundleRec[selectbundleindex].ItemValue;
                    QByteArray orgdata = fValueBundleRec[selectbundleindex].ValueModified?fValueBundleRec[selectbundleindex].NewValue:orgvalue.Data();
                    editfrm.nameEdt->setText(orgvalue.Name());
                    editfrm.nameEdt->setPalette(palette);
                    editfrm.decEdt->setText(QString("%1").arg(*PDWORD32(orgdata.data())));
                    editfrm.hexEdt->setText(Data2Brief(orgdata, orgvalue.Type()));
                    QObject::connect(editfrm.decEdt, SIGNAL(textChanged(QString)),
                        this, SLOT(onDWORDValueDecModified(QString)));
                    QObject::connect(editfrm.hexEdt, SIGNAL(textChanged(QString)),
                        this, SLOT(onDWORDValueHexModified(QString)));
                }
                break;
            case CE_REG_MULTI_SZ:
                {
                    Ui::MultiLineEdtDialog editfrm;
                    editfrm.setupUi(dialog);
                    CERegistryValue orgvalue = fValueBundleRec[selectbundleindex].ItemValue;
                    QByteArray orgdata = fValueBundleRec[selectbundleindex].ValueModified?fValueBundleRec[selectbundleindex].NewValue:orgvalue.Data();
                    editfrm.nameEdt->setText(orgvalue.Name());
                    editfrm.nameEdt->setPalette(palette);
                    editfrm.valueEdt->setPlainText(Data2Brief(orgdata, orgvalue.Type()));
                    QObject::connect(editfrm.valueEdt, SIGNAL(textChanged()),
                        this, SLOT(onMultiLineValueModified())); // Failed?
                }
                break;
        }

        fEditingDialog = dialog;
        fEditingType = fValueBundleRec[selectbundleindex].ItemValue.Type();
        fEditingIndex = selectbundleindex;
        QObject::connect(dialog, SIGNAL(accepted()),
            this, SLOT(onEditorAccepted()));
        QObject::connect(dialog, SIGNAL(rejected()),
            this, SLOT(onEditorRejected()));
        dialog->show();
    }
}

void TRegEdtFrm::onBriefWannaDeleteValue()
{
    QList<QTableWidgetItem*> currentitems = ui.briefView->selectedItems();
    bool nuked = false;
    foreach (QTableWidgetItem* currentitem, currentitems) {
        if (currentitem == NULL) {
            continue;
        }
        if (currentitem->column() != 0) {
            currentitem = ui.briefView->item(currentitem->row(), 0);
        }
        int bundleindex = currentitem->data(Qt::UserRole).toInt();
        int bundletype = currentitem->data(Qt::UserRole + 1).toUInt();

        if (bundletype == 1) {
            // Value
            if (fValuePatchRec.contains(bundleindex)) {
                // free newvalue?
                if (fValueBundleRec[bundleindex].ValueModified) {
                    fValueBundleRec[bundleindex].NewValue.clear();
                    fValueBundleRec[bundleindex].ValueModified = false;
                    fValueBundleRec[bundleindex].NewName.clear();
                    fValueBundleRec[bundleindex].ValueRenamed = false;
                }
            }
            if (fValueBundleRec[bundleindex].StockPresent) {
                fValuePatchRec.insert(bundleindex);
                fValueBundleRec[bundleindex].ValueNuked = true;
            } else {
                // let the holder in fValueBundleRec.
                fKeyBundleRec[fValueBundleRec[bundleindex].ParentKeyIndex].ValueIndexes.remove(bundleindex);
                fValuePatchRec.remove(bundleindex); // no more patch need
            }

            nuked = true;
        }
        if (bundletype == 0) {
            // Key
            if (fKeyPatchRec.contains(bundleindex)) {
                // save newname
                fKeyBundleRec[bundleindex].NewName.clear();
                fKeyBundleRec[bundleindex].KeyRenamed = false;
            }
            if (fKeyBundleRec[bundleindex].StockPresent) {
                fKeyPatchRec.insert(bundleindex);
                fKeyBundleRec[bundleindex].KeyNuked = true;
            } else {
                // let the holder in fValueBundleRec.
                fKeyBundleRec[fValueBundleRec[bundleindex].ParentKeyIndex].SubKeyIndexes.remove(bundleindex);
                fKeyPatchRec.remove(bundleindex); // no more patch need
            }
            //ui.keyView->removeItemWidget(fKeyBundleRec[bundleindex].KeyWidget, 0);
            //ui.keyView->removeItemWidget(fKeyBundleRec[bundleindex].KeyWidget, 1);
            fKeyBundleRec[bundleindex].KeyWidget->setHidden(true);
            nuked = true;
        }
    }
    if (nuked) {
        // TODO: Extract to Refresh sub content for key
        RefreshBriefView();
    }
    setWindowModified(IsPatchExists());
}

void TRegEdtFrm::onBriefWannaRenameValue( QTableWidgetItem* item )
{
    if (fDiscardRenameCheck) {
        return;
    }
    if (item->column() != 0) {
        return; // only name can edit
    }
    int selectbundleindex = item->data(Qt::UserRole).toInt();
    int selectbundletype = item->data(Qt::UserRole + 1).toUInt();

    if (selectbundletype == 1) {
        // Value
        Q_ASSERT(selectbundleindex >= 0 && selectbundleindex < fValueBundleRec.size());
        TValueBundleRecItem& valurec = fValueBundleRec[selectbundleindex];
        valurec.ValueRenamed = false;
        valurec.NewName.clear();
        if (item->text().compare(valurec.ItemValue.Name(), Qt::CaseInsensitive) != 0) {
            if (valurec.StockPresent) {
                valurec.ValueRenamed = true;
                valurec.NewName = item->text();
                fValuePatchRec.insert(selectbundleindex);
            } else {
                // patch must exists
                Q_ASSERT(fValuePatchRec.contains(selectbundleindex));
                valurec.ItemValue = CERegistryValue(item->text(), valurec.ItemValue.Type(), valurec.ItemValue.Data());
            }
            RefreshBriefView();
        } else {
            if (!(valurec.ValueNuked || valurec.ValueModified) && valurec.StockPresent) {
                fValuePatchRec.remove(selectbundleindex);
            }
        }
    }
    if (selectbundletype == 0) {
        // Key
        Q_ASSERT(selectbundleindex >= 0 && selectbundleindex < fKeyBundleRec.size());
        TKeyBundleRecItem& keyrec = fKeyBundleRec[selectbundleindex];
        keyrec.KeyRenamed = false;
        keyrec.NewName.clear();
        if (item->text().compare(keyrec.ItemKey.SubName(), Qt::CaseInsensitive) != 0) {
            if (keyrec.StockPresent) {
                keyrec.KeyRenamed = true;
                keyrec.NewName = item->text(); // short one, instead with SubName
                fKeyPatchRec.insert(selectbundleindex);
            } else {
                // patch must exists
                Q_ASSERT(fKeyPatchRec.contains(selectbundleindex));
                keyrec.ItemKey = CERegistryKey(0, item->text(), fKeyBundleRec[keyrec.ParentKeyIndex].ItemKey.Name());
            }
            RefreshBriefView();
        } else {
            // restored original one
            if (keyrec.KeyNuked == false && keyrec.StockPresent) {
                fKeyPatchRec.remove(selectbundleindex);
            }
        }
        // build subkey in view?
        keyrec.KeyWidget->setText(0, keyrec.KeyRenamed?keyrec.NewName:keyrec.ItemKey.SubName());
    }
    setWindowModified(IsPatchExists());
}

bool TRegEdtFrm::BriefWannaNewValue( RegistryValueDataType type )
{
    bool Result = false;
    // Add new at end of fValueBundleRec
    QTreeWidgetItem* current = ui.keyView->currentItem();
    if (current == NULL) {
        return Result;
    }
    int parentindex = current->data(0, Qt::UserRole).toInt();
    if (parentindex < 0 || parentindex >= fKeyBundleRec.size()) {
        return Result;
    }

    bool recognized = false;
    QByteArray newvalue;
    switch (type) {
        case CE_REG_SZ:
        case CE_REG_EXPAND_SZ:
        case CE_REG_LINK:
        case CE_REG_MUI_SZ:
        case CE_REG_MULTI_SZ:
            {
                newvalue = QByteArray();
                newvalue.append(QByteArray(2, '\0'));
                recognized = true;
            }
            break;
        case CE_REG_BINARY:
            {
                newvalue = QByteArray();
                recognized = true;
            }
            break;
        case CE_REG_DWORD:
            {
                int zero = 0;
                newvalue = QByteArray::fromRawData((char*)&zero, sizeof(zero));
                recognized = true;
            }
            break;
    }
    if (recognized){
        TValueBundleRecItem item;
        item.ParentKeyIndex = parentindex;
        item.StockPresent = false;
        item.ItemValue = CERegistryValue(QString("%1-1").arg(ValueType2Str(type)), type, newvalue);
        fKeyBundleRec[parentindex].ValueIndexes.insert(fValueBundleRec.size());
        fValuePatchRec.insert(fValueBundleRec.size());
        fValueBundleRec.push_back(item);
        setWindowModified(IsPatchExists());
        RefreshBriefView();
    }
    return Result;
}

void TRegEdtFrm::onBriefNewSubKey()
{
    QTreeWidgetItem* current = ui.keyView->currentItem();
    if (current == NULL) {
        return;
    }
    int parentindex = current->data(0, Qt::UserRole).toInt();
    if (parentindex < 0 || parentindex >= fKeyBundleRec.size()) {
        return;
    }

    TKeyBundleRecItem item;
    item.ParentKeyIndex = parentindex;
    item.StockPresent = false;
    item.ItemKey = CERegistryKey(0, "NewSubKey-1", fKeyBundleRec[parentindex].ItemKey.Name());
    QStringList cols;
    cols << item.ItemKey.SubName() << "0";
    QTreeWidgetItem *keyitem = new QTreeWidgetItem(cols);
    item.KeyWidget = keyitem;
    keyitem->setIcon(0, fFolderIcon);
    keyitem->setData(0, Qt::UserRole, fKeyBundleRec.size()); // you have your new ID now, 9527
    fKeyBundleRec[parentindex].KeyWidget->addChild(keyitem);
    fKeyBundleRec[parentindex].SubKeyIndexes.insert(fKeyBundleRec.size());
    fKeyPatchRec.insert(fKeyBundleRec.size());
    fKeyBundleRec.push_back(item);
    setWindowModified(IsPatchExists());
    RefreshBriefView();
}

void TRegEdtFrm::onStringValueModified( QString text )
{
    fEditorValueStringExpr = text; // !~4
}


void TRegEdtFrm::onMultiLineValueModified()
{
    if (fEditingType == CE_REG_MULTI_SZ) {
        QPlainTextEdit* valueEdt = qFindChild<QPlainTextEdit*>((QDialog*)fEditingDialog, "valueEdt");
        if (valueEdt != NULL) {
            fEditorValueStringExpr = valueEdt->toPlainText().replace(QChar('\n'), QChar('\0'));
        }
    }
}

void TRegEdtFrm::onDWORDValueHexModified( QString hex )
{
    bool ok;
    uint newint = hex.toUInt(&ok, 16);
    if (ok) {
        fEditorValueDWORDExpr = newint;
        QLineEdit* decEdt = qFindChild<QLineEdit*>((QDialog*)fEditingDialog, "decEdt");
        if (decEdt != NULL) {
            decEdt->setText(QString("%1").arg(newint));
        }
    }
}

void TRegEdtFrm::onDWORDValueDecModified( QString dec )
{
    bool ok;
    uint newint = dec.toUInt(&ok, 10);
    if (ok) {
        fEditorValueDWORDExpr = newint;
        QLineEdit* hexEdt = qFindChild<QLineEdit*>((QDialog*)fEditingDialog, "hexEdt");
        if (hexEdt != NULL) {
            hexEdt->setText(QString("%1").arg(newint, 8, 16, QChar('0')).toUpper());
        }
    }
}

void TRegEdtFrm::onBinaryValueModified( QString bin )
{

}

void TRegEdtFrm::onEditorAccepted()
{
    bool recognized = false;
    QByteArray newvalue;
    int index = -1;
    switch (fEditingType) {
        case CE_REG_SZ:
        case CE_REG_EXPAND_SZ:
        case CE_REG_LINK:
        case CE_REG_MUI_SZ:
        case CE_REG_MULTI_SZ:
            {
                index = fEditingIndex;
                newvalue = QByteArray::fromRawData((char*)fEditorValueStringExpr.data(), fEditorValueStringExpr.length() * 2);
                newvalue.append(QByteArray(2, '\0'));
                recognized = true;
            }
            break;
        case CE_REG_BINARY:
            {
                index = fEditingIndex;
                newvalue = fEditorValueBinaryExpr;
                recognized = true;
            }
            break;
        case CE_REG_DWORD:
            {
                index = fEditingIndex;
                newvalue = QByteArray::fromRawData((char*)&fEditorValueDWORDExpr, sizeof(fEditorValueDWORDExpr));
                recognized = true;
            }
            break;
    }
    if (recognized){
        if (newvalue != fValueBundleRec[index].ItemValue.Data()) {
            if (fValueBundleRec[index].StockPresent) {
                // Patch
                fValuePatchRec.insert(index);
                fValueBundleRec[index].NewValue = newvalue;
                fValueBundleRec[index].ValueModified = true;
            } else {
                // Change in place
                Q_ASSERT(fValuePatchRec.contains(index));
                fValueBundleRec[index].ItemValue.Data() = newvalue;
            }
        } else {
            if (!(fValueBundleRec[index].ValueNuked || fValueBundleRec[index].ValueRenamed)) {
                fValuePatchRec.remove(index); // TODO: Merge
            }
            // snake, we get our kit back!
            fValueBundleRec[index].NewValue.clear();
            fValueBundleRec[index].ValueModified = false;
        }
        RefreshBriefView();
    }
    setWindowModified(IsPatchExists());
}

void TRegEdtFrm::onEditorRejected()
{
    fEditingDialog = NULL;
    fEditingType = CE_REG_NONE;
}

void TRegEdtFrm::RefreshBriefView()
{
    QTreeWidgetItem* current = ui.keyView->currentItem();
    if (current != NULL) {
        int topkeyindex = current->data(0, Qt::UserRole).toInt();
        if (topkeyindex >= 0 && topkeyindex < fKeyBundleRec.size()) {
            ShowSubContentsForKey(topkeyindex);
        }
    }
}

void TRegEdtFrm::onBreifNewStringValue()
{
    BriefWannaNewValue(CE_REG_SZ);
}

void TRegEdtFrm::onBreifNewDWORDValue()
{
    BriefWannaNewValue(CE_REG_DWORD);
}

void TRegEdtFrm::onBreifNewBinaryValue()
{
    BriefWannaNewValue(CE_REG_BINARY);
}

void TRegEdtFrm::onBreifNewMultiStringValue()
{
    BriefWannaNewValue(CE_REG_MULTI_SZ);
}

void TRegEdtFrm::onBreifNewMUIStringValue()
{
    BriefWannaNewValue(CE_REG_MUI_SZ);
}

void TRegEdtFrm::onBreifNewExpandStringValue()
{
    BriefWannaNewValue(CE_REG_EXPAND_SZ);
}

void TRegEdtFrm::onBreifNewLinkValue()
{
    BriefWannaNewValue(CE_REG_LINK);
}

void TRegEdtFrm::onBriefWannaStartRename()
{
    QTableWidgetItem* current = ui.briefView->item(ui.briefView->currentRow(), 0);;
    if (current == NULL) {
        return;
    }
    if (current->column() != 0) {
        current = ui.briefView->item(current->row(), 0);
    }
    ui.briefView->editItem(current);
}

tagKeyBundleRecItem::tagKeyBundleRecItem()
{
    StockPresent = true;
    KeyNuked = false;
    KeyRenamed = false;
    ParentKeyIndex = -1;
    KeyWidget = NULL;
}

tagValueBundleRecItem::tagValueBundleRecItem() : ItemValue("", CE_REG_NONE, QByteArray())
{
    StockPresent = true;
    ValueNuked = false;
    ValueModified = false;
    ValueRenamed = false;
    ParentKeyIndex = -1;
}

