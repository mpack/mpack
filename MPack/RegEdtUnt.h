#ifndef REG_EDIT_UNIT_H
#define REG_EDIT_UNIT_H

#include <QtGui/QMainWindow>
#include "ui_RegEdtFrm.h"
#include "PlatformBuilderImageUtils.h"
#include <QtCore/QVector>
#include <QtCore/QSet>
#include <QtGui/QComboBox>
#include <QtGui/QLabel>
#include <QtGUI/QToolButton>


typedef struct tagKeyBundleRecItem {
    bool StockPresent;
    bool KeyNuked;
    bool KeyRenamed;
    QString NewName;
    // president store
    CERegistryKey ItemKey;
    QSet < int > SubKeyIndexes; // sorted
    QSet < int > ValueIndexes; // sorted
    int ParentKeyIndex;
    QTreeWidgetItem* KeyWidget;

    tagKeyBundleRecItem();
} TKeyBundleRecItem, *PKeyBundleRecItem;

typedef QVector < TKeyBundleRecItem > TKeyBundleRec;

typedef struct tagValueBundleRecItem {
    bool StockPresent;
    bool ValueNuked;
    bool ValueRenamed;
    QString NewName;
    bool ValueModified;
    QByteArray NewValue;
    // president store
    CERegistryValue ItemValue;
    int ParentKeyIndex;

    tagValueBundleRecItem();
} TValueBundleRecItem, *PValueBundleRecItem;

typedef QVector < TValueBundleRecItem > TValueBundleRec;

class TRegEdtFrm : public QMainWindow
{
    Q_OBJECT

protected:
    TKeyBundleRec fKeyBundleRec; // KeyStore
    TValueBundleRec fValueBundleRec; // ValueStore
    QSet < int > fValuePatchRec; // PendingStore
    QSet < int > fKeyPatchRec;
    QString fHiveFilename; // May need by sub
    
public:
    explicit TRegEdtFrm(QWidget *parent = 0, Qt::WFlags flags = 0);
    ~TRegEdtFrm();
private:
    Ui::RegEditForm ui;
    QLabel fAddressLabel;
    QToolButton fGoBtn;
    QComboBox fURLEdt;
    QIcon fFolderIcon, fFolderIconOpened;
    QIcon fSingleLineIcon, fBinaryIcon, fMultiLineIcon, fMUIIcon;
    QString fEditorValueStringExpr;
    DWORD fEditorValueDWORDExpr;
    QByteArray fEditorValueBinaryExpr;
    RegistryValueDataType fEditingType; // for Casting
    void* fEditingDialog;
    int fEditingIndex;
    bool fDiscardRenameCheck;
    static bool fCERegInitialized;
protected:
    void closeEvent(QCloseEvent *event);
private:
    void TestBuildTreeFromKey(CERegistryKey &topkey, QTreeWidgetItem* topitem);
    void ShowSubContentsForKey(int topkeyindex);
    void RefreshBriefView();
    QIcon SelectIconFromType(RegistryValueDataType type);
    bool ApplyPatchToHive();
    bool IsPatchExists();
    bool BriefWannaNewValue(RegistryValueDataType type);
    CERegistryKey GetSubkeyFromPlainPath(QString RootPath, QString TailPath, CERegistryKey& classes, CERegistryKey& curusr, CERegistryKey& machine, CERegistryKey& users);
public:
    bool ParseHiveFromFile(QString filename);
signals:
    void hiveUpdated(QString filename, bool modified, bool nukefile);
private slots:
    void onKeyitemExpanded(QTreeWidgetItem* item);
    void onKeyitemCollapsed(QTreeWidgetItem* item);
    void onKeyitemNavigated(QTreeWidgetItem* current, QTreeWidgetItem* previous);
    void onBriefDoubleClicked(QTableWidgetItem* item);
    void onBriefViewRightClicked(const QPoint& pos);
    void onBriefWannaModifyValue();
    void onStringValueModified(QString text);
    void onMultiLineValueModified();
    void onDWORDValueHexModified(QString hex);
    void onDWORDValueDecModified(QString dec);
    void onBinaryValueModified(QString bin);
    void onEditorAccepted();
    void onEditorRejected();
    void onBriefWannaDeleteValue();
    void onBriefWannaRenameValue(QTableWidgetItem* item);
    void onURLGoClicked();
    void onEditorSaveClicked();
    void onEditorCloseClicked();
    void onBreifNewStringValue();
    void onBreifNewDWORDValue();
    void onBreifNewBinaryValue();
    void onBreifNewMultiStringValue();
    void onBreifNewMUIStringValue();
    void onBreifNewExpandStringValue();
    void onBreifNewLinkValue();
    void onBriefNewSubKey();
    void onBriefWannaStartRename();
};

#endif
