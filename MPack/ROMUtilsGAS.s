
.intel_syntax noprefix
.globl @SearchGap@8
.globl @CalcChecksum32@8

/* CD                          *\
 * ECX = Buffer EDX = Size     *
\*  EAX:EDX Start:End          */

@SearchGap@8:
        PUSH  EDI
        MOV   EDI, ECX /* Buffer */
        MOV   ECX, EDX /* Size   */
        XOR   EAX, EAX
        REPNE SCASD
        PUSH  EDI
        REPE  SCASD
        MOV   EDX, EDI
        POP   EAX
        POP   EDI
        RET

@CalcChecksum32@8:
        TEST  EDX, EDX
        JNZ   loc_ok
        XOR   EAX, EAX
        JMP   loc_ret
    loc_ok:
        /* PUSH  ECX */
        PUSH  ESI
        PUSH  EDI
        MOV   EDI, ECX  /* EDI := Buffer */
        MOV   ESI, EDX  /* ESI := Size;  */
        XOR   ECX, ECX  /* I := 0;       */
        XOR   EAX, EAX  /* Result := 0;  */


    loc_addbytes:
        XOR   EDX, EDX
        MOV   DL, [ECX+EDI]
        ADD   EAX, EDX
        INC   ECX
        CMP   ECX, ESI
        JB    loc_addbytes /* Check if we've reached the end of the stream */

        POP   EDI
        POP   ESI
        /* POP   ECX */
    loc_ret:
        RET
