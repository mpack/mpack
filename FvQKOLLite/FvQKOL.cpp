#include <ctime>
#include <cstdlib>
#include "FvQKOL.h"

using std::time;
using std::srand;
using std::rand;


KOLString Int2Str( int Value )
{
    char buf[16];
    char* dst;
    bool minus = (Value < 0);
    if (minus) {
        Value = 0 - Value;
    }
    dst = &buf[15];
    *dst = 0;
    unsigned d = Value;
    do {
        dst--;
        *dst = (d % 10) + '0';
        d = d / 10;
    } while (d > 0);
    if (minus) {
        dst--;
        *dst = '-';
    }

	return KOLString::fromLatin1(dst);
}

KOLString Num2Bytes( double Value ) {
    int V, I = 0;
    while (Value >= 1024 && I < 4)
    {
        I++;
        Value = Value / 1024.0;
    }
    KOLString Result = Int2Str( int( Value ) );
    V = int( (Value - int( Value )) * 100 );
    if (V != 0)
    {
        if ((V % 10) == 0)
            V = V / 10;
        Result = Result + "." + Int2Str( V );
    }
    if (I > 0) {
#ifdef __GNUC__
        KOLChar Suffix[5] = {L'K', L'M', L'G', L'T', };
#else
        KOLChar Suffix[5] = _T("KMGT");//{L'K', L'M', L'G', L'T'};
#endif
        Result = Result + "?";
        Result.data()[Result.size() - 1] = Suffix[ I - 1 ];
    }
    return Result;
}

static bool randomized = false;
int random(int from, int to) {
    int result;
    if (!randomized) {
        srand((int) time(NULL));
        randomized = true;
    }
    //result = floor((double(rand())/ RAND_MAX) * (to - from)) + from;
    result = rand()%(to - from + 1) + from;
    return result;
}
