#ifndef _KOL_H
#define _KOL_H

#include <QString>
#include <QList>
#include <tchar.h>

using std::list;

#ifdef UNICODE
typedef QString KOLString;
typedef ushort KOLChar;
#else
typedef QLatin1String KOLString;
typedef uchar KOLChar;
#endif
#define _L(x)      L ## x

KOLString Int2Str(int Value);
KOLString Num2Bytes( double Value );
int random(int from, int to);

#endif
