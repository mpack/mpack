#include "FvQMD5.h"

unsigned char QuadBit2Hex(unsigned char num) {
    if (num < 10) {
        return num + '0';
    } else {
        return num + '7';
    }
}

unsigned char QuadHex2Bit(unsigned char chr) {
    if (chr < 'A') {
        return chr - '0';
    } else {
        return chr - '7';
    }
}

KOLString MD5DigestToStr( const TMD5Digest Digest )
{
    KOLString Result = "";
    for (int i = 0; i <= 15; i++) {
        Result += QuadBit2Hex(Digest.v[i] >> 4);
        Result += QuadBit2Hex(Digest.v[i] & 0xF);
    }
    return Result;
}

TMD5Digest MD5StrToDigest( const KOLString Value )
{
    TMD5Digest Result = {0,};
    
    if (Value.length() != 32) return Result;
    for (int i = 0; i <= 15; i++) {
        Result.v[i] = QuadHex2Bit(Value[i*2].toLatin1()) << 4;
        Result.v[i] += QuadHex2Bit(Value[i*2 + 1].toLatin1());
    }

    return Result;
}

bool IsEmpty( const TMD5Digest Digest )
{
    return ((Digest.A == 0) && (Digest.B == 0) && (Digest.C == 0) && (Digest.D == 0));
}

bool MD5DigestCompare( const TMD5Digest Digest1, const TMD5Digest Digest2 )
{
    if (Digest1.A != Digest2.A) return false;
    if (Digest1.B != Digest2.B) return false;
    if (Digest1.C != Digest2.C) return false;
    if (Digest1.D != Digest2.D) return false;
    return true;
}
